package dal

import (
	"context"
	"strconv"
	"time"
)

type AdminVoucherEntry struct {
	EntityId        int
	AdminId         int
	VoucherId       int
	Amount          float64
	Currency        string
	CurrencyValue   float64
	Expired         bool
	ExpiredOneMonth bool
	Payed           bool
}

func (db *DataBaseManager) GetLeasityPlansVoucherEntries(todayDate, minusOneMonthDate string) (map[int]bool, map[int][]AdminVoucherEntry, error) {

	rows, err := db.connection.Query(context.Background(), `select ve.amount as amount, ve.currency, coalesce(cd.value,cd2.value,0.0) as ufval, v.id as vId, v.entity_id as EntityId,
	v.expiration_date < $2 and (v.payment_id is null and v.manually_paid is not true) as expired1month,
	v.expiration_date < $1 and (v.payment_id is null and v.manually_paid is not true) as expired,
	(ve.liquidation_id is not null) or (v.payment_id is not null) or (v.manually_paid is true) as payed
	from vouchers v join voucher_entries ve on ve.voucher_id = v.id
	left join payments p on p.id = v.payment_id
	left join currency_data cd on cd."date" = p.accounting_date and cd.index_type = 'UF'
	left join currency_data cd2 on cd2."date" = cast(v.manual_status_set_at as date) and cd2.index_type = 'UF'
	where v.tenant_id = 'a3f946f2-9ad1-4a74-a18a-3fa1e3909e6e' and v.expiration_date > '2022-06-01' and v.canceled is not true`, todayDate, minusOneMonthDate)

	if err != nil {
		return nil, nil, err
	}

	expiredAdmins := make(map[int]bool)

	adminPayedVoucherEntries := make(map[int][]AdminVoucherEntry)
	adminExpiredVoucherEntries := make(map[int][]AdminVoucherEntry)
	adminOneMonthExpiredVoucherEntries := make(map[int][]AdminVoucherEntry)
	adminVoucherEntries := make(map[int][]AdminVoucherEntry)

	for rows.Next() {
		NewEntry := AdminVoucherEntry{}

		err = rows.Scan(&NewEntry.Amount,
			&NewEntry.Currency,
			&NewEntry.CurrencyValue,
			&NewEntry.VoucherId,
			&NewEntry.EntityId,
			&NewEntry.ExpiredOneMonth,
			&NewEntry.Expired,
			&NewEntry.Payed)
		if err != nil {
			return nil, nil, err
		}

		if NewEntry.Expired {
			expiredAdmins[NewEntry.EntityId] = true
			adminExpiredVoucherEntries[NewEntry.EntityId] = append(adminExpiredVoucherEntries[NewEntry.EntityId], NewEntry)
		}
		if NewEntry.ExpiredOneMonth {
			expiredAdmins[NewEntry.EntityId] = true
			adminOneMonthExpiredVoucherEntries[NewEntry.EntityId] = append(adminOneMonthExpiredVoucherEntries[NewEntry.EntityId], NewEntry)
		}
		if NewEntry.Payed {
			adminPayedVoucherEntries[NewEntry.EntityId] = append(adminPayedVoucherEntries[NewEntry.EntityId], NewEntry)
		}

		if NewEntry.Expired || NewEntry.ExpiredOneMonth {
			expiredAdmins[NewEntry.EntityId] = true
		}
		adminVoucherEntries[NewEntry.EntityId] = append(adminVoucherEntries[NewEntry.EntityId], NewEntry)

	}

	return expiredAdmins, adminVoucherEntries, nil
}

func (db *DataBaseManager) GetAdminContactInfo(EntityId int) (*ContactInfo, error) {

	rows, err := db.connection.Query(context.Background(), `select a.street, c.first_name, c.surname, u.username, cmp.tax_mail ,replace(concat(cpn.prefix ,cpn."number"),';',' ') as number, cl.first_name,
	coalesce(t1.properties,0), pt.id 
	from contracts c2 
	join properties p on p.id = c2.property_id
	left join users u on p.tax_id = u.id 
	join contacts c on u.contact_info_id = c.id
	join contacts cl on cl.id = c2.lessee_id
	join addresses a on a.id = p.address_id
	left join contact_phone_numbers cpn on cpn.contact_id = c.id
	left join properties_property_tags ppt on ppt.property_id = p.id
	left join property_tags pt on ppt.property_tag_id = pt.id
	left join companies cmp on cmp.id = u.company_id
	left outer join (
	select u1.id as id, count(p1.id) as properties
	from users u1
	join properties p1 on p1.tenant_id = u1.id
	group by u1.id) as t1 on t1.id = u.id 
	where c2.id = $1`, EntityId)

	if err != nil {
		return nil, err
	}
	newContact := ContactInfo{}
	newTag := 0

	for rows.Next() {

		err = rows.Scan(&newContact.AdressStreet,
			&newContact.ContactFirstName,
			&newContact.ContactSurname,
			&newContact.ContactMail,
			&newContact.TaxMail,
			&newContact.PhoneNumber,
			&newContact.LesseName,
			&newContact.Nproperties,
			&newTag)

		if err != nil {
			return nil, err
		}
		if KAMS[newTag] != "" {
			newContact.KAM += KAMS[newTag]
		}
		if PlanTypes[newTag] != "" {
			newContact.Plan = PlanTypes[newTag]
		}

	}
	if newContact.LesseName == "" {
		newContact.LesseName = strconv.Itoa(EntityId)
		newContact.AdressStreet = newContact.LesseName
	}
	return &newContact, nil
}

func (db *DataBaseManager) GetLeasityLastCyclePaymentsByTenant() (
	AccruedPayments map[string]float64,
	AccruedAmounts map[string]float64,
	err error) {

	lastCycle := time.Now().Year()*12 + int(time.Now().Month()) - 1 - 1

	rows, err := db.connection.Query(context.Background(), `select v.cycle_number, v.tenant_id,
	round(sum(ps.amount)),
	count(p.id)
	from vouchers v
	join payments p on v.payment_id  = p.id
	join payment_sessions ps on ps.id = p.id
	where payment_id is not null and v.canceled is not true and p.reversed is null and v.cycle_number = $1
	group by cycle_number, tenant_id`, lastCycle)

	if err != nil {
		return nil, nil, err
	}

	accruedPaymentsOutMap := make(map[string]float64)
	accruedAmountsOutMap := make(map[string]float64)

	nPayments := 0
	nCycle := 0
	nTenantId := ""
	accruedAmount := 0.0

	for rows.Next() {
		err = rows.Scan(&nCycle, &nTenantId, &accruedAmount, &nPayments)
		if err != nil {
			return nil, nil, err
		}

		accruedPaymentsOutMap[nTenantId] = float64(nPayments)
		accruedAmountsOutMap[nTenantId] = float64(accruedAmount)

	}

	return accruedPaymentsOutMap, accruedAmountsOutMap, nil

}
