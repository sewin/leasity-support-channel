package dal

import "context"

type VoucherInfo struct {
	TenantID              string         `json:"tenantId"`
	EntityID              int            `json:"entityId"`
	VoucherEntries        []VoucherEntry `json:"voucherEntry"`
	VoucherID             int            `json:"vId"`
	Paid                  bool           `json:"payed"`
	AdminName             string         `json:"adminName"`
	KAM                   string         `json:"kam"`
	PropertyId            int            `json:"propertyId"`
	Username              string
	PaymentID             int
	PaymentProvider       string
	PaymentAccountingDate string
	PaymentCreatedAt      string
	Status                string
	CycleNumber           int
}

type VoucherEntry struct {
	Amount   float64 `json:"amount"`
	Currency string  `json:"currency"`
	UFValue  float64 `json:"ufval"`
}

func (db *DataBaseManager) GetVoucherInfo(VoucherId int) (*VoucherInfo, error) {

	rows, err := db.connection.Query(context.Background(), `select ve.tenant_id , v.entity_id , ve.amount as amount, ve.currency, coalesce(cd.value,cd2.value,0.0) as ufval, v.id as vId, 
	(ve.liquidation_id is not null) or (v.payment_id is not null) or (v.manually_paid is true) as payed,
	coalesce(p.provider,''), coalesce(p.id, 0), coalesce(to_char(p.accounting_date, 'YYYY-MM-DD'),''), coalesce(to_char(p.created_at, 'YYYY-MM-DD'),''),
	v.info::jsonb->>'agentName' as adminName,
	case 
		when v.deleted then 'DELETED'
		when v.canceled then 'CANCELED'
		when ve.liquidation_id is not null or ve.manually_liquidated is true then 'LIQUIDATED'
		when v.manually_paid or v.payment_id is not null then 'PAYED'
		when now() > v.expiration_date then 'EXPIRATED'
		else 'GENERATED' end as status
	from vouchers v join voucher_entries ve on ve.voucher_id = v.id
	left join payments p on p.id = v.payment_id
	left join currency_data cd on cd."date" = p.accounting_date and cd.index_type = 'UF'
	left join currency_data cd2 on cd2."date" = cast(v.manual_status_set_at as date) and cd2.index_type = 'UF'
	where v.canceled is not true and v.id = $1`, VoucherId)

	if err != nil {
		return nil, err
	}

	voucherInfo := VoucherInfo{}

	for rows.Next() {
		newVoucherEntry := VoucherEntry{}
		err = rows.Scan(&voucherInfo.TenantID,
			&voucherInfo.EntityID,
			&newVoucherEntry.Amount,
			&newVoucherEntry.Currency,
			&newVoucherEntry.UFValue,
			&voucherInfo.VoucherID,
			&voucherInfo.Paid,
			&voucherInfo.PaymentProvider,
			&voucherInfo.PaymentID,
			&voucherInfo.PaymentAccountingDate,
			&voucherInfo.PaymentCreatedAt,
			&voucherInfo.AdminName,
			&voucherInfo.Status)
		if err != nil {
			return nil, err
		}
		voucherInfo.VoucherEntries = append(voucherInfo.VoucherEntries, newVoucherEntry)

	}
	voucherInfo.VoucherID = VoucherId
	return &voucherInfo, nil
}

func (db *DataBaseManager) GetTagAndPropertyInfo(vi *VoucherInfo) error {

	rows, err := db.connection.Query(context.Background(), `select u.username, coalesce(t.tagid,0), p.id from contracts ct
	join users u on u.id = ct.tenant_id 
	join properties p on ct.property_id = p.id  
	join contacts c on c.id = u.contact_info_id
	left outer join (select pt.description as tag, p.tax_id as rol, pt.id as tagid from properties p 
	left join properties_property_tags ppt on ppt.property_id = p.id
	left join property_tags pt on ppt.property_tag_id = pt.id) as t on t.rol = u.tenant_id 
	where ct.id = $1`, vi.EntityID)

	if err != nil {
		return err
	}

	tagId := 0
	tagN := 0

	for rows.Next() {
		err = rows.Scan(&vi.Username,
			&tagId,
			&vi.PropertyId)
		if err != nil {
			return err
		}
		if KAMS[tagId] != "" && tagN < 1 {
			tagN++
			vi.KAM += KAMS[tagId]
		}

	}

	return nil
}

func (db *DataBaseManager) GetInsurancePropertyContractInfo(pId int) (int, error) {

	rows, err := db.connection.Query(context.Background(), `select c.id from properties p join contracts c on c.property_id = p.id where p.id = $1`, pId)

	if err != nil {
		return 0, err
	}

	contractID := 0

	for rows.Next() {
		err = rows.Scan(&contractID)
		if err != nil {
			return 0, err
		}
	}

	return contractID, nil
}

func (db *DataBaseManager) GetLastVoucherByContract(contractId int) (*VoucherInfo, error) {

	rows, err := db.connection.Query(context.Background(), `select ve.tenant_id , v.entity_id , ve.amount as amount, ve.currency, coalesce(cd.value,cd2.value,0.0) as ufval, v.id as vId, 
	(ve.liquidation_id is not null) or (v.payment_id is not null) or (v.manually_paid is true) as payed, v.cycle_number,
	coalesce(p.provider,''), coalesce(p.id, 0), coalesce(to_char(p.accounting_date, 'YYYY-MM-DD'),''),
	v.info::jsonb->>'agentName' as adminName,
	case 
		when v.deleted then 'DELETED'
		when v.canceled then 'CANCELED'
		when ve.liquidation_id is not null or ve.manually_liquidated is true then 'LIQUIDATED'
		when v.manually_paid or v.payment_id is not null then 'PAYED'
		when now() > v.expiration_date then 'EXPIRATED'
		else 'GENERATED' end as status
	from (select v.id from vouchers v where v.entity_id = $1 order by v.id desc limit 1) as vin join vouchers v on vin.id = v.id join voucher_entries ve on ve.voucher_id = v.id
	left join payments p on p.id = v.payment_id
	left join currency_data cd on cd."date" = p.accounting_date and cd.index_type = 'UF'
	left join currency_data cd2 on cd2."date" = cast(v.manual_status_set_at as date) and cd2.index_type = 'UF'
	where v.canceled is not true and v.deleted is not true and v.id = vin.id`, contractId)

	if err != nil {
		return nil, err
	}

	voucherInfo := VoucherInfo{}

	for rows.Next() {
		newVoucherEntry := VoucherEntry{}
		err = rows.Scan(&voucherInfo.TenantID,
			&voucherInfo.EntityID,
			&newVoucherEntry.Amount,
			&newVoucherEntry.Currency,
			&newVoucherEntry.UFValue,
			&voucherInfo.VoucherID,
			&voucherInfo.Paid,
			&voucherInfo.CycleNumber,
			&voucherInfo.PaymentProvider,
			&voucherInfo.PaymentID,
			&voucherInfo.PaymentAccountingDate,
			&voucherInfo.AdminName,
			&voucherInfo.Status)
		if err != nil {
			return nil, err
		}
		voucherInfo.VoucherEntries = append(voucherInfo.VoucherEntries, newVoucherEntry)

	}

	return &voucherInfo, nil
}
