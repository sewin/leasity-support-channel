package dal

import "context"

type PaymentInfo struct {
	TenantID        string         `json:"tenantId"`
	EntityID        int            `json:"entityId"`
	VoucherEntries  []VoucherEntry `json:"voucherEntry"`
	VoucherID       int            `json:"vId"`
	Paid            bool           `json:"payed"`
	AdminName       string         `json:"adminName"`
	KAM             string         `json:"kam"`
	PropertyId      int            `json:"propertyId"`
	Username        string
	PaymentID       int
	PaymentProvider string
	PaymentDate     string
}

func (db *DataBaseManager) GetPaymentInfo(paymentId int) (*PaymentInfo, error) {

	rows, err := db.connection.Query(context.Background(), `select ve.tenant_id , v.entity_id , ve.amount as amount, ve.currency, coalesce(cd.value,cd2.value,0.0) as ufval, v.id as vId, 
	(ve.liquidation_id is not null) or (v.payment_id is not null) or (v.manually_paid is true) as payed,
	p.provider, p.id, to_char(p.accounting_date, 'YYYY-MM-DD'),
	v.info::jsonb->>'agentName' as adminName
	from vouchers v join voucher_entries ve on ve.voucher_id = v.id
	left join payments p on p.id = v.payment_id
	left join currency_data cd on cd."date" = p.accounting_date and cd.index_type = 'UF'
	left join currency_data cd2 on cd2."date" = cast(v.manual_status_set_at as date) and cd2.index_type = 'UF'
	where v.canceled is not true and p.id = $1`, paymentId)

	if err != nil {
		return nil, err
	}

	paymentInfo := PaymentInfo{}

	for rows.Next() {
		newVoucherEntry := VoucherEntry{}
		err = rows.Scan(&paymentInfo.TenantID,
			&paymentInfo.EntityID,
			&newVoucherEntry.Amount,
			&newVoucherEntry.Currency,
			&newVoucherEntry.UFValue,
			&paymentInfo.VoucherID,
			&paymentInfo.Paid,
			&paymentInfo.PaymentProvider,
			&paymentInfo.PaymentID,
			&paymentInfo.PaymentDate,
			&paymentInfo.AdminName)
		if err != nil {
			return nil, err
		}
		paymentInfo.VoucherEntries = append(paymentInfo.VoucherEntries, newVoucherEntry)

	}
	paymentInfo.PaymentID = paymentId

	return &paymentInfo, nil
}

func (db *DataBaseManager) GetPaymentTagAndPropertyInfo(pi *PaymentInfo) error {

	rows, err := db.connection.Query(context.Background(), `select u.username, t.tagid, p.id from contracts ct
	join users u on u.id = ct.tenant_id 
	join properties p on ct.property_id = p.id  
	join contacts c on c.id = u.contact_info_id
	left outer join (select pt.description as tag, p.tax_id as rol, pt.id as tagid from properties p 
	left join properties_property_tags ppt on ppt.property_id = p.id
	left join property_tags pt on ppt.property_tag_id = pt.id) as t on t.rol = u.tenant_id 
	where ct.id = $1`, pi.EntityID)

	if err != nil {
		return err
	}

	tagId := 0
	tagN := 0

	for rows.Next() {
		err = rows.Scan(&pi.Username,
			&tagId,
			&pi.PropertyId)
		if err != nil {
			return err
		}
		if KAMS[tagId] != "" && tagN < 1 {
			tagN++
			pi.KAM += KAMS[tagId]
		}

	}

	return nil
}
