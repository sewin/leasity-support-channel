package dal

import "context"

func (db *DataBaseManager) GetClientContractsByCommune(tenants []string) (map[string][]ContractAreaInfo, error) {

	rows, err := db.connection.Query(context.Background(), `select c.id, p."type", a.region , a.commune, coalesce(p.useful_area,0) from contracts c
	join properties p on c.property_id = p.id 
	join addresses a on a.id = p.address_id 
	where (p."type" = 'APARTMENT' or p."type" = 'HOUSE')
	and a.commune != ''
	and c.tenant_id = any($1)
	`, tenants)

	if err != nil {
		return nil, err
	}

	contractRegionMap := make(map[string][]ContractAreaInfo)

	newContract := ContractAreaInfo{}

	for rows.Next() {
		err = rows.Scan(&newContract.EntityId,
			&newContract.Type,
			&newContract.Region,
			&newContract.Commune,
			&newContract.UsefulArea)
		if err != nil {
			return nil, err
		}
		if newContract.Region == "RM" {
			if contractRegionMap[newContract.Region] == nil {
				contractRegionMap[newContract.Region] = make([]ContractAreaInfo, 0)
				contractRegionMap[newContract.Region] = append(contractRegionMap[newContract.Region], newContract)
			} else {
				contractRegionMap[newContract.Region] = append(contractRegionMap[newContract.Region], newContract)
			}
			newContract.Region = newContract.Commune
		}
		if contractRegionMap[newContract.Region] == nil {
			contractRegionMap[newContract.Region] = make([]ContractAreaInfo, 0)
			contractRegionMap[newContract.Region] = append(contractRegionMap[newContract.Region], newContract)
		} else {
			contractRegionMap[newContract.Region] = append(contractRegionMap[newContract.Region], newContract)
		}

	}

	return contractRegionMap, nil
}

type ContractAreaInfo struct {
	EntityId   int
	Type       string
	Region     string
	Commune    string
	UsefulArea int
}

func (db *DataBaseManager) GetCycleVouchersRentAmount(cycleNumber int) (map[int]float64, error) {

	rows, err := db.connection.Query(context.Background(), `select sum(case when ve.currency = 'UF' then ve.amount*cd.value else ve.amount end), v.entity_id from vouchers v join voucher_entries ve on ve.voucher_id = v.id 
	left join payments p ON p.id = v.payment_id 
		left join currency_data cd on (cd."date" = (case when v.manually_paid is true then cast(v.manual_status_set_at as date) when v.payment_id is not null then p.accounting_date end) and cd.index_type = 'UF')
		where v.cycle_number = $1 and v.canceled is not true and (v.manually_paid is true or v.payment_id is not null)
		and (ve."type" = 'AGENT' or ve."type" = 'OWNER' or ve.type = 'EXTRA_RENTAL')
		group by v.id`, cycleNumber)

	if err != nil {
		return nil, err
	}

	contractCycleValue := make(map[int]float64)

	newVoucherValue := 0.0
	entityId := 0

	for rows.Next() {
		err = rows.Scan(&newVoucherValue,
			&entityId)
		if err != nil {
			return nil, err
		}

		contractCycleValue[entityId] = newVoucherValue

	}

	return contractCycleValue, nil
}

func (db *DataBaseManager) GetAllTimePlansClientTenants() ([]string, error) {

	rows, err := db.connection.Query(context.Background(), `select distinct coalesce(p.tax_id,'') from properties p 
	join contracts c on c.property_id = p.id 
	join properties_property_tags ppt on ppt.property_id = p.id
	join property_tags pt on pt.id = ppt.property_tag_id
	where p.tenant_id = 'a3f946f2-9ad1-4a74-a18a-3fa1e3909e6e'
	and pt.description ilike '%Plan%'`)

	if err != nil {
		return nil, err
	}

	//ps := make([]Property, 0)
	plansTenants := make([]string, 0)

	ntenant := ""

	for rows.Next() {
		err = rows.Scan(&ntenant)
		if err != nil {
			return nil, err
		}
		if ntenant != "Pendientes" && len(ntenant) == 36 {
			plansTenants = append(plansTenants, ntenant)
		} else if len(ntenant) > 36 {
			separatedTenants, err := SeparateRolIntoTenants(ntenant)
			if err != nil {
				separatedTenants = []string{"Tenants mal formateados"}
			}
			plansTenants = append(plansTenants, separatedTenants...)
		}

	}

	return plansTenants, nil

}
