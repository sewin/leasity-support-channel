package dal

import (
	"testing"
)

func TestSeparateTenants(t *testing.T) {

	type TestCase struct {
		rol         string
		expectederr error
		expectedOut []string
	}
	TestCases := []TestCase{
		{rol: "3525a5b6-0473-47ba-9e63-f7641d9c4595', '15e5e70c-da2a-4124-a277-70dfbc6cc67e', 'a3f946f2-9ad1-4a74-a18a-3fa1e3909e6e",
			expectederr: nil,
			expectedOut: []string{"3525a5b6-0473-47ba-9e63-f7641d9c4595", "15e5e70c-da2a-4124-a277-70dfbc6cc67e", "a3f946f2-9ad1-4a74-a18a-3fa1e3909e6e"}},
		{rol: "3525a5b6-0473-47ba-9e63-f7641d9c4595',  '15e5e70c-da2a-4124-a277-70dfbc6cc67e','a3f946f2-9ad1-4a74-a18a-3fa1e3909e6e",
			expectederr: ErrSeparatingTenants,
			expectedOut: nil},
		{rol: "3525a5b6-0473-47ba-9e63-f7641d9c4595','15e5e70c-da2a-4124-a277-70dfbc6cc67e','a3f946f2-9ad1-4a74-a18a-3fa1e3909e6e",
			expectederr: ErrSeparatingTenants,
			expectedOut: nil},
		{rol: "3525a5b6-0473-47ba-9e63-f7641d9c4595', '15e5e70c-da2a-4124-a277-70dfbc6cc67e'",
			expectederr: ErrSeparatingTenants,
			expectedOut: nil},
		{rol: "3525a5b6-0473-47ba-9e63-f7641d9c4595', '15e5e70c-da2a-4124-a277-70dfbc6cc67e",
			expectederr: nil,
			expectedOut: []string{"3525a5b6-0473-47ba-9e63-f7641d9c4595", "15e5e70c-da2a-4124-a277-70dfbc6cc67e"}},
	}

	for _, tc := range TestCases {
		tenants, err := SeparateRolIntoTenants(tc.rol)
		if err != tc.expectederr {
			t.Log("test failed", "err expected:", tc.expectederr, "got:", err)
			t.Log("out:", tenants)
			t.Fail()
		} else {
			t.Log("input:", tc.rol)
			t.Log("out:", tenants)
		}

	}

}

/* func TestGetAllTimeTenants(t *testing.T) {
	os.Setenv("DB_HOST", "localhost")
	os.Setenv("DB_PORT", "9623")
	os.Setenv("DB_USER", "leasity_sebastian")
	os.Setenv("DB_USER_PASSWORD", "jE8?a5JP#AbqV7wr")
	os.Setenv("DB_NAME", "leasity")
	os.Setenv("PAYMENTS_DB_NAME", "payments")
	os.Setenv("AUTH_SLACK_CHANNEL", "C04AH7QDS04")
	os.Setenv("TEST_SUPPORT_CHAN", "C0592RHFPV3")

	LeasityDB, err := NewDataBaseManager(os.Getenv("DB_HOST"), os.Getenv("DB_USER"), os.Getenv("DB_USER_PASSWORD"), os.Getenv("DB_NAME"), os.Getenv("DB_PORT"))
	if err != nil {

		t.Fatalf("Error connecting to leasity DB " + " : " + err.Error())
		return
	}

	PaymentsDB, err := NewDataBaseManager(os.Getenv("DB_HOST"), os.Getenv("DB_USER"), os.Getenv("DB_USER_PASSWORD"), os.Getenv("PAYMENTS_DB_NAME"), os.Getenv("DB_PORT"))
	if err != nil {

		t.Fatalf("Error connecting to payments DB " + " : " + err.Error())
		return
	}

	LeasityDB.Connect()

	PaymentsDB.Connect()

	tenants, err := LeasityDB.GetAllTimePlansClientTenants()
	if err != nil {
		t.Fatalf("Error connecting to leasity DB " + " : " + err.Error())
		return
	}

	log.Println(len(tenants))

	contracts, err := PaymentsDB.GetCycleVouchersRentAmount(24287)
	if err != nil {
		t.Fatalf("Error connecting to leasity DB " + " : " + err.Error())
		return
	}
	log.Println(len(contracts))
}
*/
