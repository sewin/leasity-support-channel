package dal

import (
	"context"
	"time"
)

func (db *DataBaseManager) GetPlansClientTenantsAndKams() (map[int]*PlanAccountClient, error) {

	rows, err := db.connection.Query(context.Background(), `select distinct coalesce(p.tax_id,''), coalesce(a.street,''), c.id, p.id , pt.description, c.enabled from properties p
	join contracts c on c.property_id = p.id 
	join properties_property_tags ppt on ppt.property_id = p.id
	join property_tags pt on pt.id = ppt.property_tag_id
	join addresses a on p.address_id = a.id
	where p.tenant_id = 'a3f946f2-9ad1-4a74-a18a-3fa1e3909e6e'`)

	if err != nil {
		return nil, err
	}

	plansTenants := make(map[int]*PlanAccountClient)

	ntenant := ""
	nEntityId := 0
	tag := ""
	enabled := false
	name := ""
	nPropertyId := 0

	for rows.Next() {
		err = rows.Scan(&ntenant, &name, &nEntityId, &nPropertyId, &tag, &enabled)
		if err != nil {
			return nil, err
		}

		if plansTenants[nEntityId] != nil && !(KAMSStatus[tag]) {
			continue
		}
		if !KAMSStatus[tag] {
			tag = "Sin KAM"
		}

		newClient := PlanAccountClient{TenantID: ntenant, ContractID: nEntityId, KAM: tag, Enabled: enabled, Name: name, PropertyID: nPropertyId}
		plansTenants[nEntityId] = &newClient

	}

	return plansTenants, nil

}

type PlanAccountClient struct {
	TenantID   string `json:"tenant_id"`
	ContractID int    `json:"contract_id"`
	PropertyID int    `json:"property_id"`
	KAM        string `json:"kam"`
	Enabled    bool   `json:"enabled"`
	Name       string `json:"name"`
}

func (db *DataBaseManager) GetPlansVoucherPayments() ([]PlanAccountVoucherPaymentsInfo, error) {

	timeNowString := time.Now().Format("2006-01-02")

	rows, err := db.connection.Query(context.Background(), `select sum(ve.amount) as amount, v.cycle_number , v.id, v.entity_id as EntityId,
	v.expiration_date < $1 and (v.payment_id is null and v.manually_paid is not true) as expired,
	(ve.liquidation_id is not null) or (v.payment_id is not null) or (v.manually_paid is true) as payed
	from vouchers v join voucher_entries ve on ve.voucher_id = v.id
	left join payments p on p.id = v.payment_id
	left join currency_data cd on cd."date" = p.accounting_date and cd.index_type = 'UF'
	left join currency_data cd2 on cd2."date" = cast(v.manual_status_set_at as date) and cd2.index_type = 'UF'
	where v.tenant_id = 'a3f946f2-9ad1-4a74-a18a-3fa1e3909e6e' and v.expiration_date > '2022-06-01' and v.canceled is not true
	group by v.id, ve.liquidation_id`, timeNowString)

	if err != nil {
		return nil, err
	}

	//ps := make([]Property, 0)
	plansPayments := make([]PlanAccountVoucherPaymentsInfo, 0)

	nCycle := 0
	nAmount := 0.0
	nVId := 0
	nEntityId := 0
	expired := false
	payed := false

	for rows.Next() {
		err = rows.Scan(&nAmount, &nCycle, &nVId, &nEntityId, &expired, &payed)
		if err != nil {
			return nil, err
		}

		newVoucher := PlanAccountVoucherPaymentsInfo{Amount: nAmount, EntityID: nEntityId,
			CycleNumber: nCycle, Payed: payed, Expired: expired, VoucherID: nVId}
		plansPayments = append(plansPayments, newVoucher)

	}

	return plansPayments, nil

}

type PlanAccountVoucherPaymentsInfo struct {
	Amount      float64 `json:"amount"`
	CycleNumber int     `json:"cycle_number"`
	VoucherID   int     `json:"voucher_id"`
	EntityID    int     `json:"entity_id"`
	Expired     bool    `json:"expired"`
	Payed       bool    `json:"payed"`
}
