package couchbase

import (
	"testing"

	log "github.com/sirupsen/logrus"
)

func TestCouchbaseConnection(t *testing.T) {
	db := NewDataBaseManager()

	if err := db.Connect(); err != nil {
		log.Error(err)
		return
	}
	defer db.Close()

}
