package couchbase

import (
	"os"
	"strconv"
	"time"

	"github.com/couchbase/gocb/v2"
	log "github.com/sirupsen/logrus"
)

// ErrNotFound : Not Found
var ErrNotFound = gocb.ErrDocumentNotFound
var ErrNoResult = gocb.ErrNoResult

type dataBaseManager struct {
	// unexported fields
	cluster       *gocb.Cluster
	bucket        *gocb.Bucket
	connString    string
	user          string
	password      string
	bucketName    string
	certPath      string
	verboseLogger bool
}

func NewDataBaseManager() *dataBaseManager {
	connString := os.Getenv("DB_CONN_STRING")
	if connString == "" {
		connString = "couchbase://localhost"
	}

	user := os.Getenv("CB_DB_USER")
	if user == "" {
		user = "Administrator"
	}

	password := os.Getenv("CB_DB_PASSWORD")
	if password == "" {
		password = "5d9sZQmT"
	}

	bucketName := os.Getenv("CB_DB_BUCKET_NAME")
	if bucketName == "" {
		bucketName = "firmex"
	}

	verboseLogger := false
	verboseLoggerStr := os.Getenv("DB_VERBOSE_LOGGER")
	if verboseLoggerStr != "" {
		res, err := strconv.ParseBool(verboseLoggerStr)
		if err == nil {
			verboseLogger = res
		}
	}

	return &dataBaseManager{
		connString:    connString,
		user:          user,
		password:      password,
		bucketName:    bucketName,
		verboseLogger: verboseLogger,
	}

}

func (manager *dataBaseManager) Connect() error {
	log.WithFields(log.Fields{
		"user":       manager.user,
		"password":   manager.password,
		"bucketName": manager.bucketName,
		"certPath":   manager.certPath},
	).Info("Connecting to DB at:", manager.connString)

	var err error

	if manager.verboseLogger {
		gocb.SetLogger(gocb.VerboseStdioLogger())
	}

	// Initialize the Connection
	manager.cluster, err = gocb.Connect(manager.connString, gocb.ClusterOptions{
		Username: manager.user,
		Password: manager.password,
	})
	if err != nil {
		log.Fatal("DB connection failed:", err.Error())
		return err
	}

	manager.bucket = manager.cluster.Bucket(manager.bucketName)

	err = manager.bucket.WaitUntilReady(5*time.Second, nil)
	if err != nil {
		log.Fatal("DB connection failed:", err.Error())
		return err
	}

	log.Info("Connected to DB")

	return nil
}

func (manager *dataBaseManager) Close() {
	log.Info("Closing DB connection ...")
	manager.cluster.Close(nil)
}
