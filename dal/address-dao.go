package dal

import "context"

func (db *DataBaseManager) GetAddressInfoByAddresName(addressName string) ([]AddressInfo, error) {
	addressName = "%" + addressName + "%"
	rows, err := db.connection.Query(context.Background(), `select a.street,a.street_number,p.type, coalesce(a.apartment,''), a.commune, a.region,
	co.first_name, co.surname, coalesce(co.second_surname,''), coalesce(co.tax_id,''), co.mail,
	cl.first_name, cl.surname, coalesce(cl.second_surname,''), coalesce(cl.tax_id,''), cl.mail,
	ca.first_name, ca.first_name, coalesce(ca.second_surname,''), coalesce(ca.tax_id,''), u.username, u.id
	from addresses a join properties p on p.address_id = a.id 
	join contacts co on co.id = p.owner_id join contracts c on c.property_id = p.id join contacts cl on c.lessee_id = cl.id 
	join users u on p.tenant_id = u.id join contacts ca on u.contact_info_id = ca.id  
	where concat(trim(a.street),' ',a.street_number,' ',p.type, ' ', coalesce(a.apartment,'')) ilike $1`, addressName)

	if err != nil {
		return nil, err
	}
	var ai AddressInfo

	//ps := make([]Property, 0)
	addresses := make([]AddressInfo, 0)

	naddress := 0

	for rows.Next() {
		err = rows.Scan(&ai.Property.Street, &ai.Property.StreetNumber, &ai.Property.Type, &ai.Property.Apartment, &ai.Property.Commune, &ai.Property.Region,
			&ai.OwnerFirstName, &ai.OwnerSurname, &ai.OwnerSecondSurname, &ai.OwnerTaxId, &ai.OwnerMail,
			&ai.LesseeFirstName, &ai.LesseeSurname, &ai.LesseeSecondSurname, &ai.LesseeTaxId, &ai.LesseeMail,
			&ai.AdminFirstName, &ai.AdminSurname, &ai.AdminSecondSurname, &ai.AdminTaxId, &ai.AdminUsername, &ai.AdminTenantId)
		if err != nil {
			return nil, err
		}
		naddress++

		addresses = append(addresses, ai)

	}

	return addresses, nil

}

type AddressInfo struct {
	AdminFirstName      string   `json:"admin_first_name"`
	AdminSurname        string   `json:"admin_surname"`
	AdminSecondSurname  string   `json:"admin_second_surname"`
	AdminTenantId       string   `json:"admin_tenant_id"`
	AdminTaxId          string   `json:"admin_tax_id"`
	AdminUsername       string   `json:"admin_username"`
	OwnerFirstName      string   `json:"owner_first_name"`
	OwnerSurname        string   `json:"owner_surname"`
	OwnerSecondSurname  string   `json:"owner_second_surname"`
	OwnerTaxId          string   `json:"owner_tax_id"`
	OwnerMail           string   `json:"owner_mail"`
	LesseeFirstName     string   `json:"lessee_first_name"`
	LesseeSurname       string   `json:"lessee_surname"`
	LesseeSecondSurname string   `json:"lessee_second_surname"`
	LesseeTaxId         string   `json:"lessee_tax_id"`
	LesseeMail          string   `json:"lessee_mail"`
	Property            Property `json:"properties,omitempty"`
}

func (ad *AddressInfo) OwnerName() string {
	return ad.OwnerFirstName + " " + ad.OwnerSurname + " " + ad.OwnerSecondSurname
}

func (ad *AddressInfo) LesseeName() string {
	return ad.LesseeFirstName + " " + ad.LesseeSurname + " " + ad.LesseeSecondSurname
}

func (ad *AddressInfo) AdminName() string {
	return ad.AdminFirstName + " " + ad.AdminSurname + " " + ad.AdminSecondSurname
}
