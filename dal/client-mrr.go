package dal

import (
	"context"
	"errors"
	"log"
	"strings"
)

func (db *DataBaseManager) GetPaymentsClientTenants() ([]Client, error) {

	rows, err := db.connection.Query(context.Background(), `select distinct v.tenant_id from payments p 
		join vouchers v on p.id = v.payment_id`)

	if err != nil {
		return nil, err
	}

	//ps := make([]Property, 0)
	paymentsTenants := make([]Client, 0)

	ntenant := ""

	for rows.Next() {
		err = rows.Scan(&ntenant)
		if err != nil {
			return nil, err
		}

		paymentsTenants = append(paymentsTenants, Client{TenantId: ntenant, Type: "Pagos online"})

	}

	return paymentsTenants, nil

}

type Client struct {
	TenantId string
	EntityId int
	Type     string
}

func (db *DataBaseManager) GetPlansClientTenants() (map[int]Client, []string, error) {

	rows, err := db.connection.Query(context.Background(), `select distinct coalesce(p.tax_id,''), c.id, c.enabled from properties p 
	join contracts c on c.property_id = p.id
	where p.tenant_id = 'a3f946f2-9ad1-4a74-a18a-3fa1e3909e6e'`)

	if err != nil {
		return nil, nil, err
	}

	//ps := make([]Property, 0)
	plansTenants := make([]string, 0)

	ntenant := ""
	nEntityId := 0
	enabled := false

	outMap := make(map[int]Client)

	for rows.Next() {
		err = rows.Scan(&ntenant, &nEntityId, &enabled)
		if err != nil {
			return nil, nil, err
		}
		if enabled {
			if len(ntenant) > 36 {
				separatedTenants, err := SeparateRolIntoTenants(ntenant)
				if err != nil {
					separatedTenants = []string{"Tenants mal formateados"}
				}
				plansTenants = append(plansTenants, separatedTenants...)
				ntenant = separatedTenants[0]
			} else {
				plansTenants = append(plansTenants, ntenant)
			}
		}
		outMap[nEntityId] = Client{TenantId: ntenant, EntityId: nEntityId, Type: "Planes"}
	}

	return outMap, plansTenants, nil

}

func (db *DataBaseManager) GetCatchmentsAndBrokertiesClientTenants() (map[int]Client, error) {

	rows, err := db.connection.Query(context.Background(), `select distinct coalesce(p.tax_id,''), c.id, pt.description from properties p 
	join contracts c on c.property_id = p.id 
	join properties_property_tags ppt on ppt.property_id = p.id
	join property_tags pt on pt.id = ppt.property_tag_id
	where p.tenant_id = '5d05592c-e05a-45ab-b66f-57c87da4f123'`)

	if err != nil {
		return nil, err
	}

	ntenant := ""
	nEntityId := 0
	tag := ""

	outMap := make(map[int]Client)

	for rows.Next() {
		err = rows.Scan(&ntenant, &nEntityId, &tag)
		if err != nil {
			return nil, err
		}

		outMap[nEntityId] = Client{TenantId: ntenant, EntityId: nEntityId, Type: tag}
	}

	return outMap, nil

}

func (db *DataBaseManager) GetManualPaymentsClientTenants() (map[int]Client, error) {

	rows, err := db.connection.Query(context.Background(), `select distinct coalesce(p.tax_id,''), c.id, c.enabled from properties p 
join contracts c on c.property_id = p.id
where p.tenant_id = '78b69f71-9589-4709-88ce-beaed48e3437'`)

	if err != nil {
		return nil, err
	}

	ntenant := ""
	nEntityId := 0

	enabled := false

	outMap := make(map[int]Client)

	for rows.Next() {
		err = rows.Scan(&ntenant, &nEntityId, &enabled)
		if err != nil {
			return nil, err
		}
		if len(ntenant) > 36 {
			separatedTenants, err := SeparateRolIntoTenants(ntenant)
			if err != nil {
				separatedTenants = []string{"Tenants mal formateados"}
			}
			ntenant = separatedTenants[0]

		}
		outMap[nEntityId] = Client{TenantId: ntenant, EntityId: nEntityId, Type: "Pagos Manuales"}
	}

	return outMap, nil

}

func (db *DataBaseManager) GetTenantPeriodsProperties(tenantId string) (map[int]float64, error) {

	rows, err := db.connection.Query(context.Background(), `WITH AllCycles AS (
		SELECT generate_series(
				 (SELECT (EXTRACT(YEAR FROM MIN(created_at)) * 12 + EXTRACT(MONTH FROM MIN(created_at)) - 1) FROM contracts where tenant_id = $1),
				 (SELECT (EXTRACT(YEAR FROM MAX(created_at)) * 12 + EXTRACT(MONTH FROM MAX(created_at)) - 1) FROM contracts where tenant_id = $1),
				 1
			   ) AS cycle_number
	  )
	  SELECT
	  ac.cycle_number,
	  SUM(COUNT(c.id)) OVER (ORDER BY ac.cycle_number) AS cumulative_contracts
	  from AllCycles ac
	  LEFT JOIN contracts c ON EXTRACT(YEAR FROM c.created_at) * 12 + EXTRACT(MONTH FROM c.created_at) - 1 = ac.cycle_number
	  AND c.tenant_id = $1
	  GROUP BY ac.cycle_number`, tenantId)

	if err != nil {
		return nil, err
	}

	//ps := make([]Property, 0)

	periodProperties := make(map[int]float64)

	nProperties := 0
	period := 0

	for rows.Next() {
		err = rows.Scan(&period, &nProperties)
		if err != nil {
			return nil, err
		}
		periodProperties[period] = float64(nProperties)

	}

	return periodProperties, nil

}

func (db *DataBaseManager) GetTenantCyclePaymentsRevenue(tenantId string) (map[int]float64, error) {

	rows, err := db.connection.Query(context.Background(), `select v.cycle_number, sum(CASE WHEN v.leasity_fee_type is not null 
		THEN (case when v.leasity_fee_type = 'CLP' then v.leasity_fee_value else ps.amount*v.leasity_fee_value*0.01 end) ELSE 0 END) 
		from vouchers v
		join payments p on v.payment_id  = p.id
		join payment_sessions ps on ps.id = p.id
		where payment_id is not null and tenant_id = $1 and v.canceled is not true
		group by cycle_number`, tenantId)

	if err != nil {
		return nil, err
	}

	periodsRevenue := make(map[int]float64)
	period := 0
	revenue := 0.0

	for rows.Next() {
		err = rows.Scan(&period, &revenue)
		if err != nil {
			return nil, err
		}
		periodsRevenue[period] = revenue

	}

	return periodsRevenue, nil

}

func (db *DataBaseManager) GetContractPlanRevenue(entityId int) (map[int]float64, error) {

	rows, err := db.connection.Query(context.Background(), `select v.cycle_number, sum(ve.amount) from vouchers v join voucher_entries ve on ve.voucher_id = v.id 
	where v.entity_id = $1 and (v.manually_paid is true or v.payment_id is not null) and v.deleted is not true and ve.currency = 'CLP' and v.canceled is not true
	group by cycle_number`, entityId)

	if err != nil {
		return nil, err
	}

	periodsRevenue := make(map[int]float64)
	period := 0
	revenue := 0.0

	for rows.Next() {
		err = rows.Scan(&period, &revenue)
		if err != nil {
			return nil, err
		}
		periodsRevenue[period] = revenue

	}

	return periodsRevenue, nil

}

func (db *DataBaseManager) GetManualOnlinePaymentsRevenue(entityId int) (map[int]float64, error) {

	rows, err := db.connection.Query(context.Background(), `v.cycle_number, sum(ve.amount) from vouchers v join voucher_entries ve on ve.voucher_id = v.id 
	  where v.entity_id = $1 and v.canceled is not true and (v.manually_paid is true or v.payment_id is not null)
	  group by cycle_number`, entityId)

	if err != nil {
		return nil, err
	}

	periodsRevenue := make(map[int]float64)
	period := 0
	revenue := 0.0

	for rows.Next() {
		err = rows.Scan(&period, &revenue)
		if err != nil {
			return nil, err
		}
		periodsRevenue[period] = revenue

	}

	return periodsRevenue, nil

}

type CycleIncome struct {
	CycleNumber int
	Value       float64
}
type VoucherCmrrReportInfo struct {
	CycleNumber int     `json:"cycle_number"`
	EntityId    int     `json:"entity_id"`
	PropertyID  int     `json:"property_id"`
	Amount      float64 `json:"amount"`
	KAM         string  `json:"kam"`
	VoucherID   int     `json:"voucher_id"`
	Status      string  `json:"status"`
	Name        string  `json:"name"`
	PaymentDate string  `json:"payment_date"`
	TenantID    string  `json:"tenant_id"`
}

func (db *DataBaseManager) GetPlansAccountVoucherRevenue() (map[int]map[int]float64, []VoucherCmrrReportInfo, error) {

	rows, err := db.connection.Query(context.Background(), `select cycle_number, vouchers.id , vouchers.entity_id,
case when vouchers.payment_id is not null or vouchers.manually_paid then 'PAGADO' 
	when vouchers.canceled then 'CANCELADO' 
	when vouchers.expiration_date < now() then 'EXPIRADO' 
	else 'SENT' end, 
	round(sum(case when voucher_entries.currency = 'UF' and vouchers.payment_id is not null then amount*cd.value
	when voucher_entries.currency = 'UF' and vouchers.manual_status_set_at is not null then amount*cd2.value else amount end)/1.19), 
	case when vouchers.manually_paid then cast(cast(vouchers.manual_status_set_at as date) as varchar)
	when vouchers.payment_id is not null then cast(p.accounting_date as varchar) else '' end  
	from vouchers
	join voucher_entries on vouchers.id = voucher_entries.voucher_id
	left join payments p on p.id = vouchers.payment_id
	left join currency_data cd on cd.index_type = 'UF' and cd."date" = p.accounting_date
	left join currency_data cd2 on cd2.index_type = 'UF' and cd2."date" = CAST(TO_CHAR(vouchers.manual_status_set_at, 'YYYY/MM/DD') as date)
	where vouchers.deleted is not true and vouchers.tenant_id = 'a3f946f2-9ad1-4a74-a18a-3fa1e3909e6e'
	group by cycle_number, vouchers.entity_id, vouchers.id, p.accounting_date
	order by vouchers.entity_id, cycle_number`)

	if err != nil {
		return nil, nil, err
	}

	outMap := make(map[int]map[int]float64)
	vouchers := make([]VoucherCmrrReportInfo, 0)
	paymentDate := ""
	nCycle := 0
	nEntityId := 0
	nVoucherId := 0
	nStatus := ""
	nValue := 0.0

	for rows.Next() {
		err = rows.Scan(&nCycle, &nVoucherId, &nEntityId, &nStatus, &nValue, &paymentDate)
		if err != nil {
			return nil, nil, err
		}
		if outMap[nEntityId] == nil {
			outMap[nEntityId] = make(map[int]float64)
			outMap[nEntityId][nCycle] = nValue
		} else {
			outMap[nEntityId][nCycle] += nValue
		}

		vouchers = append(vouchers, VoucherCmrrReportInfo{CycleNumber: nCycle, EntityId: nEntityId, Amount: nValue, VoucherID: nVoucherId, Status: nStatus, PaymentDate: paymentDate})

	}

	return outMap, vouchers, nil

}

func (db *DataBaseManager) GetManualPaymentsAccountVoucherRevenue() (map[int]map[int]float64, error) {

	rows, err := db.connection.Query(context.Background(), `select cycle_number , vouchers.entity_id, round(sum(amount)/1.19) from vouchers
	join voucher_entries on vouchers.id = voucher_entries.voucher_id 
	where vouchers.deleted is not true and vouchers.tenant_id = '78b69f71-9589-4709-88ce-beaed48e3437'
	group by cycle_number, vouchers.entity_id 
	order by vouchers.entity_id, cycle_number`)

	if err != nil {
		return nil, err
	}

	outMap := make(map[int]map[int]float64)
	nCycle := 0
	nEntityId := 0
	nValue := 0.0

	for rows.Next() {
		err = rows.Scan(&nCycle, &nEntityId, &nValue)
		if err != nil {
			return nil, err
		}
		if outMap[nEntityId] == nil {
			outMap[nEntityId] = make(map[int]float64)
			outMap[nEntityId][nCycle] = nValue
		} else {
			outMap[nEntityId][nCycle] = nValue
		}

	}

	return outMap, nil

}

func (db *DataBaseManager) GetCatchmentsAndBrokertiesAccountVoucherRevenue() (map[int]map[int]float64, map[int]map[int]float64, error) {

	rows, err := db.connection.Query(context.Background(), `select cycle_number , vouchers.entity_id, round(sum(amount)/1.19), count(distinct(vouchers)) from vouchers
	join voucher_entries on vouchers.id = voucher_entries.voucher_id 
	where vouchers.deleted is not true and vouchers.tenant_id = '5d05592c-e05a-45ab-b66f-57c87da4f123'
	group by cycle_number, vouchers.entity_id 
	order by vouchers.entity_id, cycle_number`)

	if err != nil {
		return nil, nil, err
	}

	outMap := make(map[int]map[int]float64)
	counterOutMap := make(map[int]map[int]float64)
	nCycle := 0
	nEntityId := 0
	nValue := 0.0
	nVouchers := 0

	for rows.Next() {
		err = rows.Scan(&nCycle, &nEntityId, &nValue, &nVouchers)
		if err != nil {
			return nil, nil, err
		}
		if outMap[nEntityId] == nil {
			outMap[nEntityId] = make(map[int]float64)
			outMap[nEntityId][nCycle] = nValue
		} else {
			outMap[nEntityId][nCycle] = nValue
		}
		if counterOutMap[nEntityId] == nil {
			counterOutMap[nEntityId] = make(map[int]float64)
			counterOutMap[nEntityId][nCycle] = float64(nVouchers)
		} else {
			counterOutMap[nEntityId][nCycle] = float64(nVouchers)
		}

	}

	return outMap, counterOutMap, nil

}

func (db *DataBaseManager) GetTenantPropertiesMap() (map[string]map[int]float64, error) {

	rows, err := db.connection.Query(context.Background(), `WITH AllCycles AS (
		SELECT generate_series(
				 (SELECT (EXTRACT(YEAR FROM MIN(created_at)) * 12 + EXTRACT(MONTH FROM MIN(created_at)) - 1) FROM properties),
				 (SELECT (EXTRACT(YEAR FROM MAX(created_at)) * 12 + EXTRACT(MONTH FROM MAX(created_at)) - 1) FROM properties)  ,
				 1
			   ) AS cycle_number
	  )
	  SELECT
	  ac.cycle_number, p.tenant_id,
	  (count(p.id)) AS cumulative_properties
	  from AllCycles ac
	  LEFT JOIN properties p ON EXTRACT(YEAR FROM p.created_at) * 12 + EXTRACT(MONTH FROM p.created_at) - 1 <= ac.cycle_number
	  where p.deleted is not true and p.archived is not true 
	  GROUP BY ac.cycle_number, p.tenant_id `)

	if err != nil {
		return nil, err
	}

	outMap := make(map[string]map[int]float64)
	nCycle := 0
	nTenantId := ""
	nValue := 0

	for rows.Next() {
		err = rows.Scan(&nCycle, &nTenantId, &nValue)
		if err != nil {
			return nil, err
		}
		if outMap[nTenantId] == nil {
			outMap[nTenantId] = make(map[int]float64)
			outMap[nTenantId][nCycle] = float64(nValue)
		} else {
			outMap[nTenantId][nCycle] = float64(nValue)
		}

	}

	return outMap, nil

}

func (db *DataBaseManager) GetTenantContractsMap() (map[string]map[int]float64, error) {

	rows, err := db.connection.Query(context.Background(), `WITH AllCycles AS (
		SELECT generate_series(
				 (SELECT (EXTRACT(YEAR FROM MIN(created_at)) * 12 + EXTRACT(MONTH FROM MIN(created_at)) - 1) FROM contracts),
				 (SELECT (EXTRACT(YEAR FROM MAX(created_at)) * 12 + EXTRACT(MONTH FROM MAX(created_at)) - 1) FROM contracts)  ,
				 1
			   ) AS cycle_number
	  )
	  SELECT
	  ac.cycle_number, c.tenant_id,
	  (count(c.id)) AS cumulative_contracts
	  from AllCycles ac
	  LEFT JOIN contracts c ON EXTRACT(YEAR FROM c.created_at) * 12 + EXTRACT(MONTH FROM c.created_at) - 1 <= ac.cycle_number
	  join properties p on p.id = c.property_id
	  where c.enabled 
	  GROUP BY ac.cycle_number, c.tenant_id`)

	if err != nil {
		return nil, err
	}

	outMap := make(map[string]map[int]float64)
	nCycle := 0
	nTenantId := ""
	nValue := 0

	for rows.Next() {
		err = rows.Scan(&nCycle, &nTenantId, &nValue)
		if err != nil {
			return nil, err
		}
		if outMap[nTenantId] == nil {
			outMap[nTenantId] = make(map[int]float64)
			outMap[nTenantId][nCycle] = float64(nValue)
		} else {
			outMap[nTenantId][nCycle] = float64(nValue)
		}

	}

	return outMap, nil

}

func SeparateRolIntoTenants(taxId string) ([]string, error) {
	tenants := strings.Split(taxId, "','")
	for i, tenant := range tenants {
		tenants[i] = strings.Trim(tenant, "'")
		if len(tenants[i]) != 36 {
			return nil, ErrSeparatingTenants
		}
	}
	log.Println(tenants)
	return tenants, nil
}

var ErrSeparatingTenants error = errors.New("tax id tenant in bad format")

func (db *DataBaseManager) GetLeasityOnlinePaymentsByTenant() (LeasityPayments map[string]map[int]float64,
	CatchmentPayments map[string]map[int]float64,
	AccruedPayments map[string]map[int]float64,
	AccruedCatchemts map[string]map[int]float64,
	AccruedAmounts map[string]map[int]float64,
	err error) {

	rows, err := db.connection.Query(context.Background(), `select v.cycle_number, v.tenant_id , round(sum(CASE WHEN v.leasity_fee_type is not null 
		THEN (case when v.leasity_fee_type = 'CLP' then v.leasity_fee_value/1.19 else ps.amount*v.leasity_fee_value*0.01/1.19 end) ELSE 0 END)),
		round(sum(ps.amount)),
		count(p.id), coalesce(v.leasity_fee_type, '') 
		from vouchers v
		join payments p on v.payment_id  = p.id
		join payment_sessions ps on ps.id = p.id
		where payment_id is not null and v.canceled is not true and p.reversed is null
		group by cycle_number, tenant_id, v.leasity_fee_type`)

	if err != nil {
		return nil, nil, nil, nil, nil, err
	}

	outMap := make(map[string]map[int]float64)
	catchmentOutMap := make(map[string]map[int]float64)
	accruedPaymentsOutMap := make(map[string]map[int]float64)
	accruedAmountsOutMap := make(map[string]map[int]float64)
	accruedCatchmentsOutMap := make(map[string]map[int]float64)
	nFeeType := ""
	nPayments := 0
	nCycle := 0
	nTenantId := ""
	nValue := 0.0
	accruedAmount := 0.0

	for rows.Next() {
		err = rows.Scan(&nCycle, &nTenantId, &nValue, &accruedAmount, &nPayments, &nFeeType)
		if err != nil {
			return nil, nil, nil, nil, nil, err
		}

		switch nFeeType {
		case "CLP":
			if outMap[nTenantId] == nil {
				outMap[nTenantId] = make(map[int]float64)
				outMap[nTenantId][nCycle] = nValue
			} else {
				outMap[nTenantId][nCycle] = nValue
			}
		case "RATE":
			if catchmentOutMap[nTenantId] == nil {
				catchmentOutMap[nTenantId] = make(map[int]float64)
				catchmentOutMap[nTenantId][nCycle] = nValue
			} else {
				catchmentOutMap[nTenantId][nCycle] = nValue
			}
			if accruedCatchmentsOutMap[nTenantId] == nil {
				accruedCatchmentsOutMap[nTenantId] = make(map[int]float64)
				accruedCatchmentsOutMap[nTenantId][nCycle] = float64(nPayments)
			} else {
				accruedCatchmentsOutMap[nTenantId][nCycle] += float64(nPayments)
			}
		}
		if accruedPaymentsOutMap[nTenantId] == nil {
			accruedPaymentsOutMap[nTenantId] = make(map[int]float64)
			accruedPaymentsOutMap[nTenantId][nCycle] = float64(nPayments)
		} else {
			accruedPaymentsOutMap[nTenantId][nCycle] += float64(nPayments)
		}
		if accruedAmountsOutMap[nTenantId] == nil {
			accruedAmountsOutMap[nTenantId] = make(map[int]float64)
			accruedAmountsOutMap[nTenantId][nCycle] = float64(accruedAmount)
		} else {
			accruedAmountsOutMap[nTenantId][nCycle] += float64(accruedAmount)
		}

	}

	return outMap, catchmentOutMap, accruedPaymentsOutMap, accruedCatchmentsOutMap, accruedAmountsOutMap, nil

}

func (db *DataBaseManager) GetFirmClientTenants() (map[int]Client, error) {

	rows, err := db.connection.Query(context.Background(), `select distinct coalesce(p.tax_id,''), c.id, pt.description from properties p 
	join contracts c on c.property_id = p.id 
	join properties_property_tags ppt on ppt.property_id = p.id
	join property_tags pt on pt.id = ppt.property_tag_id
	where p.tenant_id = '6b28c8ac-1007-4fae-9d11-5e92c76953aa'`)

	if err != nil {
		return nil, err
	}

	ntenant := ""
	nEntityId := 0
	tag := ""

	outMap := make(map[int]Client)

	for rows.Next() {
		err = rows.Scan(&ntenant, &nEntityId, &tag)
		if err != nil {
			return nil, err
		}
		if ntenant != "" {
			outMap[nEntityId] = Client{TenantId: ntenant, EntityId: nEntityId, Type: tag}
		}

	}

	return outMap, nil

}

func (db *DataBaseManager) GetInsurancePoliciesClientTenants() (map[int]Client, error) {

	rows, err := db.connection.Query(context.Background(), `select distinct coalesce(p.tax_id,''), c.id, pt.description from properties p 
	join contracts c on c.property_id = p.id 
	join properties_property_tags ppt on ppt.property_id = p.id
	join property_tags pt on pt.id = ppt.property_tag_id
	where p.tenant_id = '066c49ff-a7c3-49cc-96bf-e22b470c9f3a'`)

	if err != nil {
		return nil, err
	}

	ntenant := ""
	nEntityId := 0
	tag := ""

	outMap := make(map[int]Client)

	for rows.Next() {
		err = rows.Scan(&ntenant, &nEntityId, &tag)
		if err != nil {
			return nil, err
		}

		outMap[nEntityId] = Client{TenantId: ntenant, EntityId: nEntityId, Type: tag}
	}

	return outMap, nil

}

func (db *DataBaseManager) GetFirmsAccountVoucherRevenue() (map[int]map[int]float64, error) {

	rows, err := db.connection.Query(context.Background(), `select cycle_number, vouchers.entity_id , round(sum(amount)/1.19) from vouchers
	join voucher_entries on vouchers.id = voucher_entries.voucher_id 
	where vouchers.deleted is not true and vouchers.canceled is not true and currency = 'CLP' and vouchers.tenant_id = '6b28c8ac-1007-4fae-9d11-5e92c76953aa' 
	group by cycle_number, vouchers.entity_id 
	order by cycle_number`)

	if err != nil {
		return nil, err
	}

	outMap := make(map[int]map[int]float64)
	nCycle := 0
	nEntityId := 0
	nValue := 0.0

	for rows.Next() {
		err = rows.Scan(&nCycle, &nEntityId, &nValue)
		if err != nil {
			return nil, err
		}
		if outMap[nEntityId] == nil {
			outMap[nEntityId] = make(map[int]float64)
			outMap[nEntityId][nCycle] = nValue
		} else {
			outMap[nEntityId][nCycle] = nValue
		}

	}
	return outMap, nil
}

func (db *DataBaseManager) GetInsuranceAccountVoucherRevenue() (map[int]map[int]float64, map[int]map[int]float64, error) {

	rows, err := db.connection.Query(context.Background(), `select cycle_number, vouchers.entity_id , round(sum(amount)/1.19), count(distinct(vouchers))
	from vouchers
	join voucher_entries on vouchers.id = voucher_entries.voucher_id 
	where vouchers.canceled is not true and vouchers.deleted is not true and currency = 'CLP' and vouchers.tenant_id = '066c49ff-a7c3-49cc-96bf-e22b470c9f3a' 
	group by cycle_number, vouchers.entity_id  
	order by cycle_number`)

	if err != nil {
		return nil, nil, err
	}

	outMap := make(map[int]map[int]float64)
	countMap := make(map[int]map[int]float64)
	nCycle := 0
	nEntityId := 0
	nValue := 0.0
	policyNumber := 0

	for rows.Next() {
		err = rows.Scan(&nCycle, &nEntityId, &nValue, &policyNumber)
		if err != nil {
			return nil, nil, err
		}
		for cycle := nCycle; cycle <= nCycle+11; cycle++ {
			if outMap[nEntityId] == nil {
				outMap[nEntityId] = make(map[int]float64)
				outMap[nEntityId][cycle] = nValue / 12
			} else {
				outMap[nEntityId][cycle] += nValue / 12
			}
			if countMap[nEntityId] == nil {
				countMap[nEntityId] = make(map[int]float64)
				countMap[nEntityId][cycle] = float64(policyNumber)
			} else {
				countMap[nEntityId][cycle] += float64(policyNumber)
			}
		}

	}
	return outMap, countMap, nil
}
