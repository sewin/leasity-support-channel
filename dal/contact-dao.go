package dal

import (
	"context"
)

func (db *DataBaseManager) GetContactInfoByMailOrTaxId(rut, mail string) ([]ContactInfo, error) {

	rows, err := db.connection.Query(context.Background(), `select c.first_name, c.surname, c.mail, c.tenant_id, u2.username, (contracts.id is not null) as lessee, (p.id is not null) as owner,
	(u.id is not null) as admin, coalesce(ad.street,''), coalesce(ad.street_number,''), coalesce(ad.commune,''), coalesce(p.type,''), coalesce(ad.apartment,''), coalesce(ad.region,''), 
	c.id from contacts c
	left join contracts on contracts.lessee_id = c.id left join properties p on p.owner_id = c.id left join users u on u.contact_info_id = c.id 
	join users u2 on u2.id = c.tenant_id left join addresses ad on p.address_id = ad.id 
	where c.tax_id = $1 or c.mail = $2`, rut, mail)

	if err != nil {
		return nil, err
	}
	var ci ContactInfo
	var p Property

	//ps := make([]Property, 0)
	contacts := make([]ContactInfo, 0)
	cis := make([]int, 0)
	ncontacts := 0

	for rows.Next() {
		err = rows.Scan(&ci.ContactFirstName, &ci.ContactSurname, &ci.ContactMail, &ci.AdminTenantId, &ci.AdminUsername,
			&ci.IsLessee, &ci.IsOwner, &ci.IsAdmin, &p.Street, &p.StreetNumber, &p.Commune, &p.Type, &p.Apartment, &p.Region, &ci.ContactId)
		if err != nil {
			return nil, err
		}
		if !contains(cis, ci.ContactId) {
			contacts = append(contacts, ci)
			cis = append(cis, ci.ContactId)
			ncontacts++
		}
		if ci.IsOwner {
			contacts[ncontacts-1].Properties = append(contacts[ncontacts-1].Properties, p)
		}

	}

	return contacts, nil
}

type ContactInfo struct {
	ContactFirstName string     `json:"contact_first_name"`
	ContactSurname   string     `json:"contact_surname"`
	ContactMail      string     `json:"contact_mail"`
	TaxMail          string     `json:"tax_mail"`
	AdminTenantId    string     `json:"admin_tenant_id"`
	AdminUsername    string     `json:"admin_username"`
	IsLessee         bool       `json:"lessee"`
	IsOwner          bool       `json:"owner"`
	IsAdmin          bool       `json:"admin"`
	Properties       []Property `json:"properties,omitempty"`
	ContactId        int        `json:"contact_id"`
	PhoneNumber      string
	Nproperties      int
	KAM              string
	Plan             string
	LesseName        string
	AdressStreet     string
}

type Property struct {
	Street       string `json:"property_street"`
	StreetNumber string `json:"property_street_number"`
	Type         string `json:"property_type"`
	Apartment    string `json:"property_apartment"`
	Region       string `json:"property_region"`
	Commune      string `json:"property_commune"`
}

func (p Property) PropertyName() string {
	return p.Street + " " + p.StreetNumber + " " + PropertyType[p.Type] + " " + p.Apartment + ", " + Communes[p.Commune] + ", " + p.Region + "."
}

func contains(elems []int, v int) bool {
	for _, s := range elems {
		if v == s {
			return true
		}
	}
	return false
}

func (ci ContactInfo) ContactFullName() string {
	return ci.ContactFirstName + " " + ci.ContactSurname
}

func (ci ContactInfo) ContactRoles() string {
	out := ""
	if ci.IsAdmin {
		out += "Admin "
	}
	if ci.IsOwner {
		out += "Propietario "
	}
	if ci.IsLessee {
		out += "Arrendatario"
	}
	return out
}
