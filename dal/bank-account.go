package dal

import (
	"context"
	"errors"
)

func (db *DataBaseManager) GetBankAccountLastPaymentInfo(bankAccountId int) (*BankAccountRejectedPaymentInfo, error) {

	rows, err := db.connection.Query(context.Background(), `select sum(ve.amount), ve.voucher_id, 
	v.cycle_number, v.entity_id, 
	ba.account_number, ba.account_type, ba.full_name, ba.mail,
	ba.tax_id, ba.bank, ba.entity_id
	from voucher_entries ve 
	join vouchers v on v.id = ve.voucher_id
	join bank_accounts ba on ba.id = ve.bank_account_id 
	where ve.bank_account_id = $1
	group by ve.voucher_id, v.cycle_number, ba.account_number, 
	ba.account_type, ba.bank, ba.entity_id, ba.full_name, v.entity_id, ba.mail, ba.tax_id
	order by v.cycle_number desc limit 1`, bankAccountId)

	if err != nil {
		return nil, err
	}

	bankAccountInfo := BankAccountRejectedPaymentInfo{}

	for rows.Next() {
		err = rows.Scan(&bankAccountInfo.PendingPaymentAmount,
			&bankAccountInfo.VoucherID,
			&bankAccountInfo.VoucherCycle,
			&bankAccountInfo.ContractID,
			&bankAccountInfo.BankAccount.Number,
			&bankAccountInfo.BankAccount.Type,
			&bankAccountInfo.BankAccount.Name,
			&bankAccountInfo.BankAccount.Mail,
			&bankAccountInfo.BankAccount.TaxID,
			&bankAccountInfo.BankAccount.Bank,
			&bankAccountInfo.BankAccount.ContactID,
		)
		if err != nil {
			return nil, err
		}

	}

	if bankAccountInfo.VoucherID == 0 {
		return nil, errors.New("no payment for this bank account")
	}

	bankAccountInfo.BankAccountID = bankAccountId
	bankAccountInfo.BankAccount.ID = bankAccountId

	month := bankAccountInfo.VoucherCycle%12 + 1
	bankAccountInfo.PaymentMonth = Months[month]

	return &bankAccountInfo, nil
}

type BankAccountRejectedPaymentInfo struct {
	BankAccountID        int         `json:"bankAccountID"`
	VoucherID            int         `json:"voucherID"`
	ContractID           int         `json:"contractID"`
	PendingPaymentAmount float64     `json:"amount"`
	VoucherCycle         int         `json:"voucherCycle"`
	PaymentMonth         string      `json:"month"`
	AdminName            string      `json:"adminName"`
	AdminMail            string      `json:"adminMail"`
	ContactName          string      `json:"contactName"`
	PropertyAddress      string      `json:"address"`
	KAM                  string      `json:"kam"`
	BankAccount          BankAccount `json:"bankAccount"`
}

type BankAccount struct {
	ID        int    `json:"id"`
	Type      string `json:"type"`
	Number    string `json:"number"`
	Bank      string `json:"bank"`
	Name      string `json:"name"`
	Mail      string `json:"mail"`
	TaxID     string `json:"taxID"`
	ContactID int    `json:"contactID"`
}

func (db *DataBaseManager) GetBankAccountContactInfo(bi *BankAccountRejectedPaymentInfo) error {

	rows, err := db.connection.Query(context.Background(), `select coalesce(cu.first_name,''), coalesce(cu.surname,''), 
	coalesce(cu.mail,''), coalesce(c.first_name,''), coalesce(c.surname,''), 
	coalesce(a.street,''), coalesce(a.street_number,''),coalesce(a.apartment,''), coalesce(a.commune,''), coalesce(p.type,'')
	from contacts c 
	join users u on u.id = c.tenant_id
	join contacts cu on u.contact_info_id = cu.id
	join contracts ct on ct.id = $2
	join properties p on p.id = ct.property_id
	join addresses a on a.id = p.address_id 
	where c.id = $1`, bi.BankAccount.ContactID, bi.ContractID)

	if err != nil {
		return err
	}

	firstName := ""
	surname := ""
	contactFirstName := ""
	contactSurname := ""
	street := ""
	streetNumber := ""
	apartment := ""
	commune := ""
	propertyType := ""

	for rows.Next() {
		err = rows.Scan(&firstName,
			&surname,
			&bi.AdminMail,
			&contactFirstName,
			&contactSurname,
			&street,
			&streetNumber,
			&apartment,
			&commune,
			&propertyType,
		)
		if err != nil {
			return err
		}

	}

	bi.AdminName = firstName + " " + surname
	bi.ContactName = contactFirstName + " " + contactSurname
	address := ""

	if apartment == "" {
		address = street + " " + streetNumber + ", " + Communes[commune]
	} else {
		address = street + " " + streetNumber + " " + PropertyType[propertyType] + " " + apartment + ", " + Communes[commune]
	}

	bi.PropertyAddress = address

	return nil
}

func (db *DataBaseManager) GetContractKAM(bi *BankAccountRejectedPaymentInfo) error {

	rows, err := db.connection.Query(context.Background(), `select distinct coalesce(t.tagid,0) from contracts ct
	join properties p on ct.property_id = p.id  
	left outer join (select pt.description as tag, p.tax_id as rol, pt.id as tagid from properties p 
	left join properties_property_tags ppt on ppt.property_id = p.id
	left join property_tags pt on ppt.property_tag_id = pt.id) as t on t.rol = ct.tenant_id 
	where ct.id = $1`, bi.ContractID)
	if err != nil {
		return err
	}

	tagId := 0
	tagN := 0
	kam := ""

	for rows.Next() {
		err = rows.Scan(&tagId)
		if err != nil {
			return err
		}
		if KAMS[tagId] != "" && tagN < 1 {
			tagN++
			kam += KAMS[tagId]
		}

	}

	bi.KAM = kam

	return nil
}
