package dal

import (
	"context"
	"errors"
	"fmt"
	"log"

	"github.com/jackc/pgx/v4/pgxpool"
)

var ErrNoRowFound = errors.New("no rows match the query")
var ErrNoEnvVar = errors.New("some of the needed env variables are not setted")

type DataBaseManager struct {
	connection       *pgxpool.Pool
	host             string
	dbname           string
	connectionstring string
}

func NewDataBaseManager(host, user, password, dbname, dbport string) (*DataBaseManager, error) {
	if host == "" || user == "" || password == "" || dbname == "" || dbport == "" {
		return nil, ErrNoEnvVar
	}
	return &DataBaseManager{
		host:             host,
		dbname:           dbname,
		connectionstring: fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s", host, dbport, user, password, dbname),
	}, nil
}

func (manager *DataBaseManager) Connect() error {
	log.Println("Connecting to DB at: ", manager.host)
	var err error
	manager.connection, err = pgxpool.Connect(context.Background(), manager.connectionstring)
	if err != nil {
		log.Println("DB connection failed:", err.Error())
		return err
	}
	log.Println("Connected to DB: ", manager.dbname)
	return nil
}

func (manager *DataBaseManager) Close() {
	log.Println("Closing DB connection ...")
	manager.connection.Close()
}
