package updatenotificationcron

import (
	"log"
	"testing"

	"github.com/joho/godotenv"
	"leasity.cl/tools/support-channel/dal"
)

func TestRedisCache(t *testing.T) {
	err := godotenv.Load("local-testing.env")
	if err != nil {
		log.Fatalf("Some error occured. Err: %s", err)
	}
	dbm := dal.DataBaseManager{}
	cronChecker, err := NewBankAccountCronChecker(dbm, dbm, "C0592RHFPV3")
	if err != nil {
		t.Log(err)
		t.Fail()
	}

	bankAccountFixerInfo := dal.BankAccountRejectedPaymentInfo{BankAccountID: 1234, VoucherID: 12939, ContractID: 1245,
		BankAccount: dal.BankAccount{Type: "CC", Number: "12tc-ad12-11245235", TaxID: "18.620.781-5"}}

	err = cronChecker.AddBankAccountToChecker(&BankAccountInfo{BankAccountInfo: bankAccountFixerInfo, TS: ""})
	if err != nil {
		t.Log(err)
		t.Fail()
	}

	/* err = cronChecker.EraseBankAccount(1234)
	if err != nil {
		t.Log(err)
		t.Fail()
	} */

	gettedBankAccountInfo := make(map[int]BankAccountInfo)

	ok, err := cronChecker.bankAccouuntStore.Get("bank-accounts", &gettedBankAccountInfo)
	if !ok {
		t.Log(err)
		t.Fail()
	}
	log.Println(gettedBankAccountInfo)
}
