package updatenotificationcron

import (
	"errors"
	"log"
	"os"
	"time"

	"github.com/go-co-op/gocron"
	"github.com/philippgille/gokv"
	"github.com/philippgille/gokv/encoding"
	"github.com/philippgille/gokv/file"
	"leasity.cl/tools/support-channel/dal"
	"leasity.cl/tools/support-channel/slack"
)

type BankAccountUpdateChecker struct {
	scheduler         *gocron.Scheduler
	bankAccouuntStore gokv.Store
	bankAccounts      map[int]BankAccountInfo
	LeasityDB         dal.DataBaseManager
	PaymentsDB        dal.DataBaseManager
	SlackChannel      string
}

type BankAccountInfo struct {
	BankAccountInfo dal.BankAccountRejectedPaymentInfo `json:"bankAccountInfo"`
	TS              string                             `json:"ts"`
}

func NewBankAccountCronChecker(leasityDB, paymentsDB dal.DataBaseManager, slackChannel string) (*BankAccountUpdateChecker, error) {
	scheduler := gocron.NewScheduler(time.UTC)
	bankAccounts := make(map[int]BankAccountInfo)

	/* address := os.Getenv("BANK_ACCOUNT_FIXER_DB_ADDRESS")
	if address == "" {
		return nil, errors.New("bank account fixer db not setted")
	}

	user := os.Getenv("BANK_ACCOUNT_FIXER_DB_USER")
	if user == "" {
		return nil, errors.New("bank account fixer db not setted")
	}

	password := os.Getenv("BANK_ACCOUNT_FIXER_DB_PASSWORD")
	if password == "" {
		return nil, errors.New("bank account fixer db not setted")
	}

	name := os.Getenv("BANK_ACCOUNT_FIXER_DB_NAME")
	if name == "" {
		return nil, errors.New("bank account fixer db not setted")
	}

	var options2 = postgresql.Options{
		ConnectionURL: "postgres://" + user + ":" + password + "@" + address + "/" + name,
	}

	postgresClient, err := postgresql.NewClient(options2)
	if err != nil {
		return nil, err
	} */

	storeDirectory := os.Getenv("BANK_ACCOUNT_FIXER_FILE_DIRECTORY")
	if storeDirectory == "" {
		storeDirectory = "gokv"
	}

	var defaulFileNameExtension = "json"

	var fileOptions = file.Options{
		Directory:         storeDirectory,
		FilenameExtension: &defaulFileNameExtension,
		Codec:             encoding.JSON,
	}

	fileStoreClient, err := file.NewStore(fileOptions)
	if err != nil {
		return nil, err
	}

	/* options := redis.Options{}
	address := os.Getenv("REDIS_DB_ADDRESS")
	if address == "" {
		return nil, errors.New("redis address not setted")
	}
	password := os.Getenv("REDIS_DB_PASSWORD")
	if password == "" {
		return nil, errors.New("REDIS_DB_PASSWORD")
	}
	options.Address = address
	options.DB = 0
	options.Password = password

	client, err := redis.NewClient(options)
	if err != nil {
		log.Println(err)
		return nil, err
	} */

	bankAccountChecker := BankAccountUpdateChecker{scheduler: scheduler, bankAccounts: bankAccounts, LeasityDB: leasityDB, PaymentsDB: paymentsDB, SlackChannel: slackChannel, bankAccouuntStore: fileStoreClient}

	return &bankAccountChecker, nil
}

func (s *BankAccountUpdateChecker) StartFromCache() error {
	gettedBankAccountInfo := make(map[int]BankAccountInfo)

	ok, err := s.bankAccouuntStore.Get("bank-accounts", &gettedBankAccountInfo)
	if !ok || err != nil {
		return errors.Join(errors.New("error setting bank accounts from redis"), err)
	}
	s.bankAccounts = gettedBankAccountInfo
	return nil
}

func (s *BankAccountUpdateChecker) CheckBankAccountsCronJob() {
	s.scheduler.Every(30).Minutes().Do(func() {
		log.Println("Running cron:", s.bankAccounts)
		for i, ba := range s.bankAccounts {
			updatedBankAccount, err := s.PaymentsDB.GetBankAccountLastPaymentInfo(i)
			if err != nil {
				continue
			}
			if checkBankAccountChange(*updatedBankAccount, ba.BankAccountInfo) {
				sc := slack.NewSlackChannel(s.SlackChannel, "")
				body, err := sc.SendBankAccountUpdateToThread(ba.TS, *updatedBankAccount)
				if err != nil {
					log.Println("Error generating account update to thread" + string(body))
					continue
				}
				err = sc.SendSlackMessage(body)
				if err != nil {
					log.Println("Error sending account update to thread " + err.Error())
					continue
				}
				err = sc.ReactToSlackMessage(ba.TS, "white_check_mark")
				if err != nil {
					log.Println("Error reacting to thread " + err.Error())
					continue
				}
				err = s.EraseBankAccount(ba.BankAccountInfo.BankAccountID)
				if err != nil {
					log.Println("Error erasing bank account from checker " + err.Error())
					continue
				}
				err = s.bankAccouuntStore.Set("bank-accounts", s.bankAccounts)
				if err != nil {
					log.Println(errors.Join(errors.New("error setting bank accounts from redis"), err))
					continue
				}
			}
		}
	})
	s.scheduler.StartBlocking()
}

func (s *BankAccountUpdateChecker) AddBankAccountToChecker(bi *BankAccountInfo) error {
	s.bankAccounts[bi.BankAccountInfo.BankAccountID] = *bi

	err := s.bankAccouuntStore.Set("bank-accounts", s.bankAccounts)
	if err != nil {
		return errors.Join(errors.New("error setting bank accounts from redis"), err)
	}

	return nil
}

func (s *BankAccountUpdateChecker) EraseBankAccount(bankAccountID int) error {
	if _, ok := s.bankAccounts[bankAccountID]; ok {
		delete(s.bankAccounts, bankAccountID)
	} else {
		return errors.New("bank account not in fixer")
	}

	err := s.bankAccouuntStore.Set("bank-accounts", s.bankAccounts)
	if err != nil {
		return errors.Join(errors.New("error setting bank account in redis"), err)
	}

	return nil
}

func checkBankAccountChange(ba1, ba2 dal.BankAccountRejectedPaymentInfo) bool {
	out := ba1.BankAccount.Number != ba2.BankAccount.Number || ba1.BankAccount.Type != ba2.BankAccount.Type || ba1.BankAccount.Bank != ba2.BankAccount.Bank ||
		ba1.BankAccount.TaxID != ba2.BankAccount.TaxID || ba1.BankAccount.Name != ba2.BankAccount.Name || ba1.BankAccount.Mail != ba2.BankAccount.Mail
	return out
}
