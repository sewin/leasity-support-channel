package server

import (
	"encoding/json"
	"log"

	"github.com/valyala/fasthttp"
)

func GetKamInfo(ctx *fasthttp.RequestCtx) {

	clientsPlans, err := LeasityDB.GetPlansClientTenantsAndKams()
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	clientsPlanPayments, err := PaymentsDB.GetPlansVoucherPayments()
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	outBody := struct {
		ClientsPlans        interface{} `json:"clients_plans"`
		ClientsPlanPayments interface{} `json:"clients_plan_payments"`
	}{
		ClientsPlans:        clientsPlans,
		ClientsPlanPayments: clientsPlanPayments,
	}

	responsejson, err := json.Marshal(outBody)
	if err != nil {
		log.Println(err.Error())
		return
	}

	ctx.SetStatusCode(200)
	ctx.SetContentType("application/json")
	ctx.SetBody(responsejson)

}
