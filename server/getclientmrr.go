package server

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"log"
	"strconv"
	"time"

	"github.com/valyala/fasthttp"
	model "leasity.cl/tools/support-channel/models"
	"leasity.cl/tools/support-channel/slack"
	"leasity.cl/tools/support-channel/tools"
)

func GetClientMonthlyRecurringRevenue(ctx *fasthttp.RequestCtx) {

	timestamp, _ := strconv.Atoi(string(ctx.Request.Header.Peek("X-Slack-Request-Timestamp")))

	if (timestamp - int(time.Now().Unix())) > 60*3 {
		ctx.Error("Auth Expired", 400)
		CmrrCh <- true
		return
	}

	sig_basestr := "v0:" + strconv.Itoa(timestamp) + ":" + string(ctx.Request.Body())
	mac := hmac.New(sha256.New, []byte("bb59e57beda00ae9922724831c9e75f5"))

	mac.Write([]byte(sig_basestr))

	tokenResult := string(hex.EncodeToString(mac.Sum(nil)))
	verificationToken := "v0=" + tokenResult

	if !hmac.Equal([]byte(verificationToken), ctx.Request.Header.Peek("X-Slack-Signature")) {
		ctx.SetStatusCode(400)
		ctx.SetContentType("application/json")
		errorjson, _ := json.Marshal(SlackErrorMsg{ResponseType: "ephemeral", Text: "Not authorized"})
		ctx.SetBody(errorjson)
		CmrrCh <- true
		return
	}

	requestChannel := string(ctx.FormValue("channel_id"))

	slackChannel := "C05FV4JN0P5"
	/* if os.Getenv("TEST_SUPPORT_CHAN") != "" {
		slackChannel = os.Getenv("TEST_SUPPORT_CHAN")
	} */

	if requestChannel == "" || requestChannel != slackChannel {
		ctx.SetStatusCode(400)
		ctx.SetContentType("application/json")
		errorjson, _ := json.Marshal(SlackErrorMsg{ResponseType: "ephemeral", Text: "Not authorized"})
		ctx.SetBody(errorjson)
		CmrrCh <- true
		return
	}

	/* 	paymentsTenants, err := PaymentsDB.GetPaymentsClientTenants()
	   	if err != nil {
	   		log.Println(err.Error())
	   		ctx.Error(err.Error(), 500)
	   		CmrrCh <- true
	   		return
	   	} */

	ClientMRRReport := model.GetClientMonthlyRecurringRevenueReport()
	go func() {
		defer func() { CmrrCh <- true }()

		clientContractMap, currentContracts, err := LeasityDB.GetPlansClientTenants()
		if err != nil {
			log.Println(err.Error())
			ctx.Error(err.Error(), 500)
			CmrrCh <- true
			return
		}
		manualPaymentsContractMap, err := LeasityDB.GetManualPaymentsClientTenants()
		if err != nil {
			log.Println(err.Error())
			ctx.Error(err.Error(), 500)
			CmrrCh <- true
			return
		}
		catchmentsAndBrokertiesClientTenants, err := LeasityDB.GetCatchmentsAndBrokertiesClientTenants()
		if err != nil {
			log.Println(err.Error())
			ctx.Error(err.Error(), 500)
			CmrrCh <- true
			return
		}
		onlinePaymentsTenantMap, onlineCatchmentsTenantMap, accruedPaymentsMap, accruedCatchmentsMap, accruedAmountsMap, err := PaymentsDB.GetLeasityOnlinePaymentsByTenant()
		if err != nil {
			log.Println(err.Error())
			ctx.Error(err.Error(), 500)
			CmrrCh <- true
			return
		}

		revenuePlansMap, _, err := PaymentsDB.GetPlansAccountVoucherRevenue()
		if err != nil {
			log.Println(err.Error())
			ctx.Error(err.Error(), 500)
			CmrrCh <- true
			return
		}

		revenueManualPaymentsMap, err := PaymentsDB.GetManualPaymentsAccountVoucherRevenue()
		if err != nil {
			log.Println(err.Error())
			ctx.Error(err.Error(), 500)
			CmrrCh <- true
			return
		}

		catchmentAndBrokertiesRevenueMap, catchmentCounterMap, err := PaymentsDB.GetCatchmentsAndBrokertiesAccountVoucherRevenue()
		if err != nil {
			log.Println(err.Error())
			ctx.Error(err.Error(), 500)
			CmrrCh <- true
			return
		}

		firmsContractMap, err := LeasityDB.GetFirmClientTenants()
		if err != nil {
			log.Println(err.Error())
			ctx.Error(err.Error(), 500)
			CmrrCh <- true
			return
		}

		policiesContractMap, err := LeasityDB.GetInsurancePoliciesClientTenants()
		if err != nil {
			log.Println(err.Error())
			ctx.Error(err.Error(), 500)
			CmrrCh <- true
			return
		}

		propertiesMap, err := LeasityDB.GetTenantPropertiesMap()
		if err != nil {
			log.Println(err.Error())
			ctx.Error(err.Error(), 500)
			CmrrCh <- true
			return
		}

		contractsMap, err := LeasityDB.GetTenantContractsMap()
		if err != nil {
			log.Println(err.Error())
			ctx.Error(err.Error(), 500)
			CmrrCh <- true
			return
		}

		firmRevenueMap, err := PaymentsDB.GetFirmsAccountVoucherRevenue()
		if err != nil {
			log.Println(err.Error())
			ctx.Error(err.Error(), 500)
			CmrrCh <- true
			return
		}
		insuranceRevenueMap, policyCountMap, err := PaymentsDB.GetInsuranceAccountVoucherRevenue()
		if err != nil {
			log.Println(err.Error())
			ctx.Error(err.Error(), 500)
			CmrrCh <- true
			return
		}

		for cid, client := range clientContractMap {

			if ClientMRRReport.PlanClients[client.TenantId] == nil {
				ClientMRRReport.PlanClients[client.TenantId] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: client.TenantId, PlanEntityId: cid, Type: client.Type}
				ClientMRRReport.PlanClients[client.TenantId].PeriodValue = revenuePlansMap[cid]
			} else {
				ClientMRRReport.PlanClients[client.TenantId].PeriodValue = SumCycleContractRevenueMap(revenuePlansMap[cid], ClientMRRReport.PlanClients[client.TenantId].PeriodValue)
			}
		}

		for cid, client := range catchmentsAndBrokertiesClientTenants {
			switch client.Type {
			case "Recurrencia":
				if ClientMRRReport.CatchmentClients[client.TenantId] == nil {
					ClientMRRReport.CatchmentClients[client.TenantId] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: client.TenantId, PlanEntityId: cid, Type: client.Type}
					ClientMRRReport.CatchmentClients[client.TenantId].PeriodValue = catchmentAndBrokertiesRevenueMap[cid]
				} else {
					ClientMRRReport.CatchmentClients[client.TenantId].PeriodValue = SumCycleContractRevenueMap(catchmentAndBrokertiesRevenueMap[cid], ClientMRRReport.CatchmentClients[client.TenantId].PeriodValue)
				}
				if ClientMRRReport.AccruedCatchemts[client.TenantId] == nil {
					ClientMRRReport.AccruedCatchemts[client.TenantId] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: client.TenantId, Type: "N Recurrencias"}
					ClientMRRReport.AccruedCatchemts[client.TenantId].PeriodValue = catchmentCounterMap[client.EntityId]
				} else {
					ClientMRRReport.AccruedCatchemts[client.TenantId].PeriodValue = SumCycleContractRevenueMap(catchmentCounterMap[client.EntityId], ClientMRRReport.AccruedCatchemts[client.TenantId].PeriodValue)
				}
			case "Corretaje":
				if ClientMRRReport.BrokerClients[client.TenantId] == nil {
					ClientMRRReport.BrokerClients[client.TenantId] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: client.TenantId, PlanEntityId: cid, Type: "Corretajes"}
					ClientMRRReport.BrokerClients[client.TenantId].PeriodValue = catchmentAndBrokertiesRevenueMap[cid]
				} else {
					ClientMRRReport.BrokerClients[client.TenantId].PeriodValue = SumCycleContractRevenueMap(catchmentAndBrokertiesRevenueMap[cid], ClientMRRReport.BrokerClients[client.TenantId].PeriodValue)
				}
			}
		}

		for cid, client := range manualPaymentsContractMap {
			if ClientMRRReport.ManualPaymentsClients[client.TenantId] == nil {
				ClientMRRReport.ManualPaymentsClients[client.TenantId] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: client.TenantId, PlanEntityId: cid, Type: "Pagos manuales"}
				ClientMRRReport.ManualPaymentsClients[client.TenantId].PeriodValue = revenueManualPaymentsMap[cid]
			} else {
				ClientMRRReport.ManualPaymentsClients[client.TenantId].PeriodValue = SumCycleContractRevenueMap(revenueManualPaymentsMap[cid], ClientMRRReport.ManualPaymentsClients[client.TenantId].PeriodValue)
			}
		}

		for _, tenant := range currentContracts {
			if ClientMRRReport.Clients[tenant] == nil {
				ClientMRRReport.Clients[tenant] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: tenant, Type: "Propiedades"}
				ClientMRRReport.Clients[tenant].PeriodValue = propertiesMap[tenant]
			} else {
				ClientMRRReport.Clients[tenant].PeriodValue = propertiesMap[tenant]
			}
			if ClientMRRReport.Contracts[tenant] == nil {
				ClientMRRReport.Contracts[tenant] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: tenant, Type: "Contratos"}
				ClientMRRReport.Contracts[tenant].PeriodValue = contractsMap[tenant]
			} else {
				ClientMRRReport.Contracts[tenant].PeriodValue = contractsMap[tenant]
			}
		}

		for tenant := range onlinePaymentsTenantMap {
			if ClientMRRReport.Clients[tenant] == nil {
				ClientMRRReport.Clients[tenant] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: tenant, Type: "Propiedades pagos"}
				ClientMRRReport.Clients[tenant].PeriodValue = propertiesMap[tenant]
			}
			if ClientMRRReport.OnlinePaymentsClients[tenant] == nil {
				ClientMRRReport.OnlinePaymentsClients[tenant] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: tenant, Type: "Pagos online"}
				ClientMRRReport.OnlinePaymentsClients[tenant].PeriodValue = onlinePaymentsTenantMap[tenant]
			} else {
				ClientMRRReport.OnlinePaymentsClients[tenant].PeriodValue = SumCycleContractRevenueMap(onlinePaymentsTenantMap[tenant], ClientMRRReport.OnlinePaymentsClients[tenant].PeriodValue)
			}

		}
		for tenant := range accruedPaymentsMap {
			if ClientMRRReport.AccruedPayments[tenant] == nil {
				ClientMRRReport.AccruedPayments[tenant] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: tenant, Type: "Pagos devengados"}
				ClientMRRReport.AccruedPayments[tenant].PeriodValue = accruedPaymentsMap[tenant]
			} else {
				ClientMRRReport.AccruedPayments[tenant].PeriodValue = SumCycleContractRevenueMap(accruedPaymentsMap[tenant], ClientMRRReport.AccruedPayments[tenant].PeriodValue)
			}
			if ClientMRRReport.AccruedAmounts[tenant] == nil {
				ClientMRRReport.AccruedAmounts[tenant] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: tenant, Type: "Total devengado"}
				ClientMRRReport.AccruedAmounts[tenant].PeriodValue = accruedAmountsMap[tenant]
			} else {
				ClientMRRReport.AccruedAmounts[tenant].PeriodValue = SumCycleContractRevenueMap(accruedAmountsMap[tenant], ClientMRRReport.AccruedAmounts[tenant].PeriodValue)
			}
		}

		for tenant := range onlineCatchmentsTenantMap {
			if ClientMRRReport.CatchmentClients[tenant] == nil {
				ClientMRRReport.CatchmentClients[tenant] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: tenant, Type: "Recurrencia"}
				ClientMRRReport.CatchmentClients[tenant].PeriodValue = onlineCatchmentsTenantMap[tenant]
			} else {
				ClientMRRReport.CatchmentClients[tenant].PeriodValue = SumCycleContractRevenueMap(onlineCatchmentsTenantMap[tenant], ClientMRRReport.CatchmentClients[tenant].PeriodValue)
			}
			if ClientMRRReport.AccruedCatchemts[tenant] == nil {
				ClientMRRReport.AccruedCatchemts[tenant] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: tenant, Type: "N Recurrencias"}
				ClientMRRReport.AccruedCatchemts[tenant].PeriodValue = accruedCatchmentsMap[tenant]
			} else {
				ClientMRRReport.AccruedCatchemts[tenant].PeriodValue = SumCycleContractRevenueMap(accruedCatchmentsMap[tenant], ClientMRRReport.AccruedCatchemts[tenant].PeriodValue)
			}
		}

		for cid, client := range firmsContractMap {
			if ClientMRRReport.FirmClients[client.TenantId] == nil {
				ClientMRRReport.FirmClients[client.TenantId] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: client.TenantId, PlanEntityId: cid, Type: "Firmas"}
				ClientMRRReport.FirmClients[client.TenantId].PeriodValue = firmRevenueMap[cid]
			} else {
				ClientMRRReport.FirmClients[client.TenantId].PeriodValue = SumCycleContractRevenueMap(firmRevenueMap[cid], ClientMRRReport.FirmClients[client.TenantId].PeriodValue)
			}
		}

		for cid, client := range policiesContractMap {
			if ClientMRRReport.InsuranceClients[client.TenantId] == nil {
				ClientMRRReport.InsuranceClients[client.TenantId] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: client.TenantId, PlanEntityId: cid, Type: "Seguros"}
				ClientMRRReport.InsuranceClients[client.TenantId].PeriodValue = insuranceRevenueMap[cid]
			} else {
				ClientMRRReport.InsuranceClients[client.TenantId].PeriodValue = SumCycleContractRevenueMap2(insuranceRevenueMap[cid], ClientMRRReport.InsuranceClients[client.TenantId].PeriodValue)
			}
			if ClientMRRReport.InsuranceCounter[client.TenantId] == nil {
				ClientMRRReport.InsuranceCounter[client.TenantId] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: client.TenantId, PlanEntityId: cid, Type: "N Seguros"}
				ClientMRRReport.InsuranceCounter[client.TenantId].PeriodValue = policyCountMap[cid]
			} else {
				ClientMRRReport.InsuranceCounter[client.TenantId].PeriodValue = SumCycleContractRevenueMap2(policyCountMap[cid], ClientMRRReport.InsuranceCounter[client.TenantId].PeriodValue)
			}
		}

		err = tools.NewCsvForMrrReport("client-mrr", *ClientMRRReport)
		if err != nil {
			log.Println(err)
			return
		}

		err = slack.SendFileToSlackChannel("client-mrr.csv", "Client MRR", slackChannel)
		if err != nil {
			log.Println(err)
			return
		}
	}()

	responsejson, err := slack.ClientMrrSlackResponse()
	if err != nil {
		log.Println(err.Error())
		return
	}

	ctx.SetStatusCode(200)
	ctx.SetContentType("application/json")
	ctx.SetBody(responsejson)

}

func UsedThreatHandler(ctx *fasthttp.RequestCtx) {
	responsejson, err := slack.ClientMrrUsedChanError()
	if err != nil {
		log.Println(err.Error())
		return
	}

	ctx.SetStatusCode(200)
	ctx.SetContentType("application/json")
	ctx.SetBody(responsejson)
}

func SumCycleContractRevenueMap(in1, in2 map[int]float64) map[int]float64 {
	if len(in1) >= len(in2) {
		for cycle := range in1 {
			in1[cycle] += in2[cycle]
		}
		return in1
	} else {
		for cycle := range in2 {
			in2[cycle] += in1[cycle]
		}
		return in2
	}
}

func SumCycleContractRevenueMap2(in1, in2 map[int]float64) map[int]float64 {
	maxCycle := 0
	minCycle := 0
	out := make(map[int]float64)
	for maxCycle = range in1 {
		break
	}
	for minCycle = range in1 {
		break
	}
	for n := range in1 {
		if n > maxCycle {
			maxCycle = n
		}
		if n < minCycle {
			minCycle = n
		}
	}
	for n := range in2 {
		if n > maxCycle {
			maxCycle = n
		}
		if n < minCycle {
			minCycle = n
		}
	}

	for cycle := minCycle; cycle <= maxCycle; cycle++ {
		if in1[cycle]+in2[cycle] > 0 {
			out[cycle] = in1[cycle] + in2[cycle]
		}
	}
	return out
}

func GetClientMonthlyRecurringRevenueData(ctx *fasthttp.RequestCtx) {

	token := string(ctx.Request.Header.Peek("Token"))

	if token != "2bxYwNd8i4" {
		ctx.Error("Not authorized", 400)
		return
	}

	ClientMRRReport := model.GetClientMonthlyRecurringRevenueReport()

	clientContractMap, currentContracts, err := LeasityDB.GetPlansClientTenants()
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}
	manualPaymentsContractMap, err := LeasityDB.GetManualPaymentsClientTenants()
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}
	catchmentsAndBrokertiesClientTenants, err := LeasityDB.GetCatchmentsAndBrokertiesClientTenants()
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}
	onlinePaymentsTenantMap, onlineCatchmentsTenantMap, accruedPaymentsMap, accruedCatchmentsMap, accruedAmountsMap, err := PaymentsDB.GetLeasityOnlinePaymentsByTenant()
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	revenuePlansMap, planVouchers, err := PaymentsDB.GetPlansAccountVoucherRevenue()
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	plans, err := LeasityDB.GetPlansClientTenantsAndKams()
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	for k := range planVouchers {
		if plans[planVouchers[k].EntityId] != nil {
			planVouchers[k].TenantID = plans[planVouchers[k].EntityId].TenantID
			planVouchers[k].KAM = plans[planVouchers[k].EntityId].KAM
			planVouchers[k].Name = plans[planVouchers[k].EntityId].Name
			planVouchers[k].PropertyID = plans[planVouchers[k].EntityId].PropertyID
		}
	}

	ClientMRRReport.VoucherInformation = planVouchers

	revenueManualPaymentsMap, err := PaymentsDB.GetManualPaymentsAccountVoucherRevenue()
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	catchmentAndBrokertiesRevenueMap, catchmentCounterMap, err := PaymentsDB.GetCatchmentsAndBrokertiesAccountVoucherRevenue()
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	firmsContractMap, err := LeasityDB.GetFirmClientTenants()
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	policiesContractMap, err := LeasityDB.GetInsurancePoliciesClientTenants()
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	propertiesMap, err := LeasityDB.GetTenantPropertiesMap()
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	contractsMap, err := LeasityDB.GetTenantContractsMap()
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	firmRevenueMap, err := PaymentsDB.GetFirmsAccountVoucherRevenue()
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}
	insuranceRevenueMap, policyCountMap, err := PaymentsDB.GetInsuranceAccountVoucherRevenue()
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	for cid, client := range clientContractMap {

		if ClientMRRReport.PlanClients[client.TenantId] == nil {
			ClientMRRReport.PlanClients[client.TenantId] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: client.TenantId, PlanEntityId: cid, Type: client.Type}
			ClientMRRReport.PlanClients[client.TenantId].PeriodValue = revenuePlansMap[cid]
		} else {
			ClientMRRReport.PlanClients[client.TenantId].PeriodValue = SumCycleContractRevenueMap(revenuePlansMap[cid], ClientMRRReport.PlanClients[client.TenantId].PeriodValue)
		}
	}

	for cid, client := range catchmentsAndBrokertiesClientTenants {
		switch client.Type {
		case "Recurrencia":
			if ClientMRRReport.CatchmentClients[client.TenantId] == nil {
				ClientMRRReport.CatchmentClients[client.TenantId] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: client.TenantId, PlanEntityId: cid, Type: client.Type}
				ClientMRRReport.CatchmentClients[client.TenantId].PeriodValue = catchmentAndBrokertiesRevenueMap[cid]
			} else {
				ClientMRRReport.CatchmentClients[client.TenantId].PeriodValue = SumCycleContractRevenueMap(catchmentAndBrokertiesRevenueMap[cid], ClientMRRReport.CatchmentClients[client.TenantId].PeriodValue)
			}
			if ClientMRRReport.AccruedCatchemts[client.TenantId] == nil {
				ClientMRRReport.AccruedCatchemts[client.TenantId] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: client.TenantId, Type: "N Recurrencias"}
				ClientMRRReport.AccruedCatchemts[client.TenantId].PeriodValue = catchmentCounterMap[client.EntityId]
			} else {
				ClientMRRReport.AccruedCatchemts[client.TenantId].PeriodValue = SumCycleContractRevenueMap(catchmentCounterMap[client.EntityId], ClientMRRReport.AccruedCatchemts[client.TenantId].PeriodValue)
			}
		case "Corretaje":
			if ClientMRRReport.BrokerClients[client.TenantId] == nil {
				ClientMRRReport.BrokerClients[client.TenantId] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: client.TenantId, PlanEntityId: cid, Type: "Corretajes"}
				ClientMRRReport.BrokerClients[client.TenantId].PeriodValue = catchmentAndBrokertiesRevenueMap[cid]
			} else {
				ClientMRRReport.BrokerClients[client.TenantId].PeriodValue = SumCycleContractRevenueMap(catchmentAndBrokertiesRevenueMap[cid], ClientMRRReport.BrokerClients[client.TenantId].PeriodValue)
			}
		}
	}

	for cid, client := range manualPaymentsContractMap {
		if ClientMRRReport.ManualPaymentsClients[client.TenantId] == nil {
			ClientMRRReport.ManualPaymentsClients[client.TenantId] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: client.TenantId, PlanEntityId: cid, Type: "Pagos manuales"}
			ClientMRRReport.ManualPaymentsClients[client.TenantId].PeriodValue = revenueManualPaymentsMap[cid]
		} else {
			ClientMRRReport.ManualPaymentsClients[client.TenantId].PeriodValue = SumCycleContractRevenueMap(revenueManualPaymentsMap[cid], ClientMRRReport.ManualPaymentsClients[client.TenantId].PeriodValue)
		}
	}

	for _, tenant := range currentContracts {
		if ClientMRRReport.Clients[tenant] == nil {
			ClientMRRReport.Clients[tenant] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: tenant, Type: "Propiedades"}
			ClientMRRReport.Clients[tenant].PeriodValue = propertiesMap[tenant]
		} else {
			ClientMRRReport.Clients[tenant].PeriodValue = propertiesMap[tenant]
		}
		if ClientMRRReport.Contracts[tenant] == nil {
			ClientMRRReport.Contracts[tenant] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: tenant, Type: "Contratos"}
			ClientMRRReport.Contracts[tenant].PeriodValue = contractsMap[tenant]
		} else {
			ClientMRRReport.Contracts[tenant].PeriodValue = contractsMap[tenant]
		}
	}

	for tenant := range onlinePaymentsTenantMap {
		if ClientMRRReport.Clients[tenant] == nil {
			ClientMRRReport.Clients[tenant] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: tenant, Type: "Propiedades pagos"}
			ClientMRRReport.Clients[tenant].PeriodValue = propertiesMap[tenant]
		}
		if ClientMRRReport.OnlinePaymentsClients[tenant] == nil {
			ClientMRRReport.OnlinePaymentsClients[tenant] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: tenant, Type: "Pagos online"}
			ClientMRRReport.OnlinePaymentsClients[tenant].PeriodValue = onlinePaymentsTenantMap[tenant]
		} else {
			ClientMRRReport.OnlinePaymentsClients[tenant].PeriodValue = SumCycleContractRevenueMap(onlinePaymentsTenantMap[tenant], ClientMRRReport.OnlinePaymentsClients[tenant].PeriodValue)
		}

	}
	for tenant := range accruedPaymentsMap {
		if ClientMRRReport.AccruedPayments[tenant] == nil {
			ClientMRRReport.AccruedPayments[tenant] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: tenant, Type: "Pagos devengados"}
			ClientMRRReport.AccruedPayments[tenant].PeriodValue = accruedPaymentsMap[tenant]
		} else {
			ClientMRRReport.AccruedPayments[tenant].PeriodValue = SumCycleContractRevenueMap(accruedPaymentsMap[tenant], ClientMRRReport.AccruedPayments[tenant].PeriodValue)
		}
		if ClientMRRReport.AccruedAmounts[tenant] == nil {
			ClientMRRReport.AccruedAmounts[tenant] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: tenant, Type: "Total devengado"}
			ClientMRRReport.AccruedAmounts[tenant].PeriodValue = accruedAmountsMap[tenant]
		} else {
			ClientMRRReport.AccruedAmounts[tenant].PeriodValue = SumCycleContractRevenueMap(accruedAmountsMap[tenant], ClientMRRReport.AccruedAmounts[tenant].PeriodValue)
		}
	}

	for tenant := range onlineCatchmentsTenantMap {
		if ClientMRRReport.CatchmentClients[tenant] == nil {
			ClientMRRReport.CatchmentClients[tenant] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: tenant, Type: "Recurrencia"}
			ClientMRRReport.CatchmentClients[tenant].PeriodValue = onlineCatchmentsTenantMap[tenant]
		} else {
			ClientMRRReport.CatchmentClients[tenant].PeriodValue = SumCycleContractRevenueMap(onlineCatchmentsTenantMap[tenant], ClientMRRReport.CatchmentClients[tenant].PeriodValue)
		}
		if ClientMRRReport.AccruedCatchemts[tenant] == nil {
			ClientMRRReport.AccruedCatchemts[tenant] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: tenant, Type: "N Recurrencias"}
			ClientMRRReport.AccruedCatchemts[tenant].PeriodValue = accruedCatchmentsMap[tenant]
		} else {
			ClientMRRReport.AccruedCatchemts[tenant].PeriodValue = SumCycleContractRevenueMap(accruedCatchmentsMap[tenant], ClientMRRReport.AccruedCatchemts[tenant].PeriodValue)
		}
	}

	for cid, client := range firmsContractMap {
		if ClientMRRReport.FirmClients[client.TenantId] == nil {
			ClientMRRReport.FirmClients[client.TenantId] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: client.TenantId, PlanEntityId: cid, Type: "Firmas"}
			ClientMRRReport.FirmClients[client.TenantId].PeriodValue = firmRevenueMap[cid]
		} else {
			ClientMRRReport.FirmClients[client.TenantId].PeriodValue = SumCycleContractRevenueMap(firmRevenueMap[cid], ClientMRRReport.FirmClients[client.TenantId].PeriodValue)
		}
	}

	for cid, client := range policiesContractMap {
		if ClientMRRReport.InsuranceClients[client.TenantId] == nil {
			ClientMRRReport.InsuranceClients[client.TenantId] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: client.TenantId, PlanEntityId: cid, Type: "Seguros"}
			ClientMRRReport.InsuranceClients[client.TenantId].PeriodValue = insuranceRevenueMap[cid]
		} else {
			ClientMRRReport.InsuranceClients[client.TenantId].PeriodValue = SumCycleContractRevenueMap2(insuranceRevenueMap[cid], ClientMRRReport.InsuranceClients[client.TenantId].PeriodValue)
		}
		if ClientMRRReport.InsuranceCounter[client.TenantId] == nil {
			ClientMRRReport.InsuranceCounter[client.TenantId] = &model.ClientMonthlyRecurringRevenueInfo{TenantId: client.TenantId, PlanEntityId: cid, Type: "N Seguros"}
			ClientMRRReport.InsuranceCounter[client.TenantId].PeriodValue = policyCountMap[cid]
		} else {
			ClientMRRReport.InsuranceCounter[client.TenantId].PeriodValue = SumCycleContractRevenueMap2(policyCountMap[cid], ClientMRRReport.InsuranceCounter[client.TenantId].PeriodValue)
		}
	}

	responsejson, err := ClientMRRReport.MarshalJSON()
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	ctx.SetStatusCode(200)
	ctx.SetContentType("application/json")
	ctx.SetBody(responsejson)

}
