package server

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"log"
	"strconv"
	"time"

	"github.com/valyala/fasthttp"
	"leasity.cl/tools/support-channel/slack"
)

func SyncNethuntBillingInfo(ctx *fasthttp.RequestCtx) {
	timestamp, _ := strconv.Atoi(string(ctx.Request.Header.Peek("X-Slack-Request-Timestamp")))

	if (timestamp - int(time.Now().Unix())) > 60*3 {
		ctx.Error("Auth Expired", 400)
		CmrrCh <- true
		return
	}

	sig_basestr := "v0:" + strconv.Itoa(timestamp) + ":" + string(ctx.Request.Body())
	mac := hmac.New(sha256.New, []byte("bb59e57beda00ae9922724831c9e75f5"))

	mac.Write([]byte(sig_basestr))

	tokenResult := string(hex.EncodeToString(mac.Sum(nil)))
	verificationToken := "v0=" + tokenResult

	if !hmac.Equal([]byte(verificationToken), ctx.Request.Header.Peek("X-Slack-Signature")) {
		ctx.SetStatusCode(400)
		ctx.SetContentType("application/json")
		errorjson, _ := json.Marshal(SlackErrorMsg{ResponseType: "ephemeral", Text: "Not authorized"})
		ctx.SetBody(errorjson)
		CmrrCh <- true
		return
	}

	requestChannel := string(ctx.FormValue("channel_id"))

	slackChannel := "C04GLDHMSS1"

	if requestChannel == "" || requestChannel != slackChannel {
		ctx.SetStatusCode(400)
		ctx.SetContentType("application/json")
		errorjson, _ := json.Marshal(SlackErrorMsg{ResponseType: "ephemeral", Text: "Not authorized"})
		ctx.SetBody(errorjson)
		CmrrCh <- true
		return
	}

	request := fasthttp.AcquireRequest()

	request.SetRequestURI("https://internal.leasity.cl/nethunt/sync-plan-billing-records")
	request.Header.Set("Token", "2bxYwNd8i4")

	go func() {
		defer func() { CmrrCh <- true }()

		response := fasthttp.AcquireResponse()
		if err := fasthttp.Do(request, response); err != nil {
			return
		}
		log.Println(string(response.Body()))
	}()

	responsejson, err := slack.ClientNethuntSyncSlackResponse()
	if err != nil {
		log.Println(err.Error())
		return
	}

	ctx.SetStatusCode(200)
	ctx.SetContentType("application/json")
	ctx.SetBody(responsejson)

}

func SyncNethuntAccountInfo(ctx *fasthttp.RequestCtx) {
	timestamp, _ := strconv.Atoi(string(ctx.Request.Header.Peek("X-Slack-Request-Timestamp")))

	if (timestamp - int(time.Now().Unix())) > 60*3 {
		ctx.Error("Auth Expired", 400)
		CmrrCh <- true
		return
	}

	sig_basestr := "v0:" + strconv.Itoa(timestamp) + ":" + string(ctx.Request.Body())
	mac := hmac.New(sha256.New, []byte("bb59e57beda00ae9922724831c9e75f5"))

	mac.Write([]byte(sig_basestr))

	tokenResult := string(hex.EncodeToString(mac.Sum(nil)))
	verificationToken := "v0=" + tokenResult

	if !hmac.Equal([]byte(verificationToken), ctx.Request.Header.Peek("X-Slack-Signature")) {
		ctx.SetStatusCode(400)
		ctx.SetContentType("application/json")
		errorjson, _ := json.Marshal(SlackErrorMsg{ResponseType: "ephemeral", Text: "Not authorized"})
		ctx.SetBody(errorjson)
		CmrrCh <- true
		return
	}

	requestChannel := string(ctx.FormValue("channel_id"))

	slackChannel := "C04GLDHMSS1"

	if requestChannel == "" || requestChannel != slackChannel {
		ctx.SetStatusCode(400)
		ctx.SetContentType("application/json")
		errorjson, _ := json.Marshal(SlackErrorMsg{ResponseType: "ephemeral", Text: "Not authorized"})
		ctx.SetBody(errorjson)
		CmrrCh <- true
		return
	}

	request := fasthttp.AcquireRequest()

	request.SetRequestURI("https://internal.leasity.cl/nethunt/sync-records?recordsNumber=10000&minTutorialStatus=1")
	request.Header.Set("Token", "2bxYwNd8i4")

	go func() {
		defer func() { CmrrCh <- true }()

		response := fasthttp.AcquireResponse()
		if err := fasthttp.Do(request, response); err != nil {
			return
		}
		log.Println(string(response.Body()))
	}()

	responsejson, err := slack.ClientNethuntSyncSlackResponse()
	if err != nil {
		log.Println(err.Error())
		return
	}

	ctx.SetStatusCode(200)
	ctx.SetContentType("application/json")
	ctx.SetBody(responsejson)

}
