package server

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/valyala/fasthttp"
	model "leasity.cl/tools/support-channel/models"
	"leasity.cl/tools/support-channel/slack"
)

func GetVouchersInfo(ctx *fasthttp.RequestCtx) {
	timestamp, _ := strconv.Atoi(string(ctx.Request.Header.Peek("X-Slack-Request-Timestamp")))

	if (timestamp - int(time.Now().Unix())) > 60*3 {
		ctx.Error("Auth Expired", 400)
		return
	}

	sig_basestr := "v0:" + strconv.Itoa(timestamp) + ":" + string(ctx.Request.Body())
	mac := hmac.New(sha256.New, []byte("bb59e57beda00ae9922724831c9e75f5"))

	mac.Write([]byte(sig_basestr))

	tokenResult := string(hex.EncodeToString(mac.Sum(nil)))
	verificationToken := "v0=" + tokenResult

	if !hmac.Equal([]byte(verificationToken), ctx.Request.Header.Peek("X-Slack-Signature")) {
		ctx.SetStatusCode(400)
		ctx.SetContentType("application/json")
		errorjson, _ := json.Marshal(SlackErrorMsg{ResponseType: "ephemeral", Text: "Not authorized"})
		ctx.SetBody(errorjson)
		return
	}

	requestChannel := string(ctx.FormValue("channel_id"))

	slackChannel := "C04RG89RLQ7"

	if requestChannel == "" || requestChannel != slackChannel {
		ctx.SetStatusCode(400)
		ctx.SetContentType("application/json")
		errorjson, _ := json.Marshal(SlackErrorMsg{ResponseType: "ephemeral", Text: "Not authorized"})
		ctx.SetBody(errorjson)
		return
	}

	queryParam := string(ctx.FormValue("text"))

	if queryParam == "" {
		ctx.Error("Bad request: missing query param", 404)
	}

	VoucherId, err := strconv.Atoi(queryParam)
	if err != nil {
		ctx.Error("Bad request: wrong query param", 404)
	}

	VoucherInfo, err := PaymentsDB.GetVoucherInfo(VoucherId)
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	err = LeasityDB.GetTagAndPropertyInfo(VoucherInfo)
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	VoucherInfoModel, err := model.GetVoucherInfoAmount(*VoucherInfo)
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	responsejson, err := slack.SendVoucherInfo(VoucherInfoModel)
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	ctx.SetStatusCode(200)
	ctx.SetContentType("application/json")
	ctx.SetBody(responsejson)
}

func GetPropertyLastVoucherInfo(ctx *fasthttp.RequestCtx) {
	timestamp, _ := strconv.Atoi(string(ctx.Request.Header.Peek("X-Slack-Request-Timestamp")))

	if (timestamp - int(time.Now().Unix())) > 60*3 {
		ctx.Error("Auth Expired", 400)
		return
	}

	sig_basestr := "v0:" + strconv.Itoa(timestamp) + ":" + string(ctx.Request.Body())
	mac := hmac.New(sha256.New, []byte("bb59e57beda00ae9922724831c9e75f5"))

	mac.Write([]byte(sig_basestr))

	tokenResult := string(hex.EncodeToString(mac.Sum(nil)))
	verificationToken := "v0=" + tokenResult

	if !hmac.Equal([]byte(verificationToken), ctx.Request.Header.Peek("X-Slack-Signature")) {
		ctx.SetStatusCode(400)
		ctx.SetContentType("application/json")
		errorjson, _ := json.Marshal(SlackErrorMsg{ResponseType: "ephemeral", Text: "Not authorized"})
		ctx.SetBody(errorjson)
		return
	}

	requestChannel := string(ctx.FormValue("channel_id"))

	slackChannel := "C04RG89RLQ7"
	if os.Getenv("TEST_SUPPORT_CHAN") != "" {
		slackChannel = os.Getenv("TEST_SUPPORT_CHAN")
	}

	if requestChannel == "" || requestChannel != slackChannel {
		ctx.SetStatusCode(400)
		ctx.SetContentType("application/json")
		errorjson, _ := json.Marshal(SlackErrorMsg{ResponseType: "ephemeral", Text: "Not authorized"})
		ctx.SetBody(errorjson)
		return
	}

	queryParam := string(ctx.FormValue("text"))

	if queryParam == "" {
		ctx.Error("Bad request: missing query param", 404)
	}

	propertyID, err := strconv.Atoi(queryParam)
	if err != nil {
		ctx.Error("Bad request: wrong query param", 404)
	}

	contractID, err := LeasityDB.GetInsurancePropertyContractInfo(propertyID)
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	VoucherInfo, err := PaymentsDB.GetLastVoucherByContract(contractID)
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	VoucherInfoModel, err := model.GetVoucherInfoAmount(*VoucherInfo)
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	VoucherInfoModel.PropertyID = queryParam

	responsejson, err := slack.SendLastVoucherInfo(VoucherInfoModel)
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	ctx.SetStatusCode(200)
	ctx.SetContentType("application/json")
	ctx.SetBody(responsejson)
}
