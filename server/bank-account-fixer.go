package server

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"log"
	"strconv"
	"time"

	"github.com/valyala/fasthttp"
	"leasity.cl/tools/support-channel/slack"
	"leasity.cl/tools/support-channel/tools/sendgrid"
	updatenotificationcron "leasity.cl/tools/support-channel/update-notification-cron"
)

func BankAccountFixerHandler(ctx *fasthttp.RequestCtx) {

	timestamp, _ := strconv.Atoi(string(ctx.Request.Header.Peek("X-Slack-Request-Timestamp")))

	if (timestamp - int(time.Now().Unix())) > 60*3 {
		ctx.Error("Auth Expired", 400)
		return
	}

	sig_basestr := "v0:" + strconv.Itoa(timestamp) + ":" + string(ctx.Request.Body())
	mac := hmac.New(sha256.New, []byte("bb59e57beda00ae9922724831c9e75f5"))

	mac.Write([]byte(sig_basestr))

	tokenResult := string(hex.EncodeToString(mac.Sum(nil)))
	verificationToken := "v0=" + tokenResult

	if !hmac.Equal([]byte(verificationToken), ctx.Request.Header.Peek("X-Slack-Signature")) {
		ctx.SetStatusCode(400)
		ctx.SetContentType("application/json")
		errorjson, _ := json.Marshal(SlackErrorMsg{ResponseType: "ephemeral", Text: "Not authorized"})
		ctx.SetBody(errorjson)
		return
	}

	requestChannel := string(ctx.FormValue("channel_id"))

	if requestChannel == "" || requestChannel != BankAccountFixer.SlackChannel {
		ctx.SetStatusCode(400)
		ctx.SetContentType("application/json")
		errorjson, _ := json.Marshal(SlackErrorMsg{ResponseType: "ephemeral", Text: "Not authorized"})
		ctx.SetBody(errorjson)
		return
	}

	queryParam := string(ctx.FormValue("text"))

	bankAccountID, err := strconv.Atoi(queryParam)
	if err != nil {
		ctx.Error("Error with request query", 400)
		return
	}

	mailSender, _ := sendgrid.NewBankAccountFixerMailSender()

	bankAccountFixerInfo, err := PaymentsDB.GetBankAccountLastPaymentInfo(bankAccountID)
	if err != nil {
		log.Println("Error with request query bank account id:", bankAccountID, err)
		ctx.SetStatusCode(400)
		return
	}

	go func() {

		err = LeasityDB.GetBankAccountContactInfo(bankAccountFixerInfo)
		if err != nil {
			log.Println("Error with request query ", err)
			return
		}

		err = LeasityDB.GetContractKAM(bankAccountFixerInfo)
		if err != nil {
			log.Println("Error with request query ", err)
			return
		}

		slackChannel := slack.NewSlackChannel(requestChannel, "") // not passing the token here

		msg, err := slack.GenBankAccountFixBlocks(*bankAccountFixerInfo)
		if err != nil {
			log.Println("Error with request query ", err)
			return
		}

		ts, err := slackChannel.SendBlocksToSlackChannelReturningTs(msg, "Bank account fix request")
		if err != nil {
			log.Println("Error with request query ", err)
			return
		}

		bi := updatenotificationcron.BankAccountInfo{BankAccountInfo: *bankAccountFixerInfo, TS: ts}

		err = BankAccountFixer.AddBankAccountToChecker(&bi)
		if err != nil {
			log.Println("Error with request query ", err)
			return
		}

		err = mailSender.SendMail(bankAccountFixerInfo.AdminMail, *bankAccountFixerInfo)
		if err != nil {
			log.Println("Error with request query ", err)
			return
		}

	}()

	ctx.SetStatusCode(200)
	ctx.SetContentType("application/json")

}
