package server

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"log"
	"os"
	"sort"
	"strconv"
	"time"

	"github.com/valyala/fasthttp"
	model "leasity.cl/tools/support-channel/models"
	"leasity.cl/tools/support-channel/slack"
	"leasity.cl/tools/support-channel/tools"
)

func GetPaymentInfo(ctx *fasthttp.RequestCtx) {
	timestamp, _ := strconv.Atoi(string(ctx.Request.Header.Peek("X-Slack-Request-Timestamp")))

	if (timestamp - int(time.Now().Unix())) > 60*3 {
		ctx.Error("Auth Expired", 400)
		return
	}

	sig_basestr := "v0:" + strconv.Itoa(timestamp) + ":" + string(ctx.Request.Body())
	mac := hmac.New(sha256.New, []byte("bb59e57beda00ae9922724831c9e75f5"))

	mac.Write([]byte(sig_basestr))

	tokenResult := string(hex.EncodeToString(mac.Sum(nil)))
	verificationToken := "v0=" + tokenResult

	if !hmac.Equal([]byte(verificationToken), ctx.Request.Header.Peek("X-Slack-Signature")) {
		ctx.SetStatusCode(400)
		ctx.SetContentType("application/json")
		errorjson, _ := json.Marshal(SlackErrorMsg{ResponseType: "ephemeral", Text: "Not authorized"})
		ctx.SetBody(errorjson)
		return
	}

	requestChannel := string(ctx.FormValue("channel_id"))

	slackChannel := "C04RG89RLQ7"

	if requestChannel == "" || requestChannel != slackChannel {
		ctx.SetStatusCode(400)
		ctx.SetContentType("application/json")
		errorjson, _ := json.Marshal(SlackErrorMsg{ResponseType: "ephemeral", Text: "Not authorized"})
		ctx.SetBody(errorjson)
		return
	}

	queryParam := string(ctx.FormValue("text"))

	if queryParam == "" {
		ctx.Error("Bad request: missing query param", 404)
	}

	paymentId, err := strconv.Atoi(queryParam)
	if err != nil {
		ctx.Error("Bad request: wrong query param", 404)
	}

	PaymentInfo, err := PaymentsDB.GetPaymentInfo(paymentId)
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	err = LeasityDB.GetPaymentTagAndPropertyInfo(PaymentInfo)
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	PaymentInfoModel, err := model.GetPaymentInfoAmount(*PaymentInfo)
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	responsejson, err := slack.SendPaymentInfo(PaymentInfoModel)
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	ctx.SetStatusCode(200)
	ctx.SetContentType("application/json")
	ctx.SetBody(responsejson)
}

func GetUsedSignatures(ctx *fasthttp.RequestCtx) {
	timestamp, _ := strconv.Atoi(string(ctx.Request.Header.Peek("X-Slack-Request-Timestamp")))

	if (timestamp - int(time.Now().Unix())) > 60*3 {
		ctx.Error("Auth Expired", 400)
		return
	}

	sig_basestr := "v0:" + strconv.Itoa(timestamp) + ":" + string(ctx.Request.Body())
	mac := hmac.New(sha256.New, []byte("bb59e57beda00ae9922724831c9e75f5"))

	mac.Write([]byte(sig_basestr))

	tokenResult := string(hex.EncodeToString(mac.Sum(nil)))
	verificationToken := "v0=" + tokenResult

	if !hmac.Equal([]byte(verificationToken), ctx.Request.Header.Peek("X-Slack-Signature")) {
		ctx.SetStatusCode(400)
		ctx.SetContentType("application/json")
		errorjson, _ := json.Marshal(SlackErrorMsg{ResponseType: "ephemeral", Text: "Not authorized"})
		ctx.SetBody(errorjson)
		return
	}

	requestChannel := string(ctx.FormValue("channel_id"))

	slackChannel := "C05FV4JN0P5"
	if os.Getenv("TEST_SUPPORT_CHAN") != "" {
		slackChannel = os.Getenv("TEST_SUPPORT_CHAN")
	}

	if requestChannel == "" || requestChannel != slackChannel {
		ctx.SetStatusCode(400)
		ctx.SetContentType("application/json")
		errorjson, _ := json.Marshal(SlackErrorMsg{ResponseType: "ephemeral", Text: "Not authorized"})
		ctx.SetBody(errorjson)
		return
	}

	go func() {

		request := fasthttp.AcquireRequest()

		request.SetRequestURI("http://15.229.21.86:8087/internal/used-signatures")
		request.Header.Set("Token", "2bxYwNd8i4")
		response := fasthttp.AcquireResponse()
		if err := fasthttp.Do(request, response); err != nil {
			log.Println(err)
			return
		}

		Months := make([]model.MonthUsedSignatures, 0)

		json.Unmarshal(response.Body(), &Months)

		sort.Slice(Months, func(i, j int) bool {
			t1, _ := time.Parse("2006-01-02", Months[i].YearMonth+"-01")
			t2, _ := time.Parse("2006-01-02", Months[j].YearMonth+"-01")
			return t1.Before(t2)
		})

		sc := slack.NewSlackChannel(slackChannel, "")

		_, err := sc.SendUsedSignaturesInfo(Months)
		if err != nil {
			log.Println(err)
			return
		}

		err = tools.NewCsvForUsedSignatures("used-signatures", Months)
		if err != nil {
			log.Println(err)
			return
		}
		err = slack.SendFileToSlackChannel("used-signatures.csv", "Used Signatures", slackChannel)
		if err != nil {
			log.Println(err)
			return
		}

	}()

	ctx.SetStatusCode(200)

}
