package server

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/valyala/fasthttp"
	model "leasity.cl/tools/support-channel/models"
	"leasity.cl/tools/support-channel/slack"
	"leasity.cl/tools/support-channel/tools"
)

func GetTicketData(ctx *fasthttp.RequestCtx) {

	timestamp, _ := strconv.Atoi(string(ctx.Request.Header.Peek("X-Slack-Request-Timestamp")))

	if (timestamp - int(time.Now().Unix())) > 60*3 {
		ctx.Error("Auth Expired", 400)
		CmrrCh <- true
		return
	}

	sig_basestr := "v0:" + strconv.Itoa(timestamp) + ":" + string(ctx.Request.Body())
	mac := hmac.New(sha256.New, []byte("bb59e57beda00ae9922724831c9e75f5"))

	mac.Write([]byte(sig_basestr))

	tokenResult := string(hex.EncodeToString(mac.Sum(nil)))
	verificationToken := "v0=" + tokenResult

	if !hmac.Equal([]byte(verificationToken), ctx.Request.Header.Peek("X-Slack-Signature")) {
		ctx.SetStatusCode(400)
		ctx.SetContentType("application/json")
		errorjson, _ := json.Marshal(SlackErrorMsg{ResponseType: "ephemeral", Text: "Not authorized"})
		ctx.SetBody(errorjson)
		CmrrCh <- true
		return
	}

	requestChannel := string(ctx.FormValue("channel_id"))

	slackChannel := "C05L3BBUP37"
	if os.Getenv("TEST_SUPPORT_CHAN") != "" {
		slackChannel = os.Getenv("TEST_SUPPORT_CHAN")
	}

	if requestChannel == "" || requestChannel != slackChannel {
		ctx.SetStatusCode(400)
		ctx.SetContentType("application/json")
		errorjson, _ := json.Marshal(SlackErrorMsg{ResponseType: "ephemeral", Text: "Not authorized"})
		ctx.SetBody(errorjson)
		CmrrCh <- true
		return
	}

	cycleText := string(ctx.FormValue("text"))
	if cycleText == "" {
		ctx.Error("Bad request: missing query param", 404)
		CmrrCh <- true
		return
	}
	month, err := strconv.Atoi(cycleText[:2])
	if err != nil {
		ctx.Error("Bad request: wrong format", 404)
		CmrrCh <- true
		return
	}

	log.Println(month)
	year, err := strconv.Atoi(cycleText[3:])
	if err != nil {
		ctx.Error("Bad request: wrong format", 404)
		CmrrCh <- true
		return
	}

	cycleNumber := month + year*12 - 1

	log.Println(cycleNumber)

	go func() {
		defer func() { CmrrCh <- true }()

		tenants, err := LeasityDB.GetAllTimePlansClientTenants()
		if err != nil {
			ctx.Error("Internal error", 500)
		}

		contracts, err := LeasityDB.GetClientContractsByCommune(tenants)
		if err != nil {
			ctx.Error("Internal error", 500)
		}

		voucherAmounts, err := PaymentsDB.GetCycleVouchersRentAmount(cycleNumber)
		if err != nil {
			ctx.Error("Internal error", 500)
		}

		report, err := model.NewRentAverageReportFromContractAndVouchers(contracts, voucherAmounts)
		if err != nil {
			ctx.Error("Internal error", 500)
		}

		err = tools.NewCsvForAverageRentReport("rent-averages", report)
		if err != nil {
			log.Println(err)
			return
		}

		err = slack.SendFileToSlackChannel("rent-averages.csv", "Rent Averages", slackChannel)
		if err != nil {
			log.Println(err)
			return
		}

	}()

	responsejson, err := slack.ClientMrrSlackResponse()
	if err != nil {
		log.Println(err.Error())
		return
	}

	ctx.SetStatusCode(200)
	ctx.SetContentType("application/json")
	ctx.SetBody(responsejson)

}
