package server

import (
	"log"
	"os"

	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"
	"leasity.cl/tools/support-channel/dal"
	ratelimiter "leasity.cl/tools/support-channel/rate-limiter"
	updatenotificationcron "leasity.cl/tools/support-channel/update-notification-cron"
)

/* const (
	host     = "leasity-production-db.cyzb8xgnijnj.sa-east-1.rds.amazonaws.com"
	dbport   = 9623
	user     = "leasity_sebastian"
	password = "jE8?a5JP#AbqV7wr"
	dbname   = "leasity"
) */

var LeasityDB *dal.DataBaseManager
var PaymentsDB *dal.DataBaseManager
var AuthSlackChannel string
var CmrrCh chan bool
var BankAccountFixer *updatenotificationcron.BankAccountUpdateChecker

type FastHTTPServer struct {
	ListenAddr  string
	Router      *router.Router
	RateLimiter *ratelimiter.TokenLimiter
}

func NewFastHttpServer(addr string) *FastHTTPServer {
	r := router.New()

	return &FastHTTPServer{ListenAddr: addr, Router: r}
}

func (s *FastHTTPServer) Run() {
	log.Println("server listening")

	var err error

	LeasityDB, err = dal.NewDataBaseManager(os.Getenv("DB_HOST"), os.Getenv("DB_USER"), os.Getenv("DB_USER_PASSWORD"), os.Getenv("DB_NAME"), os.Getenv("DB_PORT"))
	if err != nil {

		log.Fatalln("Error connecting to leasity DB " + " : " + err.Error())
		return
	}

	PaymentsDB, err = dal.NewDataBaseManager(os.Getenv("DB_HOST"), os.Getenv("DB_USER"), os.Getenv("DB_USER_PASSWORD"), os.Getenv("PAYMENTS_DB_NAME"), os.Getenv("DB_PORT"))
	if err != nil {

		log.Fatalln("Error connecting to payments DB " + " : " + err.Error())
		return
	}

	if err := LeasityDB.Connect(); err != nil {
		log.Fatalf("Could not connect to")
		return
	}
	defer LeasityDB.Close()

	if err := PaymentsDB.Connect(); err != nil {
		log.Fatalf("Could not connect to")
		return
	}
	defer PaymentsDB.Close()

	s.RateLimiter = ratelimiter.NewRateLimiterPerMinute(2, 1)

	bankAccountFixChannel := os.Getenv("BANK_ACCOUNT_FIX_CHANNEL")
	if bankAccountFixChannel == "" {
		bankAccountFixChannel = "C07A2FK762X"
	}

	bankAccountFixer, err := updatenotificationcron.NewBankAccountCronChecker(*LeasityDB, *PaymentsDB, bankAccountFixChannel)
	if err != nil {
		log.Println("Could not connect to db", err)
		return
	}

	BankAccountFixer = bankAccountFixer

	err = BankAccountFixer.StartFromCache()
	if err != nil {
		log.Println("Could not start from cache, starting empty map")
	} else {
		log.Println("Started bank account fixer map from cache")
	}

	go BankAccountFixer.CheckBankAccountsCronJob()

	AuthSlackChannel = os.Getenv("AUTH_SLACK_CHANNEL")

	CmrrCh = make(chan bool, 1) //Canal para las rutinas
	CmrrCh <- true

	s.Router.POST("/api/get-contact-info", GetContactInfo)
	s.Router.POST("/api/get-address-info", GetAddressInfo)
	s.Router.POST("/api/get-plans-payments-info", s.RateLimiter.AddThreadLimiterMiddleware(GetLeasityPlansPaymentsInfo, UsedThreatHandler, CmrrCh))
	s.Router.POST("/api/get-voucher-info", GetVouchersInfo)
	s.Router.POST("/api/get-property-last-voucher-info", GetPropertyLastVoucherInfo)
	s.Router.POST("/api/get-payment-info", GetPaymentInfo)
	s.Router.POST("/api/get-used-signatures", GetUsedSignatures)
	s.Router.POST("/api/sync-nethunt-bills", s.RateLimiter.AddThreadLimiterMiddleware(SyncNethuntBillingInfo, UsedThreatHandler, CmrrCh))
	s.Router.POST("/api/sync-nethunt-accounts", s.RateLimiter.AddThreadLimiterMiddleware(SyncNethuntAccountInfo, UsedThreatHandler, CmrrCh))
	s.Router.POST("/api/get-client-monthly-recurring-revenue", s.RateLimiter.AddThreadLimiterMiddleware(GetClientMonthlyRecurringRevenue, UsedThreatHandler, CmrrCh))
	s.Router.POST("/api/get-average-ticket-data", s.RateLimiter.AddThreadLimiterMiddleware(GetTicketData, UsedThreatHandler, CmrrCh))
	s.Router.POST("/api/bank-account-fixer", BankAccountFixerHandler)

	s.Router.GET("/api/get-kam-ticket-data", GetKamInfo)
	s.Router.GET("/api/get-cmrr-data", GetClientMonthlyRecurringRevenueData)

	if err := fasthttp.ListenAndServe(s.ListenAddr, s.Router.Handler); err != nil {
		log.Fatalf("error in ListenAndServe: %s", err)
	}

}
