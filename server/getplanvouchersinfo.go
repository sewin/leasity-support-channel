package server

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/valyala/fasthttp"
	model "leasity.cl/tools/support-channel/models"
	"leasity.cl/tools/support-channel/slack"
	"leasity.cl/tools/support-channel/tools"
)

func GetLeasityPlansPaymentsInfo(ctx *fasthttp.RequestCtx) {
	timestamp, _ := strconv.Atoi(string(ctx.Request.Header.Peek("X-Slack-Request-Timestamp")))

	if (timestamp - int(time.Now().Unix())) > 60*3 {
		ctx.Error("Auth Expired", 400)
		CmrrCh <- true
		return
	}

	sig_basestr := "v0:" + strconv.Itoa(timestamp) + ":" + string(ctx.Request.Body())
	mac := hmac.New(sha256.New, []byte("bb59e57beda00ae9922724831c9e75f5"))

	mac.Write([]byte(sig_basestr))

	tokenResult := string(hex.EncodeToString(mac.Sum(nil)))
	verificationToken := "v0=" + tokenResult

	if !hmac.Equal([]byte(verificationToken), ctx.Request.Header.Peek("X-Slack-Signature")) {
		ctx.SetStatusCode(400)
		ctx.SetContentType("application/json")
		errorjson, _ := json.Marshal(SlackErrorMsg{ResponseType: "ephemeral", Text: "Not authorized"})
		ctx.SetBody(errorjson)
		CmrrCh <- true
		return
	}

	requestChannel := string(ctx.FormValue("channel_id"))

	slackChannel := "C04RG89RLQ7"
	if os.Getenv("TEST_SUPPORT_CHAN") != "" {
		slackChannel = os.Getenv("TEST_SUPPORT_CHAN")
	}

	if requestChannel == "" || requestChannel != slackChannel {
		ctx.SetStatusCode(400)
		ctx.SetContentType("application/json")
		errorjson, _ := json.Marshal(SlackErrorMsg{ResponseType: "ephemeral", Text: "Not authorized"})
		ctx.SetBody(errorjson)
		CmrrCh <- true
		return
	}

	today := time.Now().Format("2006-01-02")
	minusOneMonthToday := time.Now().AddDate(0, -1, 0).Format("2006-01-02")

	expiredAdmins, adminVoucherEntries, err := PaymentsDB.GetLeasityPlansVoucherEntries(today, minusOneMonthToday)
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		CmrrCh <- true
		return
	}

	paymentsNumber, paymentsAmounts, err := PaymentsDB.GetLeasityLastCyclePaymentsByTenant()
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		CmrrCh <- true
		return
	}

	expiredAdminsInfo, err := model.GetAdminExpiredPlansFromVoucherEntries(expiredAdmins, adminVoucherEntries, paymentsAmounts, paymentsNumber)
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		CmrrCh <- true
		return
	}

	clientContractMap, _, err := LeasityDB.GetPlansClientTenants()
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		CmrrCh <- true
		return
	}

	totalAmount := 0.0
	expiredVouchers := 0

	model.SortAdminExpiredPlanInfoTable(expiredAdminsInfo)

	go func() {
		defer func() { CmrrCh <- true }()
		for i := 0; i < len(expiredAdminsInfo); i++ {
			contactInfo, err := LeasityDB.GetAdminContactInfo(expiredAdminsInfo[i].EntityId)
			if err != nil {
				log.Println(err, " Admin ", expiredAdminsInfo[i].AdminId, " info not found")
				continue
			}
			expiredAdminsInfo[i].UpdateContactInfo(*contactInfo)
			expiredAdminsInfo[i].PaymentsAmount = paymentsAmounts[clientContractMap[expiredAdminsInfo[i].EntityId].TenantId]
			expiredAdminsInfo[i].PaymentsNumber = paymentsNumber[clientContractMap[expiredAdminsInfo[i].EntityId].TenantId]
			expiredAdminsInfo[i].TenantID = clientContractMap[expiredAdminsInfo[i].EntityId].TenantId
			expiredVouchers += len(expiredAdminsInfo[i].ExpiredVouchers)
			totalAmount += expiredAdminsInfo[i].ExpiredAmount
			log.Println("admin: " + expiredAdminsInfo[i].Admin.FullName() + " | expired vouchers: " + strconv.Itoa(len(expiredAdminsInfo[i].ExpiredVouchers)) + " total: " + strconv.Itoa(int(expiredAdminsInfo[i].ExpiredAmount)) + " | one month expired: " + strconv.Itoa(len(expiredAdminsInfo[i].OneMonthExpiredVouchers)) + " total: " + strconv.Itoa(int(expiredAdminsInfo[i].OneMonthExpiredAmount)) + " | payed vouchers: " + strconv.Itoa(len(expiredAdminsInfo[i].PayedVouchers)))
		}

		log.Println("total expired admins: "+strconv.Itoa(len(expiredAdminsInfo)), "-- total expired amount: ", totalAmount)

		err = tools.NewCsvFromExpiredAdmins("expiredAdmins", expiredAdminsInfo)
		if err != nil {
			log.Println(err)
			return
		}
		SlackChannel := slack.NewSlackChannel(slackChannel, "xoxb-981968212868-4348378383506-pmKxfL5etDcqxeXQzCRgukFc")
		responsejson, err := SlackChannel.SendExpiredAdminsInfo(expiredAdminsInfo, expiredVouchers, totalAmount)
		if err != nil {
			log.Println(err.Error())
			return
		}

		err = SlackChannel.SendSlackMessage(responsejson)
		if err != nil {
			log.Println(err)
			return
		}

		err = slack.SendFileToSlackChannel("expiredAdmins.csv", "Admins with expired vouchers", slackChannel)
		if err != nil {
			log.Println(err)
			return
		}

	}()

	responsejson, err := slack.ClientMrrSlackResponse()
	if err != nil {
		log.Println(err.Error())
		return
	}

	ctx.SetStatusCode(200)
	ctx.SetContentType("application/json")
	ctx.SetBody(responsejson)

}
