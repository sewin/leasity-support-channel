package server

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"log"
	"strconv"
	"time"

	"github.com/valyala/fasthttp"
	"leasity.cl/tools/support-channel/slack"
)

func GetContactInfo(ctx *fasthttp.RequestCtx) {

	timestamp, _ := strconv.Atoi(string(ctx.Request.Header.Peek("X-Slack-Request-Timestamp")))

	if (timestamp - int(time.Now().Unix())) > 60*3 {
		ctx.Error("Auth Expired", 400)
		return
	}

	sig_basestr := "v0:" + strconv.Itoa(timestamp) + ":" + string(ctx.Request.Body())
	mac := hmac.New(sha256.New, []byte("bb59e57beda00ae9922724831c9e75f5"))

	mac.Write([]byte(sig_basestr))

	tokenResult := string(hex.EncodeToString(mac.Sum(nil)))
	verificationToken := "v0=" + tokenResult

	if !hmac.Equal([]byte(verificationToken), ctx.Request.Header.Peek("X-Slack-Signature")) {
		ctx.SetStatusCode(400)
		ctx.SetContentType("application/json")
		errorjson, _ := json.Marshal(SlackErrorMsg{ResponseType: "ephemeral", Text: "Not authorized"})
		ctx.SetBody(errorjson)
		return
	}

	requestChannel := string(ctx.FormValue("channel_id"))

	if requestChannel == "" || requestChannel != AuthSlackChannel {
		ctx.SetStatusCode(400)
		ctx.SetContentType("application/json")
		errorjson, _ := json.Marshal(SlackErrorMsg{ResponseType: "ephemeral", Text: "Not authorized"})
		ctx.SetBody(errorjson)
		return
	}

	queryParam := string(ctx.FormValue("text"))

	if queryParam == "" {
		ctx.Error("Bad request: missing query param", 404)
	}

	queryTxt := "Query Arg: " + queryParam
	//querytest := string(ctx.Request.Body())
	/* 	log.Println(fgsg)
	   	queryMail := string(ctx.QueryArgs().Peek("mail"))
	   	if queryMail == "" {
	   		queryMail = "x"
	   	}
	   	queryTaxId := string(ctx.QueryArgs().Peek("rut"))
	   	if queryTaxId == "" {
	   		queryTaxId = "x"
	   	}
	   	if queryMail != "x" {
	   		queryTxt = "Mail: " + queryMail
	   	}
	   	if queryTaxId != "x" {
	   		queryTxt = "Rut: " + queryTaxId
	   	} */

	a, err := LeasityDB.GetContactInfoByMailOrTaxId(queryParam, queryParam)
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}

	SlackChannel := slack.NewSlackChannel("C03D0RU8XHC", "xoxb-981968212868-3745041018135-TXfmA3Qq8NVJzEIMWIeuMKz5")
	responsejson, err := SlackChannel.SendContactInfoToSlackChannel(a, queryTxt)
	if err != nil {
		log.Println(err.Error())
		return
	}
	ctx.SetStatusCode(200)
	ctx.SetContentType("application/json")
	ctx.SetBody(responsejson)

}

type SlackErrorMsg struct {
	ResponseType string `json:"response_type"`
	Text         string `json:"text"`
}
