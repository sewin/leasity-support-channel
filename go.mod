module leasity.cl/tools/support-channel

go 1.17

require (
	github.com/couchbase/gocb/v2 v2.6.3
	github.com/fasthttp/router v1.4.12
	github.com/go-co-op/gocron v1.37.0
	github.com/jackc/pgx/v4 v4.17.2
	github.com/joho/godotenv v1.5.1
	github.com/leekchan/accounting v1.0.0
	github.com/philippgille/gokv v0.7.0
	github.com/philippgille/gokv/encoding v0.7.0
	github.com/philippgille/gokv/file v0.7.0
	github.com/sendgrid/sendgrid-go v3.14.0+incompatible
	github.com/sirupsen/logrus v1.9.3
	github.com/valyala/fasthttp v1.41.0
	golang.org/x/time v0.3.0
)

require (
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/couchbase/gocbcore/v10 v10.2.3 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/google/uuid v1.4.0 // indirect
	github.com/jackc/chunkreader/v2 v2.0.1 // indirect
	github.com/jackc/pgconn v1.13.0 // indirect
	github.com/jackc/pgio v1.0.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgproto3/v2 v2.3.1 // indirect
	github.com/jackc/pgservicefile v0.0.0-20200714003250-2b9c44734f2b // indirect
	github.com/jackc/pgtype v1.12.0 // indirect
	github.com/jackc/puddle v1.3.0 // indirect
	github.com/klauspost/compress v1.15.9 // indirect
	github.com/lib/pq v1.10.9 // indirect
	github.com/philippgille/gokv/util v0.7.0 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/robfig/cron/v3 v3.0.1 // indirect
	github.com/savsgio/gotils v0.0.0-20220530130905-52f3993e8d6d // indirect
	github.com/sendgrid/rest v2.6.9+incompatible // indirect
	github.com/shopspring/decimal v1.2.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	golang.org/x/crypto v0.0.0-20220722155217-630584e8d5aa // indirect
	golang.org/x/sys v0.0.0-20220728004956-3c1f35247d10 // indirect
	golang.org/x/text v0.3.7 // indirect
)
