.PHONY: run build clean

run:
	go run main.go

build:
	go build -o bin/support-channel main.go

clean:
	rm -r bin