package slack

import (
	"log"
	"testing"

	"leasity.cl/tools/support-channel/dal"
)

func TestSendingSlackMsg(t *testing.T) {
	sc := NewSlackChannel("C0592RHFPV3", "")

	bankAccountFixerInfo := dal.BankAccountRejectedPaymentInfo{BankAccountID: 1234, VoucherID: 12939, ContractID: 1245,
		BankAccount: dal.BankAccount{Type: "CC", Number: "12tc-ad12-11245235", TaxID: "18.620.781-5"}}

	msg, err := GenBankAccountFixBlocks(bankAccountFixerInfo)
	if err != nil {
		log.Println("Error with request query ", err)
		return
	}

	ts, err := sc.SendBlocksToSlackChannelReturningTs(msg, "Bank account fix request")
	if err != nil {
		log.Println("Error with request query ", err)
		return
	}

	body, err := sc.SendBankAccountUpdateToThread(ts, bankAccountFixerInfo)
	if err != nil {
		log.Println("Error generating account update to thread" + string(body))
		t.Error("")
	}
	err = sc.SendSlackMessage(body)
	if err != nil {
		log.Println("Error sending account update to thread" + err.Error())

	}
	err = sc.ReactToSlackMessage(ts, "white_check_mark")
	if err != nil {
		log.Println("Error sending account update to thread" + err.Error())
	}

}
