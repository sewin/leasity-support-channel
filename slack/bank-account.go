package slack

import (
	"encoding/json"
	"strconv"

	"leasity.cl/tools/support-channel/dal"
	model "leasity.cl/tools/support-channel/models"
)

func GenBankAccountFixBlocks(bi dal.BankAccountRejectedPaymentInfo) ([]Block, error) {
	responseBlocks := make([]Block, 0)

	responseBlocks = append(responseBlocks, Block{Type: "section", Text: Text{Type: "mrkdwn", Text: ":arrow_forward: Se ha iniciado el flujo para solicitar la corrección de la cuenta de banco *" + strconv.Itoa(bi.BankAccount.ID) + "*"}})
	responseBlocks = append(responseBlocks, Block{Type: "divider"})

	voucherInfoText := "*Nombre del admin*: " + bi.AdminName + "\n\n *Admin Contact Email:* " + bi.AdminMail +
		" \n\n*KAM*: " + KAMUSERS[bi.KAM] + " \n\n *Last Voucher ID:* " + strconv.Itoa(bi.VoucherID) +
		"\n\n*Nombre*: " + bi.BankAccount.Name +
		"\n\n *Rut:* " + bi.BankAccount.TaxID + " \n\n*Número de cuenta*: " +
		bi.BankAccount.Number + " \n\n *Tipo:* " + bi.BankAccount.Type + " \n\n *Banco:* " + bi.BankAccount.Bank + " (SBIF: " + model.SBIFBanks[bi.BankAccount.Bank] + ")" +
		" \n\n *Mail:* " + bi.BankAccount.Mail

	adminContactInfo := Text{Type: "mrkdwn", Text: voucherInfoText}

	responseBlocks = append(responseBlocks, Block{Type: "section", Text: adminContactInfo})

	responseBlocks = append(responseBlocks, Block{Type: "divider"})
	responseBlocks = append(responseBlocks, Block{Type: "section", Text: Text{Type: "mrkdwn", Text: "Se notificará al correo " + bi.AdminMail + " para que cambie la cuenta de banco. Una vez se modifique la cuenta se notificará en el hilo de este mensaje"}})

	return responseBlocks, nil
}

func (sc *SlackChannel) SendBlocksToSlackChannelReturningTs(blocks []Block, text string) (string, error) {
	msg, err := json.Marshal(SlackMessage{Channel: sc.ChannelId, Text: text, Blocks: blocks})
	if err != nil {
		return "", err
	}

	ts, err := sc.SendSlackMessageReturningTs(msg)
	if err != nil {
		return "", err
	}

	return ts, nil
}

func (sc SlackChannel) SendBankAccountUpdateToThread(ts string, bi dal.BankAccountRejectedPaymentInfo) ([]byte, error) {

	responseBlocks := make([]Block, 0)

	responseBlocks = append(responseBlocks, Block{Type: "section", Text: Text{Type: "mrkdwn", Text: ":white_check_mark: La cuenta de banco " + strconv.Itoa(bi.BankAccount.ID) + " ha sido modificada :white_check_mark:"}})
	responseBlocks = append(responseBlocks, Block{Type: "divider"})

	bankAccountInfoText := "*Nombre*: " + bi.BankAccount.Name +
		"\n\n *Rut:* " + bi.BankAccount.TaxID + " \n\n*Número de cuenta*: " +
		bi.BankAccount.Number + " \n\n *Tipo:* " + bi.BankAccount.Type + " \n\n *Banco:* " + bi.BankAccount.Bank + " (SBIF: " + model.SBIFBanks[bi.BankAccount.Bank] + ")" +
		" \n\n *Mail:* " + bi.BankAccount.Mail

	bankAccountInfo := Text{Type: "mrkdwn", Text: bankAccountInfoText}

	responseBlocks = append(responseBlocks, Block{Type: "section", Text: bankAccountInfo})

	commandResponse, err := json.Marshal(SlackThreadMessage{Channel: sc.ChannelId, Text: "Bank account updated", Blocks: responseBlocks, ThreadTs: ts})
	if err != nil {
		return nil, err
	}

	return commandResponse, err
}

var KAMUSERS = map[string]string{
	"Luciano":  "<@U03LQ4Y5W1Z>",
	"Pipe":     "<@U03MXGVMN31>",
	"Tomás":    "<@UUWCZVBFX>",
	"Felipe":   "<@U02EWR1NBQV>",
	"Gigi":     "<@U04A6K9B78R>",
	"Melville": "<@U057VJ6SHCL>",
	"Sebe":     "<@U0573K1UPMZ>",
	"Nati":     "<@U06MBE7RT8F>",
	"":         "<@U04JE6NJ09F>",
	"Paolo":    "<@U07PW70FNAX>",
}
