package slack

import (
	"encoding/json"
	"log"

	"leasity.cl/tools/support-channel/dal"
)

func (sc SlackChannel) SendAddressInfoToSlackChannel(adresses []dal.AddressInfo, query string) ([]byte, error) {
	responseBlocks := make([]Block, 0)

	responseBlocks = append(responseBlocks, Block{Type: "header", Text: Text{Type: "plain_text", Text: query}})

	naddress := 0

	for i := 0; i < len(adresses) && naddress < 9; i++ {

		infoArray := make([]Text, 0)
		infoArray = append(infoArray, Text{Type: "mrkdwn", Text: "*" + adresses[i].Property.PropertyName() + "*"})
		responseBlocks = append(responseBlocks, Block{Type: "section", Fields: infoArray})

		ownerctc := make([]Text, 0)
		ownerctc = append(ownerctc, Text{Type: "mrkdwn", Text: "*Owner name:*\n " + adresses[i].OwnerName()})
		ownerctc = append(ownerctc, Text{Type: "mrkdwn", Text: "*Owner mail:*\n " + adresses[i].OwnerMail})
		ownerctc = append(ownerctc, Text{Type: "mrkdwn", Text: "*Owner tax id:*\n " + adresses[i].OwnerTaxId})

		responseBlocks = append(responseBlocks, Block{Type: "section", Fields: ownerctc})

		lesseectc := make([]Text, 0)
		lesseectc = append(lesseectc, Text{Type: "mrkdwn", Text: "*Lessee name:*\n " + adresses[i].LesseeName()})
		lesseectc = append(lesseectc, Text{Type: "mrkdwn", Text: "*Lessee mail:*\n " + adresses[i].LesseeMail})
		lesseectc = append(lesseectc, Text{Type: "mrkdwn", Text: "*Lessee tax id:*\n " + adresses[i].LesseeTaxId})

		responseBlocks = append(responseBlocks, Block{Type: "section", Fields: lesseectc})

		adminctc := make([]Text, 0)
		adminctc = append(adminctc, Text{Type: "mrkdwn", Text: "*Admin name:*\n " + adresses[i].AdminName()})
		adminctc = append(adminctc, Text{Type: "mrkdwn", Text: "*Admin username:*\n " + adresses[i].AdminUsername})
		adminctc = append(adminctc, Text{Type: "mrkdwn", Text: "*Admin tax id:*\n " + adresses[i].AdminTaxId})
		adminctc = append(adminctc, Text{Type: "mrkdwn", Text: "*Tenant id:*\n " + adresses[i].AdminTenantId})

		responseBlocks = append(responseBlocks, Block{Type: "section", Fields: adminctc})

		responseBlocks = append(responseBlocks, Block{Type: "divider"})

		naddress++

	}
	if len(adresses) > 9 {
		overFlowText := make([]Text, 1)
		overFlowText[0] = Text{Type: "mrkdwn", Text: "Hay mas propiedades que *no están siendo mostradas* debido al tamaño del mensaje. Para encontrar la propiedad que buscas especifíca mejor la dirección"}
		responseBlocks = append(responseBlocks, Block{Type: "section", Fields: overFlowText})
	}

	commandResponse, err := json.Marshal(SlackCommandAnswer{ResponseType: "in_channel", Text: "Address Info", Blocks: responseBlocks})
	if err != nil {
		return nil, err
	}

	log.Println(string(commandResponse))

	return commandResponse, err
}
