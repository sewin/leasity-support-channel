package slack

import (
	"encoding/json"
	"log"

	model "leasity.cl/tools/support-channel/models"
)

func SendVoucherInfo(vim model.VoucherInfo) ([]byte, error) {
	responseBlocks := make([]Block, 0)

	responseBlocks = append(responseBlocks, Block{Type: "header", Text: Text{Type: "plain_text", Text: "Voucher " + vim.VoucherID + " Information :clipboard::moneybag:"}})
	responseBlocks = append(responseBlocks, Block{Type: "divider"})

	voucherInfoText := "*Admin:* " + vim.AdminName +
		"\n*Username:* " + vim.Username +
		"\n*TenantId:* " + vim.TenantID +
		"\n*KAM:* " + vim.KAM +
		"\n*Voucher Amount:* " + vim.Amount +
		"\n*PropertyId:* " + vim.PropertyID +
		"\n*Status:* " + vim.Status
	if vim.PaymentID != "0" {
		voucherInfoText += "\n*PaymentId:* " + vim.PaymentID +
			"\n*Payment Accounting Date:* " + vim.PaymentAccountingDate +
			"\n*Payment Created At:* " + vim.PaymentCreatedAt +
			"\n*Provider:* " + vim.Provider
	}
	voucherInfoText += "\n*Voucher Id:* " + vim.VoucherID
	if vim.AdminName == "" {
		voucherInfoText = "*No information found*"
	}

	adminContactInfo := Text{Type: "mrkdwn", Text: voucherInfoText}

	responseBlocks = append(responseBlocks, Block{Type: "section", Text: adminContactInfo})

	commandResponse, err := json.Marshal(SlackCommandAnswer{ResponseType: "in_channel", Text: "Voucher Info", Blocks: responseBlocks})
	if err != nil {
		return nil, err
	}

	log.Println(string(commandResponse))

	return commandResponse, err
}

func SendPaymentInfo(vim model.PaymentInfo) ([]byte, error) {
	responseBlocks := make([]Block, 0)

	responseBlocks = append(responseBlocks, Block{Type: "header", Text: Text{Type: "plain_text", Text: "Payment " + vim.PaymentID + " Information :clipboard::moneybag:"}})
	responseBlocks = append(responseBlocks, Block{Type: "divider"})

	voucherInfoText := "*Admin:* " + vim.AdminName +
		"\n*Username:* " + vim.Username +
		"\n*TenantId:* " + vim.TenantID +
		"\n*KAM:* " + vim.KAM +
		"\n*Voucher Amount:* " + vim.Amount +
		"\n*PropertyId:* " + vim.PropertyID
	if vim.PaymentID != "" {
		voucherInfoText += "\n*Voucher Id:* " + vim.VoucherID +
			"\n*Payment Date:* " + vim.PaymentDate +
			"\n*Provider:* " + vim.Provider
	}
	if vim.Username == "" {
		voucherInfoText = "*No information found*"
	}

	adminContactInfo := Text{Type: "mrkdwn", Text: voucherInfoText}

	responseBlocks = append(responseBlocks, Block{Type: "section", Text: adminContactInfo})

	commandResponse, err := json.Marshal(SlackCommandAnswer{ResponseType: "in_channel", Text: "Payment Info", Blocks: responseBlocks})
	if err != nil {
		return nil, err
	}

	log.Println(string(commandResponse))

	return commandResponse, err
}

func ClientMrrSlackResponse() ([]byte, error) {
	responseBlocks := make([]Block, 0)

	responseBlocks = append(responseBlocks, Block{Type: "header", Text: Text{Type: "plain_text", Text: "En un par de minutos llegará el reporte :hourglass_flowing_sand::bust_in_silhouette::moneybag:"}})
	responseBlocks = append(responseBlocks, Block{Type: "divider"})

	commandResponse, err := json.Marshal(SlackCommandAnswer{ResponseType: "in_channel", Text: "Client data", Blocks: responseBlocks})
	if err != nil {
		return nil, err
	}

	log.Println(string(commandResponse))

	return commandResponse, err
}

func ClientNethuntSyncSlackResponse() ([]byte, error) {
	responseBlocks := make([]Block, 0)

	responseBlocks = append(responseBlocks, Block{Type: "header", Text: Text{Type: "plain_text", Text: "Sincronización en progreso, espera unos minutos :hourglass_flowing_sand::bust_in_silhouette::moneybag:"}})
	responseBlocks = append(responseBlocks, Block{Type: "divider"})

	commandResponse, err := json.Marshal(SlackCommandAnswer{ResponseType: "in_channel", Text: "Client data", Blocks: responseBlocks})
	if err != nil {
		return nil, err
	}

	log.Println(string(commandResponse))

	return commandResponse, err
}

func ClientMrrUsedChanError() ([]byte, error) {
	responseBlocks := make([]Block, 0)

	responseBlocks = append(responseBlocks, Block{Type: "header", Text: Text{Type: "plain_text", Text: "Ya hay una consulta en progreso, ten paciencia :hourglass_flowing_sand::bust_in_silhouette::moneybag:"}})
	responseBlocks = append(responseBlocks, Block{Type: "divider"})

	commandResponse, err := json.Marshal(SlackCommandAnswer{ResponseType: "in_channel", Text: "Client data", Blocks: responseBlocks})
	if err != nil {
		return nil, err
	}

	log.Println(string(commandResponse))

	return commandResponse, err
}

func SendLastVoucherInfo(vim model.VoucherInfo) ([]byte, error) {
	responseBlocks := make([]Block, 0)

	responseBlocks = append(responseBlocks, Block{Type: "header", Text: Text{Type: "plain_text", Text: "Property " + vim.PropertyID + " last voucher information :clipboard::moneybag:"}})
	responseBlocks = append(responseBlocks, Block{Type: "divider"})

	voucherInfoText := "*Client:* " + vim.AdminName +
		"\n*Voucher Amount:* " + vim.Amount +
		"\n*PropertyId:* " + vim.PropertyID +
		"\n*Status:* " + vim.Status +
		"\n*Cycle:* " + vim.Period

	if vim.PaymentID != "0" {
		voucherInfoText += "\n*PaymentId:* " + vim.PaymentID +
			"\n*Payment Date:* " + vim.PaymentAccountingDate
	}
	voucherInfoText += "\n*Voucher Id:* " + vim.VoucherID
	if vim.AdminName == "" {
		voucherInfoText = "*No information found*"
	}

	adminContactInfo := Text{Type: "mrkdwn", Text: voucherInfoText}

	responseBlocks = append(responseBlocks, Block{Type: "section", Text: adminContactInfo})

	commandResponse, err := json.Marshal(SlackCommandAnswer{ResponseType: "in_channel", Text: "Voucher Info", Blocks: responseBlocks})
	if err != nil {
		return nil, err
	}

	log.Println(string(commandResponse))

	return commandResponse, err
}
