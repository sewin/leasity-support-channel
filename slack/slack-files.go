package slack

import (
	"bytes"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"path"
	"path/filepath"
)

func SendFileToSlackChannel(sendingfilepath, comment, channel string) error {
	fileDir, _ := os.Getwd()
	fileName := sendingfilepath
	filePath := path.Join(fileDir, fileName)

	file, _ := os.Open(filePath)
	defer file.Close()
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	part, _ := writer.CreateFormFile("file", filepath.Base(file.Name()))
	io.Copy(part, file)

	err := writer.WriteField("initial_comment", comment)
	if err != nil {
		return err
	}
	err = writer.WriteField("channels", channel)
	if err != nil {
		return err
	}

	writer.Close()
	r, _ := http.NewRequest("POST", "https://slack.com/api/files.upload", body)
	log.Println(writer.FormDataContentType())
	r.Header.Add("Content-Type", writer.FormDataContentType())
	r.Header.Add("Authorization", "Bearer xoxb-981968212868-4348378383506-pmKxfL5etDcqxeXQzCRgukFc")
	client := &http.Client{}

	resp, _ := client.Do(r)
	resBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	log.Println(string(resBody))
	return nil
}
