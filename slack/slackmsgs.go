package slack

import (
	"encoding/json"
	"errors"
	"log"
	"strconv"

	"github.com/valyala/fasthttp"
	"leasity.cl/tools/support-channel/dal"
)

type SlackChannel struct {
	ChannelId          string
	AuthorizationToken string
}

func (sc SlackChannel) SendContactInfoToSlackChannel(contacts []dal.ContactInfo, query string) ([]byte, error) {

	responseBlocks := make([]Block, 0)

	responseBlocks = append(responseBlocks, Block{Type: "header", Text: Text{Type: "plain_text", Text: query}})

	for i := 0; i < len(contacts) && i < 7; i++ {
		contactInfo := make([]Text, 0)
		contactInfo = append(contactInfo, Text{Type: "mrkdwn", Text: "*Contact Name:*\n" + contacts[i].ContactFullName()})
		contactInfo = append(contactInfo, Text{Type: "mrkdwn", Text: "*Contact Mail:*\n" + contacts[i].ContactMail})
		contactInfo = append(contactInfo, Text{Type: "mrkdwn", Text: "*Admin Username:*\n" + contacts[i].AdminUsername})
		contactInfo = append(contactInfo, Text{Type: "mrkdwn", Text: "*Tenant Id:*\n" + contacts[i].AdminTenantId})
		contactInfo = append(contactInfo, Text{Type: "mrkdwn", Text: "*Rol(es):*\n" + contacts[i].ContactRoles()})
		contactInfo = append(contactInfo, Text{Type: "mrkdwn", Text: "*Contact Id:*\n" + strconv.Itoa(contacts[i].ContactId)})

		responseBlocks = append(responseBlocks, Block{Type: "section", Fields: contactInfo})
		nproperties := 0
		maxproperties := 20
		if len(contacts[i].Properties) > 0 {
			propertiesTitle := make([]Text, 1)
			propertiesTitle[0] = Text{Type: "mrkdwn", Text: "*Propiedades*"}
			responseBlocks = append(responseBlocks, Block{Type: "section", Fields: propertiesTitle})
		}

		properties := ""
		for j := 0; j < len(contacts[i].Properties) && nproperties <= maxproperties; j++ {
			properties += contacts[i].Properties[j].PropertyName() + "\n"
			nproperties++
		}
		if properties != "" {
			propertiesText := make([]Text, 1)
			propertiesText[0] = Text{Type: "mrkdwn", Text: properties}
			responseBlocks = append(responseBlocks, Block{Type: "section", Fields: propertiesText})
		}
		if nproperties >= maxproperties {
			overFlowText := make([]Text, 1)
			overFlowText[0] = Text{Type: "mrkdwn", Text: "Hay mas propiedades que *no están siendo mostradas* debido al tamaño del mensaje"}
			responseBlocks = append(responseBlocks, Block{Type: "section", Fields: overFlowText})
		}

		responseBlocks = append(responseBlocks, Block{Type: "divider"})
	}

	commandResponse, err := json.Marshal(SlackCommandAnswer{ResponseType: "in_channel", Text: "Contact Info", Blocks: responseBlocks})
	if err != nil {
		return nil, err
	}

	log.Println(string(commandResponse))
	return commandResponse, err
}

func NewSlackChannel(chanelid, authtoken string) SlackChannel {
	return SlackChannel{ChannelId: chanelid, AuthorizationToken: authtoken}
}

func (sc *SlackChannel) SendSlackMessage(msg []byte) error {
	req := fasthttp.AcquireRequest()
	defer fasthttp.ReleaseRequest(req)
	req.SetRequestURI("https://slack.com/api/chat.postMessage")
	req.Header.SetMethod("POST")
	req.Header.Set("Content-type", "application/json")
	req.Header.Set("Authorization", "Bearer xoxb-981968212868-4348378383506-pmKxfL5etDcqxeXQzCRgukFc")

	req.SetBody(msg)

	resp := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseResponse(resp)

	// Perform the request
	err := fasthttp.Do(req, resp)
	if err != nil {
		return err
	}
	if resp.StatusCode() != fasthttp.StatusOK {
		return errors.New("request to slack failed" + string(resp.Body()))
	}

	return nil
}

func (sc *SlackChannel) SendSlackMessageReturningTs(msg []byte) (string, error) {
	req := fasthttp.AcquireRequest()
	defer fasthttp.ReleaseRequest(req)
	req.SetRequestURI("https://slack.com/api/chat.postMessage")
	req.Header.SetMethod("POST")
	req.Header.Set("Content-type", "application/json")
	req.Header.Set("Authorization", "Bearer xoxb-981968212868-4348378383506-pmKxfL5etDcqxeXQzCRgukFc")

	req.SetBody(msg)

	resp := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseResponse(resp)

	// Perform the request
	err := fasthttp.Do(req, resp)
	if err != nil {
		return "", err
	}
	bodyMap := make(map[string]interface{})
	err = json.Unmarshal(resp.Body(), &bodyMap)
	if err != nil {
		return "", err
	}

	return bodyMap["ts"].(string), nil
}

type SlackMessage struct {
	Channel string      `json:"channel"`
	Text    string      `json:"text"`
	Blocks  interface{} `json:"blocks"`
}

type SlackThreadMessage struct {
	Channel  string      `json:"channel"`
	Text     string      `json:"text"`
	ThreadTs string      `json:"thread_ts"`
	Blocks   interface{} `json:"blocks"`
}

type Block struct {
	Type   string      `json:"type"`
	Text   interface{} `json:"text,omitempty"`
	Fields interface{} `json:"fields,omitempty"`
}

type Text struct {
	Type string `json:"type"`
	Text string `json:"text"`
}

type SlackCommandAnswer struct {
	ResponseType string      `json:"response_type"`
	Text         string      `json:"text"`
	Blocks       interface{} `json:"blocks"`
}

type ReactToMessage struct {
	Channel string `json:"channel"`
	TS      string `json:"timestamp"`
	Name    string `json:"name"`
}

func (sc *SlackChannel) ReactToSlackMessage(ts, reaction string) error {
	req := fasthttp.AcquireRequest()
	defer fasthttp.ReleaseRequest(req)
	req.SetRequestURI("https://slack.com/api/reactions.add")
	req.Header.SetMethod("POST")
	req.Header.Set("Content-type", "application/json")
	req.Header.Set("Authorization", "Bearer xoxb-981968212868-4348378383506-pmKxfL5etDcqxeXQzCRgukFc")

	reactStruct := ReactToMessage{Channel: sc.ChannelId, Name: reaction, TS: ts}
	body, err := json.Marshal(reactStruct)
	if err != nil {
		return err
	}

	req.SetBody(body)
	resp := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseResponse(resp)

	// Perform the request
	err = fasthttp.Do(req, resp)
	if err != nil {
		return err
	}

	if resp.StatusCode() != fasthttp.StatusOK {
		return errors.New("reaction to slack ts failed, response body: " + string(resp.Body()))
	}

	bodyMap := make(map[string]interface{})
	err = json.Unmarshal(resp.Body(), &bodyMap)
	if err != nil {
		return err
	}

	return nil
}
