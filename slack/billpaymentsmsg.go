package slack

import (
	"encoding/json"
	"log"
	"strconv"

	"github.com/leekchan/accounting"
	model "leasity.cl/tools/support-channel/models"
)

func (sc SlackChannel) SendExpiredAdminsInfo(admins []model.AdminExpiredPlanPaymentInfo, expiredVouchers int, totalAmount float64) ([]byte, error) {
	ac := accounting.Accounting{Symbol: "$", Precision: 0, Thousand: ".", Decimal: "."}
	responseBlocks := make([]Block, 0)

	responseBlocks = append(responseBlocks, Block{Type: "header", Text: Text{Type: "plain_text", Text: "Resumen de los vouchers expirados :page_facing_up::money_with_wings:"}})
	responseBlocks = append(responseBlocks, Block{Type: "divider"})

	adminContactInfo := Text{Type: "mrkdwn", Text: "*Administradores con vouchers expirados:* " + strconv.Itoa(len(admins)) + "\n*Voucher expirados:* " + strconv.Itoa(expiredVouchers) + "\n*Total Vouchers expirados:* " + ac.FormatMoney(totalAmount)}

	responseBlocks = append(responseBlocks, Block{Type: "section", Text: adminContactInfo})

	commandResponse, err := json.Marshal(SlackMessage{Channel: sc.ChannelId, Text: "Plan payments info", Blocks: responseBlocks})
	if err != nil {
		return nil, err
	}

	log.Println(string(commandResponse))

	return commandResponse, err
}

func (sc SlackChannel) SendUsedSignaturesInfo(UsedSignaturesMonths []model.MonthUsedSignatures) ([]byte, error) {

	responseBlocks := make([]Block, 0)

	responseBlocks = append(responseBlocks, Block{Type: "header", Text: Text{Type: "plain_text", Text: "Resumen de firmas usadas por mes :clipboard::white_check_mark:"}})
	responseBlocks = append(responseBlocks, Block{Type: "divider"})

	commandResponse, err := json.Marshal(SlackMessage{Channel: sc.ChannelId, Text: "Used signatures by month", Blocks: responseBlocks})
	if err != nil {
		return nil, err
	}

	log.Println(string(commandResponse))

	return commandResponse, err
}

func (sc SlackChannel) SendTestThreadUsedSignaturesInfo(UsedSignaturesMonths []model.MonthUsedSignatures, ts string) ([]byte, error) {

	responseBlocks := make([]Block, 0)

	responseBlocks = append(responseBlocks, Block{Type: "header", Text: Text{Type: "plain_text", Text: "Resumen de firmas usadas por mes :clipboard::white_check_mark:"}})
	responseBlocks = append(responseBlocks, Block{Type: "divider"})

	commandResponse, err := json.Marshal(SlackThreadMessage{Channel: sc.ChannelId, Text: "Used signatures by month", Blocks: responseBlocks, ThreadTs: ts})
	if err != nil {
		return nil, err
	}

	log.Println(string(commandResponse))

	return commandResponse, err
}
