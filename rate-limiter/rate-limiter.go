package ratelimiter

import (
	"time"

	"github.com/valyala/fasthttp"
	"golang.org/x/time/rate"
)

// var tokenLimiter *TokenLimiter
var limiterTokenMap map[string]*TokenLimiter

type TokenLimiter struct {
	//tokenCount  map[string]int
	//maxCount    int
	//ticker      *time.Ticker
	rateLimiter *rate.Limiter
	cmrr        chan bool
	//ch          chan struct{}

}

func NewRateLimiterPerMinute(r int, b int) *TokenLimiter {
	tk := &TokenLimiter{rateLimiter: rate.NewLimiter(Per(r, time.Minute), b)}
	tk.cmrr = make(chan bool, 1)
	tk.cmrr <- true
	return tk
}

func Per(eventCount int, duration time.Duration) rate.Limit {
	return rate.Every(duration / time.Duration(eventCount))
}

func AddRateLimiterMiddleware(handler func(*fasthttp.RequestCtx)) func(*fasthttp.RequestCtx) {
	return func(ctx *fasthttp.RequestCtx) {
		apikey, _ := ctx.UserValue("ApiKey").(string)

		if limiterTokenMap[apikey].rateLimiter.Allow() {
			handler(ctx)
		} else {
			ctx.Error("Reached request limit", 500)
		}
	}
}

func InitLimiterTokenMap(keys []string) error {
	tokenMap := make(map[string]*TokenLimiter)
	for _, v := range keys {
		tokenMap[v] = NewRateLimiterPerMinute(1, 5)
	}
	limiterTokenMap = tokenMap
	return nil
}

func (limiter *TokenLimiter) AddThreadLimiterMiddleware(handler func(*fasthttp.RequestCtx), usedChanHandler func(*fasthttp.RequestCtx), ch chan bool) func(*fasthttp.RequestCtx) {
	return func(ctx *fasthttp.RequestCtx) {
		for {
			select {
			case <-ch:
				handler(ctx)
				return
			default:
				usedChanHandler(ctx)
				return
			}
		}
	}
}
