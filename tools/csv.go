package tools

import (
	"encoding/csv"
	"os"
	"strconv"

	model "leasity.cl/tools/support-channel/models"
)

type CsvFile struct {
	Name string
	File *os.File
}

var expiredAdminRowLabels []string = []string{"Admin", "Mail", "Tax Mail", "Phone", "Expired Vouchers", "Expired Amount", "Expired > month", "Expired One month amount", "Payed Vouchers", "N Properties",
	"Payments Amount", "Payments N", "KAM", "Plan", "Tenant ID"}

func NewCsvFromExpiredAdmins(name string, admins []model.AdminExpiredPlanPaymentInfo) error {
	//ac := accounting.Accounting{Symbol: "$", Precision: 0, Thousand: ".", Decimal: "."}
	csvFile, err := os.Create(name + ".csv")
	if err != nil {
		return err
	}
	csvwriter := csv.NewWriter(csvFile)
	err = csvwriter.Write(expiredAdminRowLabels)
	for i := 0; i < len(admins); i++ {
		row := []string{admins[i].LesseeName, admins[i].Admin.Mail, admins[i].Admin.TaxMail, admins[i].Admin.Phone, strconv.Itoa(len(admins[i].ExpiredVouchers)), strconv.Itoa(int(admins[i].ExpiredAmount)),
			strconv.Itoa(len(admins[i].OneMonthExpiredVouchers)), strconv.Itoa(int(admins[i].OneMonthExpiredAmount)), strconv.Itoa(len(admins[i].PayedVouchers)), strconv.Itoa(admins[i].Nproperties),
			strconv.Itoa(int(admins[i].PaymentsAmount)), strconv.Itoa(int(admins[i].PaymentsNumber)),
			admins[i].KAM, admins[i].Plan, admins[i].TenantID}
		_ = csvwriter.Write(row)
	}
	csvwriter.Flush()
	return err

}

var clientMrrLabels []string = []string{"TenantId", "Period", "Value", "Type"}

func NewCsvForMrrReport(name string, r model.ClientMonthlyRecurringRevenueReport) error {
	//ac := accounting.Accounting{Symbol: "$", Precision: 0, Thousand: ".", Decimal: "."}
	csvFile, err := os.Create(name + ".csv")
	if err != nil {
		return err
	}
	csvwriter := csv.NewWriter(csvFile)
	err = csvwriter.Write(clientMrrLabels)
	for tenant, client := range r.Clients {
		for period, value := range client.PeriodValue {
			row := []string{tenant, strconv.Itoa(period), strconv.Itoa(int(value)),
				client.Type}
			_ = csvwriter.Write(row)
		}
	}
	for tenant, client := range r.Contracts {
		for period, value := range client.PeriodValue {
			row := []string{tenant, strconv.Itoa(period), strconv.Itoa(int(value)),
				client.Type}
			_ = csvwriter.Write(row)
		}
	}
	for tenant, client := range r.BrokerClients {
		for period, value := range client.PeriodValue {
			row := []string{tenant, strconv.Itoa(period), strconv.Itoa(int(value)),
				client.Type}
			_ = csvwriter.Write(row)
		}
	}
	for tenant, client := range r.PlanClients {
		for period, value := range client.PeriodValue {
			row := []string{tenant, strconv.Itoa(period), strconv.Itoa(int(value)),
				client.Type}
			_ = csvwriter.Write(row)
		}
	}
	for tenant, client := range r.OnlinePaymentsClients {
		for period, value := range client.PeriodValue {
			row := []string{tenant, strconv.Itoa(period), strconv.Itoa(int(value)),
				client.Type}
			_ = csvwriter.Write(row)
		}
	}
	for tenant, client := range r.ManualPaymentsClients {
		for period, value := range client.PeriodValue {
			row := []string{tenant, strconv.Itoa(period), strconv.Itoa(int(value)),
				client.Type}
			_ = csvwriter.Write(row)
		}
	}
	for tenant, client := range r.AccruedPayments {
		for period, value := range client.PeriodValue {
			row := []string{tenant, strconv.Itoa(period), strconv.Itoa(int(value)),
				client.Type}
			_ = csvwriter.Write(row)
		}
	}
	for tenant, client := range r.AccruedAmounts {
		for period, value := range client.PeriodValue {
			row := []string{tenant, strconv.Itoa(period), strconv.Itoa(int(value)),
				client.Type}
			_ = csvwriter.Write(row)
		}
	}
	for tenant, client := range r.CatchmentClients {
		for period, value := range client.PeriodValue {
			row := []string{tenant, strconv.Itoa(period), strconv.Itoa(int(value)),
				client.Type}
			_ = csvwriter.Write(row)
		}
	}
	for tenant, client := range r.AccruedCatchemts {
		for period, value := range client.PeriodValue {
			row := []string{tenant, strconv.Itoa(period), strconv.Itoa(int(value)),
				client.Type}
			_ = csvwriter.Write(row)
		}
	}
	for tenant, client := range r.FirmClients {
		for period, value := range client.PeriodValue {
			row := []string{tenant, strconv.Itoa(period), strconv.Itoa(int(value)),
				client.Type}
			_ = csvwriter.Write(row)
		}
	}
	for tenant, client := range r.InsuranceClients {
		for period, value := range client.PeriodValue {
			row := []string{tenant, strconv.Itoa(period), strconv.Itoa(int(value)),
				client.Type}
			_ = csvwriter.Write(row)
		}
	}
	for tenant, client := range r.InsuranceCounter {
		for period, value := range client.PeriodValue {
			row := []string{tenant, strconv.Itoa(period), strconv.Itoa(int(value)),
				client.Type}
			_ = csvwriter.Write(row)
		}
	}
	csvwriter.Flush()
	return err

}

var rentAveragesLabels []string = []string{"Area", "Arriendo Promedio", "$/m2", "N"}

func NewCsvForAverageRentReport(name string, r *model.RentAveragesReport) error {
	csvFile, err := os.Create(name + ".csv")
	if err != nil {
		return err
	}
	csvwriter := csv.NewWriter(csvFile)
	err = csvwriter.Write(rentAveragesLabels)
	if err != nil {
		return err
	}

	for k, v := range r.AreaStats {
		row := []string{k, v.AverageRent, v.AvarageRentPerM2, strconv.Itoa(v.NRents)}
		_ = csvwriter.Write(row)
	}
	row := []string{"NIVEL NACIONAL", r.NationalAverage.AverageRent, "-", strconv.Itoa(r.NationalAverage.NRents)}
	_ = csvwriter.Write(row)
	csvwriter.Flush()
	return nil
}

var signtauresByMonthLabels []string = []string{"Year-Month", "Type", "Amount"}

func NewCsvForUsedSignatures(name string, s []model.MonthUsedSignatures) error {
	csvFile, err := os.Create(name + ".csv")
	if err != nil {
		return err
	}
	csvwriter := csv.NewWriter(csvFile)
	err = csvwriter.Write(signtauresByMonthLabels)
	if err != nil {
		return err
	}

	for _, v := range s {
		row := []string{v.YearMonth, "Contrato de arriendo", strconv.Itoa(v.ContratoDeArriendo)}
		_ = csvwriter.Write(row)
		row = []string{v.YearMonth, "Mandato de arriendo", strconv.Itoa(v.MandatoDeArriendo)}
		_ = csvwriter.Write(row)
		row = []string{v.YearMonth, "Firmas simples", strconv.Itoa(v.SimpleFirms)}
		_ = csvwriter.Write(row)

	}

	csvwriter.Flush()
	return nil
}
