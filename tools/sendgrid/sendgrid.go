package sendgrid

import (
	"log"
	"strconv"

	"github.com/leekchan/accounting"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	"leasity.cl/tools/support-channel/dal"
)

type SendgridMailSender struct {
	ApiKey     string
	TemplateID string
	ac         accounting.Accounting
}

func NewBankAccountFixerMailSender() (*SendgridMailSender, error) {
	accounting := accounting.Accounting{Symbol: "$", Precision: 0, Thousand: ".", Decimal: "."}
	sendgridMailSender := SendgridMailSender{ApiKey: "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",
		TemplateID: "d-ca834a3eabb2429a918b533ba700aeaa", ac: accounting}

	return &sendgridMailSender, nil
}

func (ms *SendgridMailSender) SendMail(toEmail string, bi dal.BankAccountRejectedPaymentInfo) error {
	newMap := make(map[string]interface{})

	newMap["adminName"] = bi.AdminName
	newMap["contactName"] = bi.ContactName
	newMap["month"] = bi.PaymentMonth
	newMap["amount"] = ms.ac.FormatMoney(bi.PendingPaymentAmount) //fix
	newMap["address"] = bi.PropertyAddress
	newMap["bankAccountFullName"] = bi.BankAccount.Name
	newMap["bankAccountTaxId"] = bi.BankAccount.TaxID
	newMap["bankAccountBankName"] = bi.BankAccount.Bank
	newMap["bankAccountType"] = bi.BankAccount.Type
	newMap["bankAccountNumber"] = bi.BankAccount.Number
	newMap["bankAccountMail"] = bi.BankAccount.Mail
	newMap["reviewAccountURL"] = "https://app.leasity.cl/rental/contacts/" + strconv.Itoa(bi.BankAccount.ContactID)

	m := mail.NewV3Mail()

	from := mail.NewEmail("Leasity", "contacto@leasity.cl")
	to := mail.NewEmail("User", toEmail)

	m.SetFrom(from)
	m.SetTemplateID(ms.TemplateID)

	// create new *Personalization
	personalization := mail.NewPersonalization()
	personalization.AddTos(to)
	personalization.Subject = "Carta de reajuste"
	for k, v := range newMap {
		personalization.SetDynamicTemplateData(k, v)
	}

	// add `personalization` to `m`
	m.AddPersonalizations(personalization)

	request := sendgrid.GetRequest(ms.ApiKey, "/v3/mail/send", "https://api.sendgrid.com")
	request.Method = "POST"
	var Body = mail.GetRequestBody(m)
	request.Body = Body
	_, err := sendgrid.API(request)
	if err != nil {
		log.Println(err)
		return err
	} else {

	}

	return nil
}
