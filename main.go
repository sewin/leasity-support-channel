package main

import (
	"leasity.cl/tools/support-channel/server"
)

func main() {

	/* err := godotenv.Load("local-testing.env")
	if err != nil {
		log.Fatalf("Some error occured. Err: %s", err)
	} */

	fserver := server.NewFastHttpServer("0.0.0.0:8080")
	fserver.ListenAddr = "0.0.0.0:8080"
	fserver.Run()

}
