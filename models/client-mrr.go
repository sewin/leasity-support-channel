package model

import (
	"encoding/json"

	"leasity.cl/tools/support-channel/dal"
)

type ClientMonthlyRecurringRevenueInfo struct {
	TenantId         string
	PlanEntityId     int
	Username         string
	ClientPeriodData MrrData
	Type             string
	PeriodValue      map[int]float64
}

type MrrData struct {
	PaymentsRevenue  map[string]float64
	PlanRevenue      map[string]float64
	PropertiesNumber map[string]int
}

type MonthUsedSignatures struct {
	ContratoDeArriendo int    `json:"ContratoDeArriendo" validate:"required"`
	MandatoDeArriendo  int    `json:"MandatoDeArriendo" validate:"required"`
	SimpleFirms        int    `json:"SimpleFirms" validate:"required"`
	YearMonth          string `json:"YearMonth" validate:"required"`
}

type ClientMonthlyRecurringRevenueReport struct {
	Clients                 map[string]*ClientMonthlyRecurringRevenueInfo //Plan Clients
	PlanClients             map[string]*ClientMonthlyRecurringRevenueInfo
	OnlinePaymentsClients   map[string]*ClientMonthlyRecurringRevenueInfo
	ManualPaymentsClients   map[string]*ClientMonthlyRecurringRevenueInfo
	ContractPaymentsClients map[string]*ClientMonthlyRecurringRevenueInfo
	BrokerClients           map[string]*ClientMonthlyRecurringRevenueInfo
	CatchmentClients        map[string]*ClientMonthlyRecurringRevenueInfo
	FirmClients             map[string]*ClientMonthlyRecurringRevenueInfo
	InsuranceClients        map[string]*ClientMonthlyRecurringRevenueInfo
	InsuranceCounter        map[string]*ClientMonthlyRecurringRevenueInfo
	AccruedPayments         map[string]*ClientMonthlyRecurringRevenueInfo
	AccruedCatchemts        map[string]*ClientMonthlyRecurringRevenueInfo
	AccruedAmounts          map[string]*ClientMonthlyRecurringRevenueInfo
	Contracts               map[string]*ClientMonthlyRecurringRevenueInfo
	RevenueTenants          map[string]bool
	VoucherInformation      []dal.VoucherCmrrReportInfo
}

func GetClientMonthlyRecurringRevenueReport() *ClientMonthlyRecurringRevenueReport {

	planClients := make(map[string]*ClientMonthlyRecurringRevenueInfo)
	onlinePaymentsClients := make(map[string]*ClientMonthlyRecurringRevenueInfo)
	manualPaymentsClients := make(map[string]*ClientMonthlyRecurringRevenueInfo)
	contractPaymentsClients := make(map[string]*ClientMonthlyRecurringRevenueInfo)
	brokerClients := make(map[string]*ClientMonthlyRecurringRevenueInfo)
	catchmentClients := make(map[string]*ClientMonthlyRecurringRevenueInfo)
	clientsProperties := make(map[string]*ClientMonthlyRecurringRevenueInfo)
	clientsContracts := make(map[string]*ClientMonthlyRecurringRevenueInfo)
	firmClients := make(map[string]*ClientMonthlyRecurringRevenueInfo)
	insuranceClients := make(map[string]*ClientMonthlyRecurringRevenueInfo)
	accruedPayments := make(map[string]*ClientMonthlyRecurringRevenueInfo)
	accruedCatchments := make(map[string]*ClientMonthlyRecurringRevenueInfo)
	accruedAmounts := make(map[string]*ClientMonthlyRecurringRevenueInfo)
	insuranceCounter := make(map[string]*ClientMonthlyRecurringRevenueInfo)

	cmrr := ClientMonthlyRecurringRevenueReport{Clients: clientsProperties, PlanClients: planClients, OnlinePaymentsClients: onlinePaymentsClients, ManualPaymentsClients: manualPaymentsClients,
		BrokerClients: brokerClients, CatchmentClients: catchmentClients, ContractPaymentsClients: contractPaymentsClients, AccruedAmounts: accruedAmounts, Contracts: clientsContracts,
		FirmClients: firmClients, InsuranceClients: insuranceClients, AccruedPayments: accruedPayments, AccruedCatchemts: accruedCatchments, InsuranceCounter: insuranceCounter}

	return &cmrr
}

/* func (cmrr *ClientMonthlyRecurringRevenueReport) ConsolidateLeasityPayments() error {
	for i, v := range cmrr.ContractPaymentsClients {
		for cycle, value := range v.PeriodValue {
			if cmrr.LeasityPaymentsClients[i] != nil {
				cmrr.LeasityPaymentsClients[i] = &ClientMonthlyRecurringRevenueInfo{TenantId: i, Type: "Pagos Leasity", PeriodValue = }
		}

		}
	}
	return nil
} */
/* func (cmrr *ClientMonthlyRecurringRevenueReport) GetClientPeriodProperties() error{
	for i, v := range cmrr.Clients {
		dal.
	}
	return nil
} */

type RevenueReportEntry struct {
	TenantId string  `json:"tenant_id"`
	Cycle    int     `json:"cycle"`
	Value    float64 `json:"value"`
	Type     string  `json:"type"`
}

type RevenueReport struct {
	CmrrEntries  []RevenueReportEntry        `json:"cmrr_entries"`
	PlanVouchers []dal.VoucherCmrrReportInfo `json:"plan_vouchers"`
}

func (cmrr *ClientMonthlyRecurringRevenueReport) MarshalJSON() ([]byte, error) {
	var entries []RevenueReportEntry

	for tenantId, clientInfo := range cmrr.Clients {
		for cycle, value := range clientInfo.PeriodValue {
			entry := RevenueReportEntry{
				TenantId: tenantId,
				Cycle:    cycle,
				Value:    value,
				Type:     "Propiedades",
			}
			entries = append(entries, entry)
		}
	}

	for tenantId, clientInfo := range cmrr.PlanClients {
		for cycle, value := range clientInfo.PeriodValue {
			entry := RevenueReportEntry{
				TenantId: tenantId,
				Cycle:    cycle,
				Value:    value,
				Type:     "Planes",
			}
			entries = append(entries, entry)
		}
	}

	for tenantId, clientInfo := range cmrr.OnlinePaymentsClients {
		for cycle, value := range clientInfo.PeriodValue {
			entry := RevenueReportEntry{
				TenantId: tenantId,
				Cycle:    cycle,
				Value:    value,
				Type:     "Pagos Online",
			}
			entries = append(entries, entry)
		}
	}

	for tenantId, clientInfo := range cmrr.ManualPaymentsClients {
		for cycle, value := range clientInfo.PeriodValue {
			entry := RevenueReportEntry{
				TenantId: tenantId,
				Cycle:    cycle,
				Value:    value,
				Type:     "Pagos Manuales",
			}
			entries = append(entries, entry)
		}
	}

	/* 	for tenantId, clientInfo := range cmrr.ContractPaymentsClients {
		for cycle, value := range clientInfo.PeriodValue {
			entry := RevenueReportEntry{
				TenantId: tenantId,
				Cycle:    cycle,
				Value:    value,
				Type:     "ContractPaymentsClients",
			}
			entries = append(entries, entry)
		}
	} */

	for tenantId, clientInfo := range cmrr.BrokerClients {
		for cycle, value := range clientInfo.PeriodValue {
			entry := RevenueReportEntry{
				TenantId: tenantId,
				Cycle:    cycle,
				Value:    value,
				Type:     "Corretajes",
			}
			entries = append(entries, entry)
		}
	}

	for tenantId, clientInfo := range cmrr.CatchmentClients {
		for cycle, value := range clientInfo.PeriodValue {
			entry := RevenueReportEntry{
				TenantId: tenantId,
				Cycle:    cycle,
				Value:    value,
				Type:     "Recurrencias",
			}
			entries = append(entries, entry)
		}
	}

	for tenantId, clientInfo := range cmrr.FirmClients {
		for cycle, value := range clientInfo.PeriodValue {
			entry := RevenueReportEntry{
				TenantId: tenantId,
				Cycle:    cycle,
				Value:    value,
				Type:     "Firmas",
			}
			entries = append(entries, entry)
		}
	}

	for tenantId, clientInfo := range cmrr.InsuranceClients {
		for cycle, value := range clientInfo.PeriodValue {
			entry := RevenueReportEntry{
				TenantId: tenantId,
				Cycle:    cycle,
				Value:    value,
				Type:     "Seguros",
			}
			entries = append(entries, entry)
		}
	}

	for tenantId, clientInfo := range cmrr.AccruedPayments {
		for cycle, value := range clientInfo.PeriodValue {
			entry := RevenueReportEntry{
				TenantId: tenantId,
				Cycle:    cycle,
				Value:    value,
				Type:     "Pagos devengados",
			}
			entries = append(entries, entry)
		}
	}

	for tenantId, clientInfo := range cmrr.AccruedCatchemts {
		for cycle, value := range clientInfo.PeriodValue {
			entry := RevenueReportEntry{
				TenantId: tenantId,
				Cycle:    cycle,
				Value:    value,
				Type:     "N Recurrencias",
			}
			entries = append(entries, entry)
		}
	}

	for tenantId, clientInfo := range cmrr.AccruedAmounts {
		for cycle, value := range clientInfo.PeriodValue {
			entry := RevenueReportEntry{
				TenantId: tenantId,
				Cycle:    cycle,
				Value:    value,
				Type:     "Total devengado",
			}
			entries = append(entries, entry)
		}
	}

	for tenantId, clientInfo := range cmrr.Contracts {
		for cycle, value := range clientInfo.PeriodValue {
			entry := RevenueReportEntry{
				TenantId: tenantId,
				Cycle:    cycle,
				Value:    value,
				Type:     "Contratos",
			}
			entries = append(entries, entry)
		}
	}

	for tenantId, clientInfo := range cmrr.InsuranceCounter {
		for cycle, value := range clientInfo.PeriodValue {
			entry := RevenueReportEntry{
				TenantId: tenantId,
				Cycle:    cycle,
				Value:    value,
				Type:     "N Seguros",
			}
			entries = append(entries, entry)
		}
	}

	reportData := RevenueReport{CmrrEntries: entries, PlanVouchers: cmrr.VoucherInformation}

	return json.Marshal(reportData)
}
