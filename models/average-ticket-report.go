package model

import (
	"github.com/leekchan/accounting"
	"leasity.cl/tools/support-channel/dal"
)

type RentAveragesReport struct {
	NationalAverage AreaStats
	AreaStats       map[string]AreaStats
}

type AreaStats struct {
	AverageRent      string
	AvarageRentPerM2 string
	MaxRent          string
	MinRent          string
	NRents           int
}

func NewRentAverageReportFromContractAndVouchers(contracts map[string][]dal.ContractAreaInfo, amounts map[int]float64) (*RentAveragesReport, error) {
	ac := accounting.Accounting{Symbol: "$", Precision: 0, Thousand: ".", Decimal: "."}
	averagesReport := RentAveragesReport{}
	nationalAverage := 0.0
	nationalSamples := 0.0
	averagesReport.AreaStats = make(map[string]AreaStats)
	for k, cs := range contracts {
		value := 0.0
		perm2val := 0.0
		ns := 0.0
		nsa := 0.0
		for _, c := range cs {
			if amounts[c.EntityId] > 20000 && amounts[c.EntityId] < 3000000 {
				value += amounts[c.EntityId]
				if c.UsefulArea != 0 {
					perm2val += amounts[c.EntityId] / float64(c.UsefulArea)
					nsa++
				}
				nationalAverage += amounts[c.EntityId]
				nationalSamples++
				ns++
			}

		}
		newAreaStats := AreaStats{}
		newAreaStats.AverageRent = ac.FormatMoney(value / ns)
		newAreaStats.AvarageRentPerM2 = ac.FormatMoney(perm2val / nsa)
		newAreaStats.NRents = int(ns)
		averagesReport.AreaStats[k] = newAreaStats
	}
	averagesReport.NationalAverage.AverageRent = ac.FormatMoney(nationalAverage / nationalSamples)
	averagesReport.NationalAverage.NRents = int(nationalSamples)

	return &averagesReport, nil
}
