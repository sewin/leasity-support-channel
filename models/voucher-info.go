package model

import (
	"math"
	"strconv"

	"github.com/leekchan/accounting"
	"leasity.cl/tools/support-channel/dal"
)

type VoucherInfo struct {
	TenantID              string `json:"tenantId"`
	Amount                string `json:"amount"`
	VoucherID             string `json:"vId"`
	PaymentID             string `json:"paymentId"`
	AdminName             string `json:"adminName"`
	Username              string
	KAM                   string `json:"kam"`
	PropertyID            string `json:"propertyId"`
	Provider              string
	PaymentAccountingDate string
	PaymentCreatedAt      string
	Status                string
	Period                string
}

func GetVoucherInfoAmount(vid dal.VoucherInfo) (VoucherInfo, error) {
	voucherAmount := 0.0
	for _, v := range vid.VoucherEntries {
		if v.Currency == "UF" {
			voucherAmount += v.UFValue * v.Amount
		} else {
			voucherAmount += v.Amount
		}
	}

	period := strconv.Itoa(int(math.Mod(float64(vid.CycleNumber), 12))+1) + "-" + strconv.Itoa(vid.CycleNumber/12)
	ac := accounting.Accounting{Symbol: "$", Precision: 0, Thousand: ".", Decimal: "."}

	return VoucherInfo{TenantID: vid.TenantID,
		Amount:                ac.FormatMoney(voucherAmount),
		VoucherID:             strconv.Itoa(vid.VoucherID),
		PaymentID:             strconv.Itoa(vid.PaymentID),
		AdminName:             vid.AdminName,
		KAM:                   vid.KAM,
		PropertyID:            strconv.Itoa(vid.PropertyId),
		Username:              vid.Username,
		Provider:              vid.PaymentProvider,
		PaymentAccountingDate: vid.PaymentAccountingDate,
		PaymentCreatedAt:      vid.PaymentCreatedAt,
		Status:                vid.Status,
		Period:                period,
	}, nil

}
