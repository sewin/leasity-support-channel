package model

import (
	"sort"

	"leasity.cl/tools/support-channel/dal"
)

type AdminExpiredPlanPaymentInfo struct {
	Admin                   Contact
	OneMonthExpiredVouchers []int
	ExpiredVouchers         []int
	OneMonthExpiredAmount   float64
	ExpiredAmount           float64
	PayedVouchers           []int
	AdminId                 int
	Nproperties             int
	EntityId                int
	KAM                     string
	Plan                    string
	PaymentsAmount          float64
	PaymentsNumber          float64
	LesseeName              string
	TenantID                string
}

type Contact struct {
	FirstName string `json:"first_name"`
	Surname   string `json:"surname"`
	Mail      string `json:"mail"`
	TenantId  string `json:"tenant_id"`
	Phone     string `json:"phone_umber"`
	TaxMail   string `json:"tax_mail"`
}

func GetAdminExpiredPlansFromVoucherEntries(expiredAdmins map[int]bool, adminVoucherEntries map[int][]dal.AdminVoucherEntry, paymentsAmount map[string]float64, paymentsNumber map[string]float64) ([]AdminExpiredPlanPaymentInfo, error) {
	AdminInfos := make([]AdminExpiredPlanPaymentInfo, 0)
	for k := range expiredAdmins {
		newExpiredAdmin := AdminExpiredPlanPaymentInfo{EntityId: k}

		for _, entry := range adminVoucherEntries[k] {
			entryAmount := entry.Amount
			if entry.Currency == "UF" {
				entryAmount = entryAmount * entry.CurrencyValue
			}
			if entry.Expired {
				newExpiredAdmin.ExpiredAmount += entryAmount
				if !contains(newExpiredAdmin.ExpiredVouchers, entry.VoucherId) {
					newExpiredAdmin.ExpiredVouchers = append(newExpiredAdmin.ExpiredVouchers, entry.VoucherId)
				}
			}
			if entry.ExpiredOneMonth {
				newExpiredAdmin.OneMonthExpiredAmount += entryAmount
				if !contains(newExpiredAdmin.OneMonthExpiredVouchers, entry.VoucherId) {
					newExpiredAdmin.OneMonthExpiredVouchers = append(newExpiredAdmin.OneMonthExpiredVouchers, entry.VoucherId)
				}
			}
			if entry.Payed {
				if !contains(newExpiredAdmin.PayedVouchers, entry.VoucherId) {
					newExpiredAdmin.PayedVouchers = append(newExpiredAdmin.PayedVouchers, entry.VoucherId)
				}
			}
		}
		AdminInfos = append(AdminInfos, newExpiredAdmin)
	}
	return AdminInfos, nil
}

func contains(elems []int, v int) bool {
	for _, s := range elems {
		if v == s {
			return true
		}
	}
	return false
}

func (ai *AdminExpiredPlanPaymentInfo) UpdateContactInfo(contactInfo dal.ContactInfo) error {
	ai.Admin.FirstName = contactInfo.ContactFirstName
	ai.Admin.Surname = contactInfo.ContactSurname
	ai.Admin.Phone = contactInfo.PhoneNumber
	ai.Admin.Mail = contactInfo.ContactMail
	ai.Admin.TaxMail = contactInfo.TaxMail
	ai.Nproperties = contactInfo.Nproperties
	ai.KAM = contactInfo.KAM
	ai.Plan = contactInfo.Plan
	ai.LesseeName = contactInfo.AdressStreet
	return nil
}

func (c *Contact) FullName() string {
	return c.FirstName + " " + c.Surname
}

type AdminExpiredInfoPointer *AdminExpiredPlanPaymentInfo

type AdminExpiredPlanInfoTable []AdminExpiredInfoPointer

func (t AdminExpiredPlanInfoTable) Len() int { return len(t) }
func (t AdminExpiredPlanInfoTable) Less(i, j int) bool {
	ri := *t[i]
	rj := *t[j]
	return ri.ExpiredAmount >= rj.ExpiredAmount
}

func (t AdminExpiredPlanInfoTable) Swap(i, j int) {
	*t[i], *t[j] = *t[j], *t[i]
}

func SortAdminExpiredPlanInfoTable(ais []AdminExpiredPlanPaymentInfo) {
	tableToSort := make([]AdminExpiredInfoPointer, len(ais))
	for i := 0; i < len(ais); i++ {
		tableToSort[i] = &ais[i]
	}
	sort.Sort(AdminExpiredPlanInfoTable(tableToSort))
}
