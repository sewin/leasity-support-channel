package model

import (
	"strconv"

	"github.com/leekchan/accounting"
	"leasity.cl/tools/support-channel/dal"
)

type PaymentInfo struct {
	TenantID    string `json:"tenantId"`
	Amount      string `json:"amount"`
	VoucherID   string `json:"vId"`
	PaymentID   string `json:"paymentId"`
	AdminName   string `json:"adminName"`
	Username    string
	KAM         string `json:"kam"`
	PropertyID  string `json:"propertyId"`
	Provider    string
	PaymentDate string
}

func GetPaymentInfoAmount(pid dal.PaymentInfo) (PaymentInfo, error) {
	voucherAmount := 0.0
	for _, v := range pid.VoucherEntries {
		if v.Currency == "UF" {
			voucherAmount += v.UFValue * v.Amount
		} else {
			voucherAmount += v.Amount
		}
	}
	ac := accounting.Accounting{Symbol: "$", Precision: 0, Thousand: ".", Decimal: "."}

	return PaymentInfo{TenantID: pid.TenantID,
		Amount:      ac.FormatMoney(voucherAmount),
		VoucherID:   strconv.Itoa(pid.VoucherID),
		PaymentID:   strconv.Itoa(pid.PaymentID),
		AdminName:   pid.AdminName,
		KAM:         pid.KAM,
		PropertyID:  strconv.Itoa(pid.PropertyId),
		Username:    pid.Username,
		Provider:    pid.PaymentProvider,
		PaymentDate: pid.PaymentDate,
	}, nil

}
