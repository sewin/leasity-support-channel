package model

type BankAccountRejectedPaymentInfo struct {
	BankAccountID        int
	LastPayedVoucherID   int
	PendingPaymentAmount float64
	VoucherCycle         int
	AdminName            string
	PropertyAddress      string
	BankAccount          BankAccount
}

type BankAccount struct {
	ID     string
	Type   string
	Number string
	Bank   string
}
